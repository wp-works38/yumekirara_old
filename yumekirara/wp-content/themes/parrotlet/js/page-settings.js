
/**
 * ホーム画面
 */
$ = jQuery;


var yk_settings_class = function (jQuery) {

    // 画面読込時
    jQuery(function () {
        $('#furi-ritu').keyup(num_check);
        $('#hakama-ritu').keyup(num_check);
        $('.left_input_show').keyup(calc_to_right);
        $('#incentiveGroupUpdte_show').keyup(check_shopBudget);
        $('#selfEvaluatefGroupUpdte').keyup(check_percent);
    });
};

// 必須チェック
var check_blank = function () {
    var input = $('#' + this.id);
    var valid = $('#valid-' + this.id);
    if (String(input.val()).length === 0) {
        valid.html('入力してください。');
        return;
    }
    valid.html('');
};

    // 入力チェック（規定の個人評価）
var check_percent = function () {

    var show_input = $('#selfEvaluatefGroupUpdte');
    var error_message = $('#valid-selfEvaluatefGroupUpdte');

    // エラークリア
    error_message.html('');

    // 空白はそのまま
    if (show_input.val() === '') {
        return false;
    }

    // 小数第一位を四捨五入
    var num = no_kanma(show_input.val());
    num = orgRound(parseFloat(num), 1);
    num = parseInt(num);

    var pattern = /^([1-9]\d*|0)$/;
    if (!pattern.test(show_input.val())) {
        // 数字だけ取り出す
        var e_num = show_input.val().replace(/[^0-9]/g, '');
        if ($.isNumeric(e_num)) {
            // intに変換して 0011 -> 11 にする
            e_num = parseInt(e_num);
        }
        show_input.val(e_num);
        error_message.html('数値を入力してください');
        return false;
    }

    if (3 < String(num).length) {
        show_input.val(String(num).slice(0, 3));
        error_message.html('3桁までしか入力できません');
        return;
    }
    show_input.val(num);
};

// 予測最低接客数
var num_check = function () {

    // 振袖　小数第一位を四捨五入
    var input = $(this);
    var valid = $('#valid-' + this.id);

    // エラークリア
    valid.html('');

    // 空白はそのまま
    if ($(this).val() === '') {
        return false;
    }

    // 0以上整数のみ
    var pattern = /^([1-9]\d*|0)$/;
    if (!pattern.test(input.val())) {
        // 数字だけ取り出す
        var e_num = input.val().replace(/[^0-9]/g, '');
        if ($.isNumeric(e_num)) {
            // intに変換して 0011 -> 11 にする
            e_num = parseInt(e_num);
        }
        input.val(e_num);
        valid.html('数値を入力してください');
        return false;
    }

    var num = orgRound(parseFloat(input.val()), 1);
    num = parseInt(num);
    if (num < 0 || 1000 < num) {
        if (num < 0) {
            input.val(0);
        }
        else if (100 < num) {
            input.val(1000);
        }
        valid.html('0～1000までの数値を入力してください');
        return false;
    }

    input.val(num);
    return true;

};

// 予算チェック
var check_shopBudget = function () {

    var show_input = $($(this).parent().find('#incentiveGroupUpdte_show')[0]);
    var hidden_input = $($(this).parent().find('#incentiveGroupUpdte')[0]);
    var error_message = $($(this).parent().find('#valid-incentiveGroupUpdte')[0]);

    // エラークリア
    error_message.html('');

    // 空白はそのまま
    if (show_input.val() === '') {
        hidden_input.val(0);
        return false;
    }

    // 小数第一位を四捨五入
    var num = no_kanma(show_input.val());
    num = orgRound(parseFloat(num), 1);
    num = parseInt(num);
    if (!$.isNumeric(num)) {
        show_input.val(separate(hidden_input.val()));
        error_message.html('数値を入力してください');
        return false;
    }
    else if (11 < String(num).length) {
        show_input.val(separate(hidden_input.val()));
        error_message.html('11桁までしか入力できません');
        return false;
    }

    // 隠し項目にカンマなしの整数をセット
    hidden_input.val(num);
    show_input.val(separate(num));
    return ture;
};

var calc_to_right = function () {
    // tr取得
    var tr = $(this).parent().parent()[0];
    var left_input_show = $(tr).find('.left_input_show');
    var left_input_hidden = $(tr).find('.left_input');
    var valid_shop_left = $(tr).find('.valid_shop_left');
    
    // エラークリア
    valid_shop_left.html('');

    // 空白はそのまま
    if ($(this).val() === '') {
        left_input_hidden.val(0);
        return false;
    }

    var num = no_kanma($(this).val());
    if (!$.isNumeric(num)) {
        valid_shop_left.html('数値を入力してください。');
        $(this).val(separate(orgRound(parseInt(left_input_hidden.val()) / 1000, 1)));
        return;
    }
    else if (5 < String(num).length) {
        valid_shop_left.html('5桁までしか入力できません。');
        $(this).val(separate(orgRound(parseInt(left_input_hidden.val()) / 1000, 1)));
        return;
    }
    left_input_hidden.val(num * 1000);
    left_input_show.val(separate(num));
};

// 基本設定 店舗予算 予測最低接客数
var yk_settings_shop_class = function (jQuery) {

    const CSRF = "";
    const ajaxurl = "";

    // 店舗予算 更新
    this.SaveShop = function () {
        if (confirm('保存しますか？')) {

            var $record = new Object();
            var data = [];
            //var checkdata = [];
            var message = '';

            // テーブルデータの取得
            var rows = $("#shop-table tr");//全行を取得
            //for (var i = 0, l = rows.length; i < l; i++) {
            //    var cells = rows.eq(i).find('input');//1行目から順にth、td問わず列を取得
            //    for (var j = 0, m = cells.length; j < m; j++) {

            //        if (typeof data[i] === "undefined") data[i] = [];
            //        data[i][j] = cells.eq(j).val();//i行目j列の文字列を取得

            //        if (typeof checkdata[i] === "undefined") checkdata[i] = [];
            //        checkdata[i][j] = cells.eq(j).prop("checked");//i行目j列のcheckbox：checkedを取得
            //    }
            //}
            //$record['data'] = data;
            var arr = new Array();
            for (var row_num = 0; row_num < rows.length; row_num++) {

                // ヘッダーはコンテニュー
                if (row_num === 0)
                    continue;

                var row = $(rows[row_num]);
                var hashColor1 = {
                    F_ID: row.find('input[name="F_ID"]')[0].value
                    , F_SHOP: row.find('input[name="F_SHOP"]')[0].value
                    , F_SHOP_BUDGET: row.find('input[name="F_SHOP_BUDGET"]')[0].value
                    , F_PROVISION_FUND: row.find('input[name="F_PROVISION_FUND"]')[0].checked
                };
                arr.push(hashColor1);
            }
            $record['rows'] = arr;
            //$record['checkdata'] = checkdata;

            //更新処理 ajaxで保存
            $.ajax({
                url: this.ajaxurl,
                type: 'POST',
                data: {
                    // 呼び出すアクション名
                    action: 'my_ajax_get_posts',
                    // アクションに対応するnonce
                    secure: this.CSRF,
                    // 入力データ
                    params: $record,
                    module: 'request_get_yk_settings_shop_save'
                }
            })
                .done(function (res) {

                    if (res.code) {
                        //保存が正常終了していれば画面をリロード
                        location.reload();
                    } else {
                        // エラーメッセージ出力
                        
                    }
                    console.log(res.result);
                })
                .fail(function (jqXHR, textStatus, errorThrown) {
                    alert('error');
                });
        } else {
            // キャンセル
            location.reload;
        }
    };

    //予測最低接客数 更新
    this.SaveFinance = function () {
        if (confirm('保存しますか？')) {
            //入力データの整形
            var $record = new Object();
            $record['F_FURISODE'] = $('#furi-ritu').val();
            $record['F_HAKAMA'] = $('#hakama-ritu').val();

            //更新処理 ajaxで保存
            $.ajax({
                url: this.ajaxurl,
                type: 'POST',
                data: {
                    // 呼び出すアクション名
                    action: 'my_ajax_get_posts',
                    // アクションに対応するnonce
                    secure: this.CSRF,
                    // 入力データ
                    params: $record,
                    module: 'request_get_yk_settings_finance_save'
                }
            })
                .done(function (res) {
                    //保存後の処理
                    if (res.is_error) {
                        alert(res.result);
                    }
                    else {
                        location.reload();
                    }
                    
                })
                .fail(function (jqXHR, textStatus, errorThrown) {
                    alert('error');
                });
        } else {
            // キャンセル
            location.reload;
        }
    };

    $(document).ready(function () {

    });
};
// 入力項目設定 個人評価
var yk_settings_self_evaluate_class = function (jQuery) {

    const CSRF = "";
    const ajaxurl = "";

    //新規
    this.newSelfEvaluate = function () {
        dialog_previewSelfEvaluate.ShowNewDialog();
    };

    //編集
    this.editSelfEvaluate = function (id) {
        dialog_previewSelfEvaluate.ShowEditDialog(id);
    };

    // 削除
    this.deleteSelfEvaluate = function (id) {

        if (confirm('本当に削除しますか？')) {
            // 削除
            this.Delete(id);
        } else {
            // キャンセル
            location.reload;
        }
    };
    this.Delete = function (id) {
        var $record = new Object();
        $record['F_ID'] = id;
        //更新処理 ajaxで保存
        $.ajax({
            url: this.ajaxurl,
            type: 'POST',
            data: {
                // 呼び出すアクション名
                action: 'my_ajax_get_posts',
                // アクションに対応するnonce
                secure: this.CSRF,
                // 入力データ
                params: $record,
                module: 'request_yk_self_evaluate_all_delete'
            }
        })
            .done(function (res) {
                location.reload();
            })
            .fail(function (jqXHR, textStatus, errorThrown) {
                alert('error');
            });
    };

    $(document).ready(function () {

    });
};
// 職員マスタ
var yk_settings_staff_class = function (jQuery) {

    const CSRF = "";
    const ajaxurl = "";

    //新規
    this.newStaff = function () {
        dialog_previewStaff.ShowNewDialog();
    };

    //編集
    this.editStaff = function (id) {
        dialog_previewStaff.ShowEditDialog(id);
    };

    //職員マスタ 一括更新
    this.groupUpdate = function () {
        if (confirm('更新しますか？')) {
            // 入力チェック
            var errflg = false;
            var message = '';
            if ($('#staffs').val() === '0') {
                errflg = true;
                message += '役職を選択してください。\n';
            }
            if (!$.isNumeric($('#staff-groupUpdte input[name="F_INCENTIVE_GROUP_UPDTE"]').val())) {
                errflg = true;
                message += '規定のインセンティブは数値を入力してください。\n';
            }
            if (11 < String($('#staff-groupUpdte input[name="F_INCENTIVE_GROUP_UPDTE"]').val()).length) {
                errflg = true;
                message += '規定のインセンティブは11桁以内の数値を入力してください。\n';
            }
            if (!$.isNumeric($('#staff-groupUpdte input[name="F_SELF_EVALUATEF_GROUP_UPDTE"]').val())) {
                errflg = true;
                message += '規定の個人評価は数値を入力してください。\n';
            }
            if (parseFloat($('#staff-groupUpdte input[name="F_SELF_EVALUATEF_GROUP_UPDTE"]').val()) < 0
                || 1000 < parseFloat($('#staff-groupUpdte input[name="F_SELF_EVALUATEF_GROUP_UPDTE"]').val())) {
                message += '規定の個人評価は0～1000までの数値を入力してください。\n ';
            }
            if (errflg) {
                //$('#MessageGroupUpdte').html(message);
                alert(message);
                return;
            }

            // 更新
            var $record = new Object();
            $record['F_POSITION_ID'] = $('option:selected').attr('position');
            $record['F_INCENTIVE'] = $('#staff-groupUpdte input[name="F_INCENTIVE_GROUP_UPDTE"]').val();
            $record['F_SELF_EVALUATEF'] = $('#staff-groupUpdte input[name="F_SELF_EVALUATEF_GROUP_UPDTE"]').val();
            //更新処理 ajaxで保存
            $.ajax({
                url: this.ajaxurl,
                type: 'POST',
                data: {
                    // 呼び出すアクション名
                    action: 'my_ajax_get_posts',
                    // アクションに対応するnonce
                    secure: this.CSRF,
                    // 入力データ
                    params: $record,
                    module: 'request_get_yk_settings_staff_group_update'
                }
            })
                .done(function (res) {
                    //保存後の処理
                    location.reload();
                    $('#MessageGroupUpdte').html('');
                    //alert('保存しました。');
                })
                .fail(function (jqXHR, textStatus, errorThrown) {
                    alert('error');
                });
        } else {
            // キャンセル
            location.reload;
        }
    };

    $(document).ready(function () {

    });
};

var yk_settings = new yk_settings_class(jQuery);
var yk_settings_shop = new yk_settings_shop_class(jQuery);
var yk_settings_self_evaluate = new yk_settings_self_evaluate_class(jQuery);
var yk_settings_staff = new yk_settings_staff_class(jQuery);
//var dialog_edit_self_evaluate = new dialog_previewSelfEvaluate_class(jQuery);
//var dialog_edit_staff = new dialog_previewStaff_class(jQuery);



// 初期化処理
jQuery(function () {

    // 店舗予算の合計値
    var all_shop_sum = $('#all_shop_sum');
    all_shop_sum.html(separate( orgRound(parseInt(all_shop_sum.html()) / 1000, 100)));
    
    // 仮半期売上予算  htlml 支給原資総額
    var siky_list = $('.sikyugennsisougaku');
    for (var i_siky_list = 0; i_siky_list < siky_list.length; i_siky_list++) {
        var siky = $(siky_list[i_siky_list]);
        $(siky).html(separate(orgRound(siky.html(), 1)) + "円");
    }

    // 店舗予算  input 予算の初期値
    var left_inputs = $('.left_input_show');
    for (var i = 0; i < left_inputs.length; i++) {
        var num = no_kanma(left_inputs[i].value);
        var sen = orgRound(parseInt(num) / 1000, 1);
        $(left_inputs[i]).val(separate(sen));
    }

    // 予測最低接客数  振袖成約率
    var furi_ritu = $('#furi-ritu');
    $(furi_ritu).val(orgRound(furi_ritu.val(), 1));

    // 予測最低接客数  袴成約率
    var hakama_ritu = $('#hakama-ritu');
    $(hakama_ritu).val(orgRound(hakama_ritu.val(), 1));

    // 職員マスタ  htlml 規定のインセンティブ
    var F_INCENTIVES = $('.F_INCENTIVE');
    for (var i_F_INCENTIVES = 0; i_F_INCENTIVES < F_INCENTIVES.length; i_F_INCENTIVES++) {
        var F_INCENTIVE = $(F_INCENTIVES[i_F_INCENTIVES]);
        $(F_INCENTIVE).html(separate(orgRound(F_INCENTIVE.html(), 1))+"円");
    }
});
function orgRound(value, base) {
    return Math.round(value * base) / base;
}
function separate(num) {
    // 文字列にする
    num = String(num);

    var len = num.length;

    // 再帰的に呼び出すよ
    if (len > 3) {
        // 前半を引数に再帰呼び出し + 後半3桁
        return separate(num.substring(0, len - 3)) + ',' + num.substring(len - 3);
    } else {
        return num;
    }
}
function no_kanma(input) {
    return Number(String(input).replace(/,/g, ''));
}
