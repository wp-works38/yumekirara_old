/**
 * 編集
 */
$ = jQuery;

var dialog_previewStaff_class = function (jQuery) {

    var CSRF;

    //保存後のイベント
    var OnSave;
    var params = {
        action: 'my_ajax_get_posts',
        module: ''
    };

    //ダイアログ表示  新規作成
    this.ShowNewDialog = function () {
        $('#delete').hide();
        $('#ErrorMessageStaff').html('');
        var input = '#modal-previewStaff input';
        $(input).text('');

        $(input + '[name="F_LAST_NAME"]').val('');
        $(input + '[name="F_FAST_NAME"]').val('');

        //入力情報をクリアしておく
        $(input + '[id="F_POSITION_MANAGER"]').prop("checked", false);
        $(input + '[id="F_POSITION_CHIEF"]').prop("checked", false);
        $(input + '[id="F_POSITION_MEMBER"]').prop("checked", false);

        $(input + '[id="F_KAWAGOE"]').prop("checked", false);
        $(input + '[id="F_TOKOROZAWA"]').prop("checked", false);
        $(input + '[id="F_TATIKAWA"]').prop("checked", false);
        $(input + '[id="F_KITIJOUJI"]').prop("checked", false);
        $(input + '[id="F_STUDIO"]').prop("checked", false);

        $(input + '[name="F_INCENTIVE"]').val('');
        $(input + '[name="F_SELF_EVALUATEF"]').val('');

        // ボタンの表示非表示
        $('#modal-previewStaff #delete').hide();
        $('#modal-previewStaff #save').show();

        $('#modal-previewStaff').modal('show');
    };

    //ダイアログ表示  編集
    this.ShowEditDialog = function ($id) {
        $('#ErrorMessageStaff').html('');

        // ボタンの表示非表示
        $('#modal-previewStaff #delete').show();
        $('#modal-previewStaff #save').show();


        this.Load($id, function () {
            $('#modal-previewStaff').modal('show');

            // 規定のインセンティブ
            var num = parseInt(no_kanma($('input[name="F_INCENTIVE"]').val()));
            $('#F_INCENTIVE_show').val(separate(num));
        });
    };

    this.Load = function ($id, endfunc) {
        //最新情報を取得
        var prm = new Object();
        prm['id'] = $id;
        $.ajax({
            url: this.ajaxurl,
            type: 'POST',
            data: {
                // 呼び出すアクション名
                action: 'my_ajax_get_posts',
                // アクションに対応するnonce
                secure: this.CSRF,
                // 入力データ
                params: prm,
                module: 'request_yk_staff_get'
            }
        })
            .done(function (res) {
                $result = res['result'];
                $result2 = res['result2'];
                $result3 = res['result3'];

                $('#modal-previewStaff input[name="F_ID"]').val($result.F_ID);
                $('#modal-previewStaff input[name="F_LAST_NAME"]').val($result.F_LAST_NAME);
                $('#modal-previewStaff input[name="F_FAST_NAME"]').val($result.F_FAST_NAME);

                $('#modal-previewStaff input[id="F_POSITION_MANAGER"]').prop("checked", $result2 === "店長");
                $('#modal-previewStaff input[id="F_POSITION_CHIEF"]').prop("checked", $result2 === "チーフ");
                $('#modal-previewStaff input[id="F_POSITION_MEMBER"]').prop("checked", $result2 === "スタッフ");

                $('#modal-previewStaff input[id="F_KAWAGOE"]').prop("checked", false);
                $('#modal-previewStaff input[id="F_TOKOROZAWA"]').prop("checked", false);
                $('#modal-previewStaff input[id="F_TATIKAWA"]').prop("checked", false);
                $('#modal-previewStaff input[id="F_KITIJOUJI"]').prop("checked", false);
                $('#modal-previewStaff input[id="F_STUDIO"]').prop("checked", false);

                ary = $result3.split(',');
                ary.forEach(function (val, index) {
                    if (val === '川越') {
                        $('#modal-previewStaff input[id="F_KAWAGOE"]').prop("checked", true);
                    }
                    if (val === '所沢') {
                        $('#modal-previewStaff input[id="F_TOKOROZAWA"]').prop("checked", true);
                    }
                    if (val === '立川') {
                        $('#modal-previewStaff input[id="F_TATIKAWA"]').prop("checked", true);
                    }
                    if (val === '吉祥寺') {
                        $('#modal-previewStaff input[id="F_KITIJOUJI"]').prop("checked", true);
                    }
                    if (val === 'スタジオ') {
                        $('#modal-previewStaff input[id="F_STUDIO"]').prop("checked", true);
                    }
                });
                ary = [];
                $('#modal-previewStaff input[name="F_INCENTIVE"]').val($result.F_INCENTIVE);
                $('#modal-previewStaff input[name="F_SELF_EVALUATEF"]').val($result.F_SELF_EVALUATEF);

                endfunc();
            })
            .fail(function (jqXHR, textStatus, errorThrown) {
                //jQuery('#popup_search_erorr_message_title').show();
                //console.log(jqXHR, textStatus, errorThrown, arguments);
                alert('error');
            });

    };

    //ダイアログ保存ボタン
    this.Save = function () {
        //if (confirm('保存しますか？')) {
        // 入力チェック
        var errflg = false;
        var message = '';

        if ($('#modal-previewStaff input:radio[name="F_POSITION_ID"]:checked').val() === undefined) {
            errflg = true;
            message += '役職を選択してください。<br />';
        }
        var a = $('#modal-previewStaff').find('.F_SHOP_ID:checked').map(function () {
            return $(this).val();
        }).get();

        if (a.length === 0) {
            errflg = true;
            message += '所属店舗を選択してください。<br />';
        }

        if (errflg) {
            // アラート表示
            $('#ErrorMessageStaff').html(message);
            //alert(message);
            return;
        }
        //入力データの整形
        var $record = new Object();
        $record['F_ID'] = $('#modal-previewStaff input[name="F_ID"]').val();
        $record['F_LAST_NAME'] = $('#modal-previewStaff input[name="F_LAST_NAME"]').val();
        $record['F_FAST_NAME'] = $('#modal-previewStaff input[name="F_FAST_NAME"]').val();

        $record['F_POSITION_ID'] = $('#modal-previewStaff input:radio[name="F_POSITION_ID"]:checked').val();

        $record['F_SHOP_ID'] = a;

        $record['F_INCENTIVE'] = $('#modal-previewStaff input[name="F_INCENTIVE"]').val();
        $record['F_SELF_EVALUATEF'] = $('#modal-previewStaff input[name="F_SELF_EVALUATEF"]').val();

        //更新処理 ajaxで保存
        $.ajax({
            url: this.ajaxurl,
            type: 'POST',
            data: {
                // 呼び出すアクション名
                action: 'my_ajax_get_posts',
                // アクションに対応するnonce
                secure: this.CSRF,
                // 入力データ
                params: $record,
                module: 'request_yk_staff_save'
            }
        })
            .done(function (res) {
                //保存後の処理
                if (res.is_error) {
                    alert(res.result);
                }
                else {
                    location.reload();
                }
            })
            .fail(function (jqXHR, textStatus, errorThrown) {
                alert('error');
            });
        //} else {
        //    // キャンセル
        //    location.reload;
        //}
    };

    //ダイアログ削除
    this.Delete = function () {
        if (confirm('本当に削除しますか？')) {
            // 削除
            var $record = new Object();
            $record['F_ID'] = $('#modal-previewStaff input[name="F_ID"]').val();
            //更新処理 ajaxで保存
            $.ajax({
                url: this.ajaxurl,
                type: 'POST',
                data: {
                    // 呼び出すアクション名
                    action: 'my_ajax_get_posts',
                    // アクションに対応するnonce
                    secure: this.CSRF,
                    // 入力データ
                    params: $record,
                    module: 'request_yk_staff_delete'
                }
            })
                .done(function (res) {
                    //保存後の処理
                    location.reload();
                })
                .fail(function (jqXHR, textStatus, errorThrown) {
                    alert('error');
                });
        } else {
            // キャンセル
            location.reload;
        }
    };

    $(document).ready(function () {
        $('#F_INCENTIVE_show').keyup(num_check);
        $('#F_SELF_EVALUATEF').keyup(staff_input_check);
    });
    // 入力チェック
    var calc_to_right = function () {

        var hidden_input = $('#F_INCENTIVE');
        var show_input = $('#F_INCENTIVE_show');
        var error_message = $('#F_INCENTIVE_error');

        // エラークリア
        error_message.html('');

        // カンマ除去
        var num = no_kanma(show_input.val());

        // 小数点四捨五入
        num = orgRound(parseFloat(num), 1);
        if (!$.isNumeric(num)) {
            error_message.html('数値を入力してください。');
            show_input.val(separate(parseInt(hidden_input.val())));
            return;
        }
        else if (11 < String(num).length) {
            error_message.html('11桁までしか入力できません。');
            show_input.val(separate(parseInt(hidden_input.val())));
            return;
        }
        hidden_input.val(num);
    };

    // 入力チェック（数値）
    var num_check = function () {

        var show_input = $($(this).parent().find('#F_INCENTIVE_show')[0]);
        var hidden_input = $($(this).parent().find('#F_INCENTIVE')[0]);
        var error_message = $($(this).parent().find('#F_INCENTIVE_error')[0]);

        // エラークリア
        error_message.html('');

        // 空白はそのまま
        if (show_input.val() === '') {
            hidden_input.val(0);
            return false;
        }

        // 小数第一位を四捨五入
        var num = no_kanma(show_input.val());
        num = orgRound(parseFloat(num), 1);
        num = parseInt(num);
        if (!$.isNumeric(num)) {
            show_input.val(separate(parseInt(hidden_input.val())));
            error_message.html('数値を入力してください');
            return;
        }
        if (num < 0 || 99999999999 < num) {
            if (num < 0) {
                show_input.val(separate(parseInt(hidden_input.val())));
            }
            else if (100 < num) {
                show_input.val(separate(parseInt(hidden_input.val())));
            }
            error_message.html('11桁までしか入力できません');
            return false;
        }

        // 隠し項目にカンマなしの整数をセット
        hidden_input.val(num);
        show_input.val(separate(num));

    };
    // 入力チェック（規定の個人評価）
    var staff_input_check = function () {

        var show_input = $('#F_SELF_EVALUATEF');
        var error_message = $('#F_SELF_EVALUATEF_error');

        // エラークリア
        error_message.html('');

        // 空白はそのまま
        if (show_input.val() === '') {
            return false;
        }

        // 小数第一位を四捨五入
        var num = no_kanma(show_input.val());
        num = orgRound(parseFloat(num), 1);
        num = parseInt(num);

        var pattern = /^([1-9]\d*|0)$/;
        if (!pattern.test(show_input.val())) {
            // 数字だけ取り出す
            var e_num = show_input.val().replace(/[^0-9]/g, '');
            if ($.isNumeric(e_num)) {
                // intに変換して 0011 -> 11 にする
                e_num = parseInt(e_num);
            }
            show_input.val(e_num);
            error_message.html('数値を入力してください');
            return false;
        }



        if (3 < String(num).length) {
            show_input.val(String(num).slice(0, 3));
            error_message.html('3桁までしか入力できません');
            return;
        }
        show_input.val(num);
    };
};

var dialog_previewStaff = new dialog_previewStaff_class(jQuery);


function orgRound(value, base) {
    return Math.round(value * base) / base;
}
function separate(num) {
    // 文字列にする
    num = String(num);

    var len = num.length;

    // 再帰的に呼び出すよ
    if (len > 3) {
        // 前半を引数に再帰呼び出し + 後半3桁
        return separate(num.substring(0, len - 3)) + ',' + num.substring(len - 3);
    } else {
        return num;
    }
}
function no_kanma(input) {
    return Number(String(input).replace(/,/g, ''));
}
