/**
 * 編集
 */
$ = jQuery;

var dialog_previewSelfEvaluate_class = function (jQuery) {

    var CSRF;

    //保存後のイベント
    var OnSave;

    var params = {
        action: 'my_ajax_get_posts',
        module: ''
    };

    //一覧 項目を追加
    this.ShowNewDialog = function () {
        $('#ErrorMessageSelfEvaluate').html('');
        $('#modal-previewSelfEvaluate input').val('');
        $('#modal-previewSelfEvaluate select').val('');
        $('#modal-previewSelfEvaluate textarea').text('');
        $('#choice-table').empty();
        $('#choice-table').append('<tr><th>選択肢名</th><th>評価ポイント</th><th>削除</th></tr>');
        $('#modal-previewSelfEvaluate').modal('show');
    };

    //一覧  編集
    this.ShowEditDialog = function ($id) {
        $('#ErrorMessageSelfEvaluate').html('');
        $('#modal-previewSelfEvaluate input[name="F_SELF_EVALUATE_ID"]').val($id);
        this.Load($id, function () {
            $('#modal-previewSelfEvaluate').modal('show');




        });
    };

    //ダイアログ 表示値取得
    this.Load = function ($id, endfunc) {
        //最新情報を取得
        var prm = new Object();
        prm['F_ID'] = $id;
        $.ajax({
            url: this.ajaxurl,
            type: 'POST',
            data: {
                // 呼び出すアクション名
                action: 'my_ajax_get_posts',
                // アクションに対応するnonce
                secure: this.CSRF,
                // 入力データ
                params: prm,
                module: 'request_yk_self_evaluate_get'
            }
        })
            .done(function (res) {
                $result = res['result'];
                $result2 = res['result2'];

                $('#choice-table').empty();
                $('#choice-table').append('<tr><th>選択肢名</th><th>評価ポイント</th><th>削除</th></tr>');
                for (let i = 0; i < $result.length; i++) {
                    $('#choice-table').append(
                        '<tr id="choice-table-tr">'
                        + '<td>'
                        + '<input name="F_ID" type="hidden" class="Id" value="' + $result[i].F_ID + '" />'
                        + '<input name="F_SELF_EVALUATE" type="hidden" class="SelfEvaluate" value="' + $result[i].F_SELF_EVALUATE + '" />'
                        + '<input name="F_CHOICE_NAME" type="text"　class="ChoiceName" value="' + $result[i].F_CHOICE_NAME + '" />'
                        + '</td>'
                        + '<td>'
                        + '<input class="F_EVALUATION_POINTS_show" name="F_EVALUATION_POINTS" onkeyup="staff_input_check(this);" type="text"　class="EvaluationPoints" value="' + $result[i].F_EVALUATION_POINTS + '" />'
                        + '<label class="F_EVALUATION_POINTS_error" style="color:red;display:block"></label>'
                        + '</td>'
                        + '<td>'
                        + '<button type="button" onclick="jQuery(jQuery(this).parent().parent().children().hide())">×</button>'
                        + '</td>'
                        + '</tr>'
                    );
                }
                $('#modal-previewSelfEvaluate input[name="F_ITEM_NAME"]').val($result2[0].F_ITEM_NAME);
                endfunc();
            })
            .fail(function (jqXHR, textStatus, errorThrown) {
                //jQuery('#popup_search_erorr_message_title').show();
                //console.log(jqXHR, textStatus, errorThrown, arguments);
                alert('error');
            });

    };

    //ダイアログ 項目を追加
    this.Add = function () {
        $('#ErrorMessageSelfEvaluate').html('');
        $F_SELF_EVALUATE_ID = $('#modal-previewStaff input[name="F_SELF_EVALUATE_ID"]').val();
        $('#choice-table').append(
            '<tr>'
            + '<td>'
            + '<input name="F_ID" type="hidden" class="Id" value="" />'
            + '<input name="F_SELF_EVALUATE" type="hidden" class="SelfEvaluate" value="' + $F_SELF_EVALUATE_ID + '" />'
            + '<input name="F_CHOICE_NAME" type="text" class="ChoiceName" value="" />'
            + '</td>'
            + '<td>'
            + '<input class="F_EVALUATION_POINTS_insert" name="F_EVALUATION_POINTS" onkeyup="staff_input_check(this);" type="text" class="EvaluationPoints" value="" />'
            + '</td>'
            + '<td>'
            + '<button type="button" onclick="jQuery(jQuery(this).parent().parent().children().hide())">×</button>'
            + '</td>'
            + '</tr>'
        );
    };

    //ダイアログ 保存ボタン
    this.Save = function () {
        //if (confirm('保存しますか？')) {
            //入力データの整形
            var $record = new Object();

            $record['F_ID'] = $('#modal-previewSelfEvaluate input[name="F_SELF_EVALUATE_ID"]').val();
            $record['F_ITEM_NAME'] = $('#modal-previewSelfEvaluate input[name="F_ITEM_NAME"]').val();

            // テーブルデータの取得
            var data = [];
            var deldata = [];
            var errflg = false;
            var message = '';

            var tr = $("#choice-table tr");//全行を取得
            for (var i = 0, l = tr.length; i < l; i++) {

                if ($(tr[i].innerHTML).css('display') !== 'none') {
                    // 表示されている場合の処理
                    var cells = tr.eq(i).find('input');//1行目から順にth、td問わず列を取得
                    for (var j = 0, m = cells.length; j < m; j++) {
                        if (typeof data[i] === "undefined")
                            data[i] = [];
                        data[i][j] = cells.eq(j).val();//i行目j列の文字列を取得
                    }
                }
                else {
                    // 非表示の場合の処理 
                    var cells2 = tr.eq(i).find('input');//1行目から順にth、td問わず列を取得
                    for (var j2 = 0, m2 = cells2.length; j2 < m2; j2++) {
                        if (typeof deldata[i] === "undefined")
                            deldata[i] = [];
                        deldata[i][j2] = cells2.eq(j2).val();//i行目j列の文字列を取得
                    }
                }

            }
            // エラーチェック
            if ($('#modal-previewSelfEvaluate input[name="F_ITEM_NAME"]').val() === '') {
                errflg = true;
                message += '項目名を入力してください。<br />';
            }
            data.forEach(function (val, index) {
                if (val[2] === '') {
                    errflg = true;
                    message += index + '行目：選択肢名を入力してください。<br />';
                }
                if (!$.isNumeric(val[3])) {
                    errflg = true;
                    message += index + '行目：評価ポイントは数値を入力してください。<br />';
                }
                else if (val[3] < 0 || 101 < val[3]) {
                    errflg = true;
                    message += index + '行目：評価ポイントは0～100までの数値を入力してください。<br />';
                }
            });
            if (errflg) {
                // アラート表示
                $('#ErrorMessageSelfEvaluate').html(message);
                //alert(message);
                return;
            }
            $record['data'] = data;
            $record['deldata'] = deldata;

            //更新処理 ajaxで保存
            $.ajax({
                url: this.ajaxurl,
                type: 'POST',
                data: {
                    // 呼び出すアクション名
                    action: 'my_ajax_get_posts',
                    // アクションに対応するnonce
                    secure: this.CSRF,
                    // 入力データ
                    params: $record,
                    module: 'request_yk_self_evaluate_save'
                }
            })
                .done(function (res) {
                    //保存後の処理
                    location.reload();
                    //alert('保存しました。');
                })
                .fail(function (jqXHR, textStatus, errorThrown) {
                    alert('error');
                });
        //} else {
        //    // キャンセル
        //    location.reload;
        //}
    };

    $(document).ready(function () {
        
    });
    function orgRound(value, base) {
        return Math.round(value * base) / base;
    }
    function separate(num) {
        // 文字列にする
        num = String(num);

        var len = num.length;

        // 再帰的に呼び出すよ
        if (len > 3) {
            // 前半を引数に再帰呼び出し + 後半3桁
            return separate(num.substring(0, len - 3)) + ',' + num.substring(len - 3);
        } else {
            return num;
        }
    }
    function no_kanma(input) {
        return Number(String(input).replace(/,/g, ''));
    }
    // 入力チェック（個人評価）
    staff_input_check = function (item) {
         
        var show_input = $($(item).parent().find('.F_EVALUATION_POINTS_show')[0]);
        var error_message = $($(item).parent().find('.F_EVALUATION_POINTS_error')[0]);

        // エラークリア
        error_message.html('');

        // 空白はそのまま
        if (show_input.val() === '') {
            return false;
        }

        // 0以上整数のみ
        var pattern = /^([1-9]\d*|0)$/;
        if (!pattern.test(show_input.val())) {
            show_input.val(parseInt( show_input.val().replace(/[^0-9]/g, '')));
            error_message.html('数値を入力してください');
            return;
        }

        // 小数第一位を四捨五入
        var num = no_kanma(show_input.val());
        num = orgRound(parseFloat(num), 1);
        num = parseInt(num);
        if (3 < String(num).length) {
            show_input.val(String(num).slice(0,3));
            error_message.html('3桁までしか入力できません');
            return;
        }
        show_input.val(num);
    };
};

var dialog_previewSelfEvaluate = new dialog_previewSelfEvaluate_class(jQuery);


