
/**
 * ホーム画面
 */

$ = jQuery;
var yk_input_class = function (jQuery) {

    const staff_group = new Array();
    const ShopArray = new Array();
    const ShopStaffArray = new Array();

    const CSRF = "";
    const ajaxurl = "";
    // Ajax（計算実行）
    this.exe_ajax = function () {
        var prm = new Object();
        $.ajax({
            url: this.ajaxurl,
            type: 'POST',
            data: {
                // 呼び出すアクション名
                action: 'my_ajax_get_posts',
                // アクションに対応するnonce
                secure: this.CSRF,
                // 入力データ
                params: prm,
                module: 'request_get_yk_calc'
            }
        })
            .done(function (res) {
                $result = res['result'];
                if (res['code'] === 1) {
                    console.log('ok');
                }
                else {
                    console.log('ng');
                }
            })
            .fail(function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR, textStatus, errorThrown, arguments);
            });

    };
    //---------------------------------
    // ※画面から取得するときは×1000
    // ※画面に設定するときは÷1000
    //---------------------------------
    var set_val = function (val) {
        return parseFloat(val) / 1000;
    }
    var get_val = function (val) {
        return parseFloat(val) * 1000;
    }
    // 店舗評価の自動計算（左から右へ）
    var calc_to_right = function () {
        // tr取得
        var tr = $(this).parent().parent()[0];
        var left_input = $(tr).find('.left_input');

        // エラークリア
        var valid_shop_left = $(tr).find('.valid_shop_left');
        valid_shop_left.html('');
        var valid_shop_right = $(tr).find('.valid_shop_right');
        valid_shop_right.html('');

        // 各店半期売上予測（千円）
        var num = no_kanma(left_input.val());
        if (!$.isNumeric(num)) {
            valid_shop_left.html('数値を入力してください。');
            return;
        }
        else if (5 < String(num).length) {
            valid_shop_left.html('5桁までしか入力できません。');
            num = String(num).slice(0, 5);
        }
        left_input.val(separate(num));

        $(tr).find('.left_input_hidden').val(get_val(num));
        var shop_budg_hidden = $(tr).find('.shop_budg_hidden');
        var parsed_int = parseFloat(shop_budg_hidden.val());
        var parsed_left = get_val(num);

        var right_input = $(tr).find('.right_input');
        right_input.val(orgRound((parsed_left / parsed_int) * 100,100));

    };

    // 店舗評価の自動計算（左から右へ）
    var calc_to_left = function () {
        // tr取得
        var tr = $(this).parent().parent()[0];
        var right_input = $(tr).find('.right_input');

        // エラークリア
        var valid_shop_right = $(tr).find('.valid_shop_right');
        valid_shop_right.html('');
        var valid_shop_left = $(tr).find('.valid_shop_left');
        valid_shop_left.html('');

        // 各店半期売上予測（千円）
        if (right_input.val().slice(-1) === '.') {
            return;
        }
        else if (!$.isNumeric(right_input.val())) {
            valid_shop_right.html('数値を入力してください。');
            return;
        } else if (parseFloat(right_input.val()) < 0 ) {
            valid_shop_right.html('0以上の数値を入力してください。');
            return;
        }
        var left_input_hidden = $(tr).find('.left_input_hidden');
        var left_input = $(tr).find('.left_input');
        var shop_budg_hidden = $(tr).find('.shop_budg_hidden');
        var kkk = parseFloat(shop_budg_hidden.val()) * (parseFloat(right_input.val()) * 0.01);
        left_input.val(separate(orgRound(set_val(kkk), 1)));
        left_input_hidden.val(orgRound(kkk, 1));

        left_input.keyup();

    };

    // 入力チェック用
    var check_percent = function () {
        var input = $('#' + this.id);
        var valid = $('#valid-' + this.id);
        if (!$.isNumeric(input.val())) {
            valid.html('数値を入力してください。');
            return;
        } else if (parseFloat(input.val()) < 0 || 100 < parseFloat(input.val())) {
            valid.html('0～100までの数値を入力してください。');
            return;
        }
        valid.html('');
    };

    // 入力チェック用
    var check_count = function () {
        var input = $('#' + this.id);
        var valid = $('#valid-' + this.id);
        var num = no_kanma(input.val());
        if (!$.isNumeric(num)) {
            valid.html('数値を入力してください。');
            return;
        } else if (parseFloat(num) < 0 ) {
            valid.html('0以上の数値を入力してください。');
            return;
        }
        valid.html('');
    };


/**
 * 半期売上予算（千円）はPHPのforeachでM_SHOPのF_PROVISION_FUNDが1だったら足していく。
 * M_SHOPのF_PROVISION_FUNDが0のスタジオチームは可算されない。
 * 半期売上予測（千円）は、jsのみで計算していてF_PROVISION_FUNDの条件が入っていないので全て加算されている。
 * めんどいので最後にスタジオの各店半期売上予測（千円）を
 * 引いちゃいます。
 * F_PROVISION_FUNDが0の店舗が増えたら問題が発生します。
 * その場合は、配列で { 金額, F_PROVISION_FUND}を生成してPHP同様F_PROVISION_FUNDを判定するように計算してください。
*/
    this.calc_exe = function () {
        // ▼半期売上予測（千円）
        var shop_provision_fund_array = [];
        $('.shop_provision_fund_hidden').each(function (index, element) {
            var provision_fund = $(element).val();
            shop_provision_fund_array.push(provision_fund);
        });

        var sum = 0;
        $('.left_input_hidden').each(function (index, element) {
            if (shop_provision_fund_array[index] === '1') {
                var input = $(element).val();
                if (input) {
                    sum += parseFloat(input);
                }
            }
        });
        $('#calc_all_yosou').html(separate(set_val(sum))+'千円');

        // ▼全社予算達成率
        var calc_all_yosan = $('#calc_all_yosan_hidden').val();
        var calc_studio_yosan = $('#calc_studio_yosan_hidden').val();
        var calc_all_tasei = calc_all_yosan === 0 ? 0 : sum / (calc_all_yosan - calc_studio_yosan) * 100;
        $('#calc_all_tasei').html(orgRound(calc_all_tasei,10) + '%');


        // ▼店舗予算（選択した従業員の所属店舗）
        //選択した人IDを取得

        var tempoyosan = 0;
        var tempoyosou = 0;
        var position_rate = 0;
        $(this.ShopArray).each(function (index, element) {
            if (element['userid'] === $("#staffs").val()) {
                tempoyosan += parseFloat(element.shopbudget);
                tempoyosou += parseFloat($('#shop_' + element.shopid).find('.left_input_hidden').val());
                position_rate = parseFloat(element.positionrate);
            }
        });
        $('#calc_tempoyosan').html(separate(set_val(tempoyosan)) + '千円');
        $('#calc_tempoyosoku').html(separate(set_val(tempoyosou)) + '千円');
        $('#calc_tempotasei').html(tempoyosan === 0 ? '0%' : orgRound(tempoyosou / tempoyosan * 100, 100) + '％');

        // -------------
        // 支給賞与予測
        // -------------

        // ▼インセンティブ
        var my_incentive= 0;
        $('.calc-incentive').each(function (index, element) {
            var input = $(element).val();
            if (input) {
                my_incentive += parseFloat(input);
            }
        });
        $('#calc_incentive').html(separate(my_incentive)+"円");

        // ▼売上評価
        // 原資を算出
        var calc_all_yosan_gensi = parseFloat($('#calc_all_yosan_gensi').html());

        var sum_enable = 0;
        $('.sum_enable').each(function (index, element) {
            var input = $(element).val();
            if (input) {
                sum_enable += parseFloat(input);
            }
        });
        var gensi = cal_gensi(sum_enable / calc_all_yosan_gensi * 100, calc_all_yosan_gensi);
        // 原資から差し引くインセンティブを計算。選択職員以外の規定のインセンティブを取得
        var staff_group = new Array();
        for (var iii = 0; iii < this.ShopArray.length; iii++) {
            element = this.ShopArray[iii];
            var finditem = null;
            for (var tempi = 0; tempi < staff_group.length; tempi++) {
                var p = staff_group[tempi];
                if (p.userid === element.userid) {
                    finditem = p;
                    break;
                }
            }
            

            if (finditem) {
                finditem.shops.push({ shopid: element.shopid, shopname: element.shopname, shopbudget: element.shopbudget, shopfund: element.shopfund });
                // ここでShopを回して平均値から売上評価係数を算出
                finditem.tenpohyoukakeisu_noncal += parseFloat($('#shop_' + element.shopid).find('.right_input').val()) / 100;
                finditem.tenpohyoukakeisu = finditem.tenpohyoukakeisu_noncal / finditem.shops.length * element.positionrate;

                // ここでShopを回して平均値から個人評価係数を算出（店長のみ）いやむり、全員揃わないとむり

            } else {
                staff_group.push({
                    userid: element.userid,
                    username: element.username,
                    incentive: parseFloat(element.incentive),
                    selfpoint: parseFloat(element.selfpoint),
                    positionid: element.positionid,
                    positionname: element.positionname,
                    positionrate: element.positionrate,
                    tenpohyoukakeisu: parseFloat($('#shop_' + element.shopid).find('.right_input').val()) / 100 * element.positionrate,
                    tenpohyoukakeisu_noncal: parseFloat($('#shop_' + element.shopid).find('.right_input').val()) / 100 ,
                    shops: new Array({ shopid: element.shopid, shopname: element.shopname, shopbudget: element.shopbudget, shopfund: element.shopfund })
                });
            }
        }
        var sum_incentive = 0;
        for (var n = 0; n < staff_group.length; n++) {
            element = staff_group[n];
            if (element['userid'] === $("#staffs").val()) {
                sum_incentive += my_incentive;
            }
            else {
                sum_incentive += element.incentive;
            }
        }

        var uriage_gensi = (gensi - sum_incentive) * 0.8;
        // 全店舗の合計と自身の係数を取り出す
        var allshop_keisu = 0;
        var myshop_keisu = 0;
        $(staff_group).each(function (index, element) {
            if (element['userid'] === $("#staffs").val()) {
                myshop_keisu = element.tenpohyoukakeisu;
            }
            allshop_keisu += element.tenpohyoukakeisu;
        });
        var calc_uriagehyouka = gensi === 0 ? 0 : uriage_gensi / allshop_keisu * myshop_keisu;
        $('#calc_uriagehyouka').html(separate(orgRound(calc_uriagehyouka, 1))+'円');

        // ▼個人評価
        var kojin_gensi = (gensi - sum_incentive) * 0.2;

        var sum_selfpoint = 0;
        var myself_keisu = 0;
        for (n = 0; n < staff_group.length; n++) {
            element = staff_group[n];
            if (element['userid'] === $("#staffs").val()) {
                if (parseFloat(element.positionrate) === 2) {

                    // 所属店舗メンバーの平均
                    var syozoku_sum = 0;
                    var syozoku_sum_count = 0;
                    for (nn = 0; nn < element.shops.length; nn++) {
                        var self_shop = element.shops[nn];
                        var flg = false;
                        for (nnn = 0; nnn < staff_group.length; nnn++) {
                            var self_staff = staff_group[nnn];
                            if (self_staff.userid === element.userid)
                                continue;
                            for (nnnn = 0; nnnn < self_staff.shops.length; nnnn++) {
                                if (self_shop.shopid === self_staff.shops[nnnn].shopid) {
                                    syozoku_sum += self_staff.selfpoint;
                                    syozoku_sum_count++;
                                    break;
                                }
                            }
                        }
                    }
                    myself_keisu = (syozoku_sum / syozoku_sum_count) * parseFloat(element.positionrate);
                    sum_selfpoint += myself_keisu;
                }
                else {
                    for (nn = 0; nn < $('.self_evaluate').length; nn++) {
                        var tempval = parseFloat($('.self_evaluate')[nn].value);
                        sum_selfpoint += tempval;
                        myself_keisu += tempval;
                    }
                }
            }
            else {
                sum_selfpoint += element.selfpoint;
            }
        }
        var calc_selfhyouka = gensi === 0 ? 0 : (kojin_gensi / sum_selfpoint) * myself_keisu;
        $('#calc_selfhyouka').html(separate(orgRound(calc_selfhyouka, 1))+'円');
        $('#calc_allresult').html(separate(orgRound(calc_uriagehyouka + calc_selfhyouka + my_incentive, 1))+'円');


    };
    var cal_gensi = function (calc_all_tasei, calc_all_yosan) {

        if (110 <= calc_all_tasei) {
            return calc_all_yosan * 0.02250;
        }
        else if (109 <= calc_all_tasei) {
            return calc_all_yosan * 0.02175;
        }
        else if (108 <= calc_all_tasei) {
            return calc_all_yosan * 0.02100;
        }
        else if (107 <= calc_all_tasei) {
            return calc_all_yosan * 0.02025;
        }
        else if (106 <= calc_all_tasei) {
            return calc_all_yosan * 0.01950;
        }
        else if (105 <= calc_all_tasei) {
            return calc_all_yosan * 0.01875;
        }
        else if (104 <= calc_all_tasei) {
            return calc_all_yosan * 0.01800;
        }
        else if (103 <= calc_all_tasei) {
            return calc_all_yosan * 0.01725;
        }
        else if (102 <= calc_all_tasei) {
            return calc_all_yosan * 0.01650;
        }
        else if (101 <= calc_all_tasei) {
            return calc_all_yosan * 0.01575;
        }
        else if (100 <= calc_all_tasei) {
            return calc_all_yosan * 0.01500;
        }
        else if (99 <= calc_all_tasei) {
            return calc_all_yosan * 0.01425;
        }
        else if (98 <= calc_all_tasei) {
            return calc_all_yosan * 0.01350;
        }
        else if (97 <= calc_all_tasei) {
            return calc_all_yosan * 0.01275;
        }
        else if (96 <= calc_all_tasei) {
            return calc_all_yosan * 0.01200;
        }
        else if (95 <= calc_all_tasei) {
            return calc_all_yosan * 0.01125;
        }
        else if (94 <= calc_all_tasei) {
            return calc_all_yosan * 0.01050;
        }
        else if (93 <= calc_all_tasei) {
            return calc_all_yosan * 0.00975;
        }
        else if (92 <= calc_all_tasei) {
            return calc_all_yosan * 0.00900;
        }
        else if (91 <= calc_all_tasei) {
            return calc_all_yosan * 0.00825;
        }
        else if (90 <= calc_all_tasei) {
            return calc_all_yosan * 0.00750;
        }
        else {
            return 0;
        }
    };
    // 画面読込時
    jQuery(function () {

        var minsalses = $('.min-sales');
        for (var i = 0; i < minsalses.length; i++) {
            var tempitem = minsalses[i];
            $(tempitem).html(orgRound($(tempitem).html(), 1) +'人');
        }
        $('#calc_all_yosan').html(separate($('#calc_all_yosan').html()) + '千円');

        //---------------------------------
        // 
        //---------------------------------
        $('#staffs').change(function () {

            var r = $('option:selected').attr('position');
            if (parseFloat(r) === 2) {
                $('#selfpanel').hide();
            } else {
                $('#selfpanel').show();
            }
            
        });



        //---------------------------------
        // 店舗評価（入力チェック）
        //---------------------------------
        $('.left_input').keyup(calc_to_right);
        $('.right_input').keyup(calc_to_left);

        //---------------------------------
        // 個人評価（ポイント表示）
        //---------------------------------
        $(".self_evaluate").change(function () {
            var str = $(this).val();
            var shop_point = $(this).parent().parent().find('.shop_point');
            shop_point.html(str+'ポイント');
        });

        //---------------------------------
        // インセンティブ（入力チェック）
        //---------------------------------
        // 振袖成約率（％） 
        $('#furi-ritu').keyup(check_percent);

        // 振袖成約数（件） 
        $('#furi-count').keyup(check_count);

        // 袴成約率（％）
        $('#hakama-ritu').keyup(check_percent);

        // 袴成約数（件）
        $('#hakama-count').keyup(check_count);

        // フォローレター数（通）
        $('#letter-count').keyup(check_count);

        // 紹介客獲得数（件）
        $('#getcustomer-count').keyup(check_count);

        // 振袖撮影件数（件）
        $('#camera-count').keyup(check_count);

        // 振袖撮影売上平均単価（円）
        $('#camera-average').keyup(check_count);

        //---------------------------------
        // インセンティブ（自動計算）
        //---------------------------------
        // 振袖成約率（％） 
        $('#furi-ritu').keyup(function () {
            var input = $('#' + this.id);
            if (!$.isNumeric(input.val())) {
                return;
            } else if (parseFloat(input.val()) < 0 || 100 < parseFloat(input.val())) {
                return;
            }

            var result_view = $('#calc-furi-ritu');
            var calc_furi_ritu_hidden = $('#calc-furi-ritu_hidden');
            var num = parseFloat(input.val());
            if (90 <= num) {
                result_view.html(separate(200000)+'円');
                calc_furi_ritu_hidden.val(200000);
            }
            else if (85 <= num) {
                result_view.html(separate(100000) + '円');
                calc_furi_ritu_hidden.val(100000);
            }
            else if (80 <= num) {
                result_view.html(separate(50000) + '円');
                calc_furi_ritu_hidden.val(50000);
            }
            else if (75 <= num) {
                result_view.html(separate(30000) + '円');
                calc_furi_ritu_hidden.val(30000);
            }
            else{
                result_view.html(separate(0) + '円');
                calc_furi_ritu_hidden.val(0);
            }
        });

        // 振袖成約数（件） 
        $('#furi-count').keyup(function () {
            var input = $('#' + this.id);
            if (!$.isNumeric(input.val())) {
                return;
            } else if (parseFloat(input.val()) < 0 ) {
                return;
            }

            var result_view = $('#calc-furi-count');
            var calc_furi_count_hidden = $('#calc-furi-count_hidden');
            var num = parseFloat(input.val());
            if (25 <= num) {
                result_view.html(separate(num * 500)+'円');
                calc_furi_count_hidden.val(num * 500);
            }
            else {
                result_view.html('0円');
                calc_furi_count_hidden.val(0);
            }
        });

        // 袴成約率（％）
        $('#hakama-ritu').keyup(function () {
            var input = $('#' + this.id);
            if (!$.isNumeric(input.val())) {
                return;
            } else if (parseFloat(input.val()) < 0 || 100 < parseFloat(input.val())) {
                return;
            }

            var result_view = $('#calc-hakama-ritu');
            var calc_hakama_ritu_hidden = $('#calc-hakama-ritu_hidden');
            var num = parseFloat(input.val());
            if (95 <= num) {
                result_view.html(separate(50000) + '円');
                calc_hakama_ritu_hidden.val(50000);
            }
            else if (90 <= num) {
                result_view.html(separate(30000) + '円');
                calc_hakama_ritu_hidden.val(30000);
            }
            else if (85 <= num) {
                result_view.html(separate(10000) + '円');
                calc_hakama_ritu_hidden.val(10000);
            }
            else if (80 <= num) {
                result_view.html(separate(5000) + '円');
                calc_hakama_ritu_hidden.val(5000);
            }
            else {
                result_view.html(separate(0) + '円');
                calc_hakama_ritu_hidden.val(0);
            }
        });

        // 袴成約数（件）
        $('#hakama-count').keyup(function () {
            var input = $('#' + this.id);
            if (!$.isNumeric(input.val())) {
                return;
            } else if (parseFloat(input.val()) < 0) {
                return;
            }

            var result_view = $('#calc-hakama-count');
            var calc_hakama_count_hidden = $('#calc-hakama-count_hidden');
            var num = parseFloat(input.val());
            if (35 <= num) {
                result_view.html(separate(num * 300)+'円');
                calc_hakama_count_hidden.val(num * 300);
            }
            else {
                result_view.html(separate(0) + '円');
                calc_hakama_count_hidden.val(0);
            }
        });

        // フォローレター数（通）
        $('#letter-count').keyup(function () {
            var input = $('#' + this.id);
            if (!$.isNumeric(input.val())) {
                return;
            } else if (parseFloat(input.val()) < 0) {
                return;
            }

            var result_view = $('#calc-letter-count');
            var calc_letter_count_hidden = $('#calc-letter-count_hidden');
            var num = parseFloat(input.val());
            if (60 <= num) {
                result_view.html(separate(num * 200)+'円');
                calc_letter_count_hidden.val(num * 200);
            }
            else if (40 <= num) {
                result_view.html(separate(num * 150) + '円');
                calc_letter_count_hidden.val(num * 150);
            }
            else if (20 <= num) {
                result_view.html(separate(num * 100) + '円');
                calc_letter_count_hidden.val(num * 100);
            }
            else {
                result_view.html(separate(0) + '円');
                calc_letter_count_hidden.val(0);
            }
        });

        // 紹介客獲得数（件）
        $('#getcustomer-count').keyup(function () {
            var input = $('#' + this.id);
            if (!$.isNumeric(input.val())) {
                return;
            } else if (parseFloat(input.val()) < 0) {
                return;
            }

            var result_view = $('#calc-getcustomer-count');
            var calc_getcustomer_count_hidden = $('#calc-getcustomer-count_hidden');
            var num = parseFloat(input.val());
            if (30 <= num) {
                result_view.html(separate(num * 3500)+'円');
                calc_getcustomer_count_hidden.val(num * 3500);
            }
            else if (20 <= num) {
                result_view.html(separate(num * 2500) + '円');
                calc_getcustomer_count_hidden.val(num * 2500);
            }
            else if (15 <= num) {
                result_view.html(separate(num * 1500) + '円');
                calc_getcustomer_count_hidden.val(num * 1500);
            }
            else if (10 <= num) {
                result_view.html(separate(num * 1000) + '円');
                calc_getcustomer_count_hidden.val(num * 1000);
            }
            else {
                result_view.html(separate(0) + '円');
                calc_getcustomer_count_hidden.val(0);
            }
        });

        // 振袖撮影件数（件）
        $('#camera-count').keyup(function () {
            var input = $('#' + this.id);
            if (!$.isNumeric(input.val())) {
                return;
            } else if (parseFloat(input.val()) < 0) {
                return;
            }

            var result_view = $('#calc-camera-count');
            var calc_camera_count_hidden = $('#calc-camera-count_hidden');
            var num = parseFloat(input.val());
            if (200 <= num) {
                result_view.html(separate(50000)+'円');
                calc_camera_count_hidden.val(50000);
            }
            else if (180 <= num) {
                result_view.html(separate(30000)+'円');
                calc_camera_count_hidden.val(30000);
            }
            else if (150 <= num) {
                result_view.html(separate(20000)+'円');
                calc_camera_count_hidden.val(20000);
            }
            else {
                result_view.html(separate(0) + '円');
                calc_camera_count_hidden.val(0);
            }
        });
        // 振袖撮影売上平均単価（円）
        $('#camera-average').keyup(function () {
            var input = $('#' + this.id);
            var num = no_kanma(input.val());
            if (!$.isNumeric(num)) {
                return;
            } else if (parseFloat(num) < 0) {
                return;
            }

            input.val(separate(num));

            var result_view = $('#calc-camera-average');
            var calc_camera_average_hidden = $('#calc-camera-average_hidden');
            num = parseFloat(num);
            if (105000 <= num) {
                result_view.html(separate(80000)+'円');
                calc_camera_average_hidden.val(80000);
            }
            else if (100000<= num) {
                result_view.html(separate(50000) + '円');
                calc_camera_average_hidden.val(50000);
            }
            else if (95000 <= num) {
                result_view.html(separate(30000) + '円');
                calc_camera_average_hidden.val(30000);
            }
            else if (90000 <= num) {
                result_view.html(separate(20000) + '円');
                calc_camera_average_hidden.val(20000);
            }
            else {
                result_view.html(separate(0) + '円');
                calc_camera_average_hidden.val(0);
            }
        });

        for (var budg_i = 0; budg_i < $('.shop_budg').length; budg_i++) {
            var budg_temp = $('.shop_budg')[budg_i];
            var a = orgRound(parseFloat($(budg_temp).html()), 1);
            $(budg_temp).html(a);
        }

        for (var i = 0; i < $('.shop_budg').length; i++) {
            var budg = $('.shop_budg')[i];
            var t = separate($(budg).html()) + '千円';
            $(budg).html(t);

        }

    });

    function orgRound(value, base) {
        return Math.round(value * base) / base;
    }
    function separate(num) {
        // 文字列にする
        num = String(num);

        var len = num.length;

        // 再帰的に呼び出すよ
        if (len > 3) {
            // 前半を引数に再帰呼び出し + 後半3桁
            return separate(num.substring(0, len - 3)) + ',' + num.substring(len - 3);
        } else {
            return num;
        }
    }
    function no_kanma(input) {
        return Number(String(input).replace(/,/g, ''));
    }
};

var yk_input = new yk_input_class(jQuery);
