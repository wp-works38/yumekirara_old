<?php get_header(); ?>

<main id="page-home">
    <h1><?php esc_html_e(bloginfo('name'));?></h1>
    <?php
    $kengen = current_user_can( 'edit_users' );
    if($kengen)
    {
    ?>
    <a id="settings" href="<?php echo esc_url( home_url( '/settings' ) );?>" >システム設定</a>
    <?php
    }
    ?>
    <a id="input" href="<?php echo esc_url( home_url( '/input' ) );?>" >賞与計算スタート</a>
    <label id="message" style="color:red;">※ここで残出される算出される金額は支給確定金額ではありません。</label>
</main>

<?php get_footer(); ?>