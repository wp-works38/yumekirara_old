<?php

//Ajax呼び出し
class YK_Ajax
{
	/**
	 * Constructor
	 *
	 */
	public function __construct()
    {
	}


    //フロント側からのAjaxを処理する
    public function DoFunctionPublic()
    {
    }

    //管理者側のAjaxを処理する
    public function DoFunction()
    {
        ob_end_clean(); //ファイル破損を防ぐ //出力バッファのゴミ捨て


        if($_POST['module'] == 'request_get_yk_calc'){

            $nonce = $_POST['secure'];
            if ( ! wp_verify_nonce( $nonce, 'yk_input' ) )
            {
                // nonce 無効
                die( 'nonce 無効' );
            }

            header( 'Content-Type: application/json; charset=UTF-8' );
            $ret = array();
            $ret['code'] = 1;
            $ret['result'] = 'result!';
            echo json_encode($ret);
            return;
        }
        elseif($_POST['module'] == 'request_get_yk_settings'){

            $nonce = $_POST['secure'];
            if ( ! wp_verify_nonce( $nonce, 'yk_settings' ) )
            {
                // nonce 無効
                die( 'nonce 無効' );
            }

            header( 'Content-Type: application/json; charset=UTF-8' );
            $ret = array();
            $ret['code'] = 1;
            $ret['result'] = 'result!';
            echo json_encode($ret);
            return;
        }
		else if($_GET['module'] == 'admin_request_search_shop'){
			$search_text['searchkey'] = $_GET['searchkey'];
			$shops = YK_M_SHOP::Search($search_text);
			header( 'Content-Type: application/json; charset=UTF-8' );
			echo json_encode($shops);
			return;
		}
		else if($_GET['module'] == 'admin_request_search_staff'){
			$search_text['searchkey'] = $_GET['searchkey'];
			$staffs = YK_M_STAFF::Search($search_text);
			header( 'Content-Type: application/json; charset=UTF-8' );
			echo json_encode($staffs);
			return;
		}
		else if($_GET['module'] == 'admin_request_search_position'){
			$search_text['searchkey'] = $_GET['searchkey'];
			$positions = YK_M_POSITION::Search($search_text);
			header( 'Content-Type: application/json; charset=UTF-8' );
			echo json_encode($positions);
			return;
		}
		else if($_GET['module'] == 'admin_request_search_self_evaluate'){
			$search_text['searchkey'] = $_GET['searchkey'];
			$self_evaluates = YK_T_SELF_EVALUATE::Search($search_text);
			header( 'Content-Type: application/json; charset=UTF-8' );
			echo json_encode($self_evaluates);
		}
    }
}
