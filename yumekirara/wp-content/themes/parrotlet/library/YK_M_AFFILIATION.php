<?php
/**
 * YK_M_AFFILIATION short summary.
 *
 * YK_M_AFFILIATION description.
 *
 * @version 1.0
 * @author
 */
class YK_M_AFFILIATION
{
    //const TABLE_NAME = 'YK_M_AFFILIATION';
    private static function TABLE_NAME(){return $GLOBALS['table_prefix'].'YK_M_AFFILIATION';}

	public $F_SHOP_ID;// '店舗ID'
	public $F_STAFF_ID;// '従業員ID'
	public $F_POSITION_ID;// '役職マスタID'

	public $ShopMasterEntity;//  店舗マスタエンティティ
    public $PositionMasterEntity;//  職位マスタエンティティ
    public $StaffMasterEntity;//  職員マスタエンティティ


	private static function COLUMN_NAME(){
        $t=YK_M_AFFILIATION::TABLE_NAME();
        return "
     ${t}.F_ID as '${t}.F_ID'
    ,${t}.F_SHOP_ID as '${t}.F_SHOP_ID'
    ,${t}.F_STAFF_ID as '${t}.F_STAFF_ID'
    ,${t}.F_POSITION_ID as '${t}.F_POSITION_ID'
	";
	}
    /** Constructor */
    public function __construct(){}
    public function save(){

        $record = array();
        $record['F_ID'] = YK_Common::CnvStrEmpty($this->F_ID);
        $record['F_SHOP_ID'] = YK_Common::CnvIntZero($this->F_SHOP_ID);
        $record['F_STAFF_ID'] = YK_Common::CnvIntZero($this->F_STAFF_ID);
        $record['F_POSITION_ID'] = YK_Common::CnvIntZero($this->F_POSITION_ID);

        global $wpdb;
        if ($this->F_ID == 0){
            $result = $wpdb->insert(YK_M_AFFILIATION::TABLE_NAME(), $record,
               array(
			    '%d'// F_ID
			   ,'%d'// F_SHOP_ID
			   ,'%d'// F_STAFF_ID
			   ,'%d'// F_POSITION_ID
			   ));

            if (!$result) {
                return false;
            }

            $insert_id = $wpdb->insert_id;
            $this->F_ID = $insert_id;
            return $insert_id;
        }
        else
        {
            //更新
            $where = array('F_ID' => $this->F_ID);
            $wpdb->update(YK_M_AFFILIATION::TABLE_NAME(),$record,$where,array(
			    '%d'// F_ID
			   ,'%d'// F_SHOP_ID
			   ,'%d'// F_STAFF_ID
			   ,'%d'// F_POSITION_ID
               )
			   ,array('%d'));
            if ($wpdb->last_error!='') {
                return false;
            }
            return $this->F_ID;
        }
    }
    public function delete(){
        global $wpdb;
        $where = array('F_ID' => $this->F_ID);
        $wpdb->delete( YK_M_AFFILIATION::TABLE_NAME(), $where, null );
        if ($wpdb->last_error!='') {
            return false;
        }
        return $this->F_ID;
    }

    public static function search($param){
        global $wpdb;
        $t = YK_M_AFFILIATION::TABLE_NAME();

        // ▼column
        $column = YK_M_AFFILIATION::COLUMN_NAME();
        if($param['inner_join_position']===true)
        {
            $column .= ', '.YK_M_POSITION::COLUMN_NAME().' ';
        }
        if($param['inner_join_staff']===true)
        {
            $column .= ', '.YK_M_STAFF::COLUMN_NAME().' ';
        }
        if($param['inner_join_shop']===true)
        {
            $column .= ', '.YK_M_SHOP::COLUMN_NAME().' ';
        }


        // ▼table
        $table = YK_M_AFFILIATION::TABLE_NAME();
        if($param['inner_join_position']===true)
        {
            $table = $table.' INNER JOIN '.YK_M_POSITION::TABLE_NAME().' ON ('.YK_M_POSITION::TABLE_NAME().'.F_ID = '.YK_M_AFFILIATION::TABLE_NAME().'.F_POSITION_ID) ';
        }
        if($param['inner_join_staff']===true)
        {
            $table = $table.' INNER JOIN '.YK_M_STAFF::TABLE_NAME().' ON ('.YK_M_STAFF::TABLE_NAME().'.F_ID = '.YK_M_AFFILIATION::TABLE_NAME().'.F_STAFF_ID) ';
        }
        if($param['inner_join_shop']===true)
        {
            $table = $table.' INNER JOIN '.YK_M_SHOP::TABLE_NAME().' ON ('.YK_M_SHOP::TABLE_NAME().'.F_ID = '.YK_M_AFFILIATION::TABLE_NAME().'.F_SHOP_ID) ';
        }

        // ▼Wrhere
        $where = self::build_where($param);

        // ▼Group By
        $group = '';
        //F_IDがあるからsqlでのグループ化は厳しい
        //if($param['group_staff_id']===true)
        //{
        //    $group = $group.' GROUP BY '.YK_M_AFFILIATION::TABLE_NAME().'.F_STAFF_ID ';
        //}

        // ▼OrderBy
        $order = "${t}.F_ID";
        if(isset($param['orderkey']))
        {
            $order='';
            foreach($param['orderkey'] as $_key)
            {
                $order.= "${t}.${_key},";
            }
            $order= $order===''?"${t}.F_ID":substr($order, 0, -1);
            $order .=' '.$param['orderdirection'];
        }

        // ▼result mode(count or select)
        if($param['mode'] == 'countup'){
            $ret = $wpdb->get_row("SELECT count(*) as count FROM $table WHERE $where $group", ARRAY_A);
            return $ret['count'];

        }
        else{
            $records = $wpdb->get_results("SELECT $column FROM $table WHERE $where $group ORDER BY $order", ARRAY_A);
        }
        if (empty($records)) {
			return array();
		}

        $arr = array();
        foreach ($records as $rec){
            $result = new self();
            self::db_to_property($rec, $result);
			if($param['inner_join_shop']===true)
            {
                $result->ShopMasterEntity = new YK_M_SHOP();
                YK_M_SHOP::db_to_property($rec, $result->ShopMasterEntity);
            }
            if($param['inner_join_position']===true)
            {
                $result->PositionMasterEntity = new YK_M_POSITION();
                YK_M_POSITION::db_to_property($rec, $result->PositionMasterEntity);
            }
            if($param['inner_join_staff']===true)
            {
                $result->StaffMasterEntity = new YK_M_STAFF();
                YK_M_STAFF::db_to_property($rec, $result->StaffMasterEntity);
            }
            if($param['inner_join_shop']===true)
            {
                $result->ShopMasterEntity = new YK_M_SHOP();
                YK_M_SHOP::db_to_property($rec, $result->ShopMasterEntity);
            }
            array_push($arr, $result);
        }
        return $arr;
    }

    private static function build_where($param){

        // ▼デフォルト条件
        //if(is_null($param['IsDelete']))
        //    $param['IsDelete']='0';

        $t = YK_M_AFFILIATION::TABLE_NAME();
        $where = "$t.F_ID <> 0 "; //ダミー

        //▼検索キーが指定されたらあいまい検索
        if(!empty( $param['searchkey']))
        {
            $key = esc_sql($param['searchkey']);
            $where = $where .
               " and ($t.F_SHOP_ID like '%" . $key . "%' or
					  $t.F_STAFF_ID like '%" . $key . "%' or
                      $t.F_POSITION_ID like '%" . $key . "%') ";
        }
        //▼日付範囲１
        //if(!empty( $param['StartDate']))
        //{
        //    $stDate = esc_sql($param['StartDate']);
        //    $where = $where .
        //       " and ($t.F_CREATE_DATETIME >= '" . $stDate . "') ";
        //}
        //if(!empty( $param['StartDate']))
        //{
        //    $edDate = esc_sql($param['EndDate']);
        //    $nextDate = date('Y-m-d', strtotime($edDate. ' + 1 days'));
        //    $where = $where .
        //       " and ($t.F_CREATE_DATETIME < '" . $nextDate . "') ";
        //}

        //▼日付範囲２
        //if(!empty( $param['Min_ModifyTime']))
        //{
        //    $modDate = esc_sql($param['Min_ModifyTime']);
        //    $where = $where .
        //       " and ($t.ModifyTime >= '" . $modDate . "') ";
        //}

        //▼各カラムの一致
		if(!is_null($param['F_ID']))$where = $where . " and $t.F_ID = " . esc_sql($param['F_ID']);
		if(!is_null($param['F_SHOP_ID']))$where = $where . " and $t.F_SHOP_ID = " . esc_sql($param['F_SHOP_ID']);
		if(!is_null($param['F_STAFF_ID']))$where = $where . " and $t.F_STAFF_ID = " . esc_sql($param['F_STAFF_ID']);
		if(!is_null($param['F_POSITION_ID']))$where = $where . " and $t.F_POSITION_ID = " . esc_sql($param['F_POSITION_ID']);        return $where;
    }
    public static function total_count($param){
        $param['mode'] = 'countup';
        return self::Search($param);
    }
    private static function db_to_property($owner_data, $result){
        $t = YK_M_AFFILIATION::TABLE_NAME();
		$result->F_ID = $owner_data["${t}.F_ID"];
		$result->F_SHOP_ID = $owner_data["${t}.F_SHOP_ID"];
		$result->F_STAFF_ID = $owner_data["${t}.F_STAFF_ID"];
		$result->F_POSITION_ID = $owner_data["${t}.F_POSITION_ID"];
    }

    public static function CreateTable()
    {
        global $wpdb;
        if(YK_Common::ExistTable(YK_M_AFFILIATION::TABLE_NAME()))
            return;

        $tablename = YK_M_AFFILIATION::TABLE_NAME();
        $q = "create table `$tablename` (
			  `F_ID` BIGINT unsigned not null AUTO_INCREMENT comment 'ID'
			  , `F_SHOP_ID` BIGINT not null comment '店舗ID'
			  , `F_STAFF_ID` BIGINT not null comment '従業員ID'
			  , `F_POSITION_ID` BIGINT not null comment '役職マスタID'
			  , constraint `YK_M_AFFILIATION_PKC` primary key (`F_ID`)
			) comment 'スタッフ所属管理マスタ' ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
			";

        $wpdb->query($q);
        if(!empty($wpdb->last_error)){
            echo "更新エラーが発生しました。";
            throw new Exception($wpdb->last_error);
        }
    }

    //ユニークなユーザーＩＤを作成する
    public function make_unique_id(){
        $str = substr( '0'.date_i18n('Y'), -2);  //下二ケタ
        //ランダム5桁 先頭０埋め
        $str = $str.substr( '0000'.mt_rand(111, 99000), -5);

        //存在確認
        $userdata = WP_User::get_data_by( 'login', $str );
        if(!$userdata)
            return $str; //存在しないので採用

        //既にあったので再起処理でもう一度
        return $this->make_unique_id();
    }

	//指定したＩＤの読み込み
    public static function Load($id, $param=null){
        if ($param == null)
            $param = array();
        $param['F_ID'] = $id;
        $ret = self::Search($param);
        if($ret == null || count($ret) <= 0)
        {
            return null;
        }
        return $ret[0];
    }
}
?>