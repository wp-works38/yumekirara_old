<?php
/**
 * YK_T_CHOICE short summary.
 *
 * YK_T_CHOICE description.
 *
 * @version 1.0
 * @author
 */
class YK_T_CHOICE
{
    //const TABLE_NAME = 'YK_T_CHOICE';
    public static function TABLE_NAME(){return $GLOBALS['table_prefix'].'YK_T_CHOICE';}

	public $F_ID;// 'ID'
	public $F_SELF_EVALUATE;// '個人評価（設問）ID'
	public $F_CHOICE_NAME;// '選択肢名'
	public $F_EVALUATION_POINTS;// '評価ポイント'

    public $SelfEvaluateEntity;//  設問（親）

    public static function COLUMN_NAME(){
        $t=YK_T_CHOICE::TABLE_NAME();
        return "
     ${t}.F_ID as '${t}.F_ID'
    ,${t}.F_SELF_EVALUATE as '${t}.F_SELF_EVALUATE'
    ,${t}.F_CHOICE_NAME as '${t}.F_CHOICE_NAME'
    ,${t}.F_EVALUATION_POINTS as '${t}.F_EVALUATION_POINTS'
	";
	}
    /** Constructor */
    public function __construct(){}
    public function save(){

        $record = array();
        $record['F_ID'] = YK_Common::CnvStrEmpty($this->F_ID);
        $record['F_SELF_EVALUATE'] = YK_Common::CnvIntZero($this->F_SELF_EVALUATE);
        $record['F_CHOICE_NAME'] = YK_Common::CnvStrEmpty($this->F_CHOICE_NAME);
        $record['F_EVALUATION_POINTS'] = YK_Common::CnvDecimal5_0($this->F_EVALUATION_POINTS);

        global $wpdb;
        if ($this->F_ID == 0){
            $result = $wpdb->insert(YK_T_CHOICE::TABLE_NAME(), $record,
               array(
			    '%d' // F_ID
			   ,'%d' // F_SELF_EVALUATE
			   ,'%s' // F_CHOICE_NAME
			   ,'%d' // F_EVALUATION_POINTS
			   ));

            if (!$result) {
                return false;
            }

            $insert_id = $wpdb->insert_id;
            $this->F_ID = $insert_id;
            return $insert_id;
        }
        else
        {
            //更新
            $where = array('F_ID' => $this->F_ID);
            $wpdb->update(YK_T_CHOICE::TABLE_NAME(),$record,$where,array(
			    '%d' // F_ID
			   ,'%d' // F_SELF_EVALUATE
			   ,'%s' // F_CHOICE_NAME
			   ,'%d' // F_EVALUATION_POINTS
               )
			   ,array('%d'));
            if ($wpdb->last_error!='') {
                return false;
            }
            return $this->F_ID;
        }
    }
    public function delete(){
        global $wpdb;
        $where = array('F_ID' => $this->F_ID);
        $wpdb->delete( YK_T_CHOICE::TABLE_NAME(), $where, null );
        if ($wpdb->last_error!='') {
            return false;
        }
        return $this->F_ID;
    }

    public static function search($param){
        global $wpdb;
        $t = YK_T_CHOICE::TABLE_NAME();

        // ▼column
        $column = YK_T_CHOICE::COLUMN_NAME();
        if($param['inner_join_self_evaluate']===true)
        {
            $column .= ', '.YK_T_SELF_EVALUATE::COLUMN_NAME().' ';
        }

        // ▼table
        $table = YK_T_CHOICE::TABLE_NAME();
        if($param['inner_join_self_evaluate']===true)
        {
            $table = $table.' INNER JOIN '.YK_T_SELF_EVALUATE::TABLE_NAME().' ON ('.YK_T_SELF_EVALUATE::TABLE_NAME().'.F_ID = '.self::TABLE_NAME().'.F_SELF_EVALUATE) ';
        }

        // ▼Wrhere
        $where = self::build_where($param);

        // ▼Group By
        $group = '';
        //if($param['leftjoin_menu_name']===true)
        //{
        //    $group = $group.' GROUP BY '.YK_T_CHOICE::TABLE_NAME().'.F_ID ';
        //}

        // ▼OrderBy
        $order = "${t}.F_ID";
        if(isset($param['orderkey']))
        {
            $order='';
            foreach($param['orderkey'] as $_key)
            {
                $order.= "${t}.${_key},";
            }
            $order= $order===''?"${t}.F_ID":substr($order, 0, -1);
            $order .=' '.$param['orderdirection'];
        }

        // ▼result mode(count or select)
        if($param['mode'] == 'countup'){
            $ret = $wpdb->get_row("SELECT count(*) as count FROM $table WHERE $where $group", ARRAY_A);
            return $ret['count'];

        }
        else{
            $records = $wpdb->get_results("SELECT $column FROM $table WHERE $where $group ORDER BY $order", ARRAY_A);
        }
        if (empty($records)) {
			return array();
		}

        $arr = array();
        foreach ($records as $rec){
            $result = new self();
            self::db_to_property($rec, $result);
            if($param['inner_join_self_evaluate']===true)
            {
                $result->SelfEvaluateEntity= new YK_T_SELF_EVALUATE();
                YK_T_SELF_EVALUATE::db_to_property($rec, $result->SelfEvaluateEntity);
            }
            array_push($arr, $result);
        }
        return $arr;
    }

    private static function build_where($param){

        // ▼デフォルト条件
        //if(is_null($param['IsDelete']))
        //    $param['IsDelete']='0';

        $t = YK_T_CHOICE::TABLE_NAME();
        $where = "$t.F_ID <> 0 "; //ダミー

        //▼検索キーが指定されたらあいまい検索
        if(!empty( $param['searchkey']))
        {
            $key = esc_sql($param['searchkey']);
            $where = $where .
               " and ($t.F_SELF_EVALUATE like '%" . $key . "%' or
                      $t.F_CHOICE_NAME like '%" . $key . "%' or
                      $t.F_EVALUATION_POINTS like '%" . $key . "%') ";
        }
        //▼日付範囲１
        //if(!empty( $param['StartDate']))
        //{
        //    $stDate = esc_sql($param['StartDate']);
        //    $where = $where .
        //       " and ($t.F_CREATE_DATETIME >= '" . $stDate . "') ";
        //}
        //if(!empty( $param['StartDate']))
        //{
        //    $edDate = esc_sql($param['EndDate']);
        //    $nextDate = date('Y-m-d', strtotime($edDate. ' + 1 days'));
        //    $where = $where .
        //       " and ($t.F_CREATE_DATETIME < '" . $nextDate . "') ";
        //}

        //▼日付範囲２
        //if(!empty( $param['Min_ModifyTime']))
        //{
        //    $modDate = esc_sql($param['Min_ModifyTime']);
        //    $where = $where .
        //       " and ($t.ModifyTime >= '" . $modDate . "') ";
        //}

        //▼各カラムの一致
        if(!is_null($param['F_ID']))$where = $where . " and $t.F_ID = " . esc_sql($param['F_ID']);
        if(!is_null($param['F_SELF_EVALUATE']))$where = $where . " and $t.F_SELF_EVALUATE = '" . esc_sql($param['F_SELF_EVALUATE']) . "' ";
        if(!is_null($param['F_CHOICE_NAME']))$where = $where . " and $t.F_CHOICE_NAME = '" . esc_sql($param['F_CHOICE_NAME']) . "' ";
        if(!is_null($param['F_EVALUATION_POINTS']))$where = $where . " and $t.F_EVALUATION_POINTS = '" . esc_sql($param['F_EVALUATION_POINTS']) . "' ";
        return $where;
    }
    public static function total_count($param){
        $param['mode'] = 'countup';
        return self::Search($param);
    }
    private static function db_to_property($owner_data, $result){
        $t = YK_T_CHOICE::TABLE_NAME();
        $result->F_ID = $owner_data["${t}.F_ID"];
        $result->F_SELF_EVALUATE = $owner_data["${t}.F_SELF_EVALUATE"];
        $result->F_CHOICE_NAME = $owner_data["${t}.F_CHOICE_NAME"];
        $result->F_EVALUATION_POINTS = $owner_data["${t}.F_EVALUATION_POINTS"];
    }

    public static function CreateTable()
    {
        global $wpdb;
        if(YK_Common::ExistTable(YK_T_CHOICE::TABLE_NAME()))
            return;

        $tablename = YK_T_CHOICE::TABLE_NAME();
        $q = "create table `$tablename` (
			  `F_ID` BIGINT unsigned not null AUTO_INCREMENT comment 'ID'
			  , `F_SELF_EVALUATE` BIGINT not null comment '個人評価（設問）ID'
			  , `F_CHOICE_NAME` VARCHAR(255) not null comment '選択肢名'
			  , `F_EVALUATION_POINTS` DECIMAL(5,0) not null comment '評価ポイント'
			  , constraint `TK_T_CHOICE_PKC` primary key (`F_ID`)
			) comment '個人評価（選択肢）' ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
			";

        $wpdb->query($q);
        if(!empty($wpdb->last_error)){
            echo "更新エラーが発生しました。";
            throw new Exception($wpdb->last_error);
        }
    }

    //ユニークなユーザーＩＤを作成する
    public function make_unique_id(){
        $str = substr( '0'.date_i18n('Y'), -2);  //下二ケタ
        //ランダム5桁 先頭０埋め
        $str = $str.substr( '0000'.mt_rand(111, 99000), -5);

        //存在確認
        $userdata = WP_User::get_data_by( 'login', $str );
        if(!$userdata)
            return $str; //存在しないので採用

        //既にあったので再起処理でもう一度
        return $this->make_unique_id();
    }


}
?>