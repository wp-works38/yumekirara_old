<?php

//Ajax呼び出し
class YK_Ajax
{
	/**
     * Constructor
     *
     */
	public function __construct()
    {
	}


    //フロント側からのAjaxを処理する
    public function DoFunctionPublic()
    {
    }

    //管理者側のAjaxを処理する
    public function DoFunction()
    {
        ob_end_clean(); //ファイル破損を防ぐ //出力バッファのゴミ捨て


        if($_POST['module'] == 'request_get_yk_calc'){

            $nonce = $_POST['secure'];
            if ( ! wp_verify_nonce( $nonce, 'yk_input' ) )
            {
                // nonce 無効
                die( 'nonce 無効' );
            }

            header( 'Content-Type: application/json; charset=UTF-8' );
            $ret = array();
            $ret['code'] = 1;
            $ret['result'] = 'result!';
            echo json_encode($ret);
            return;
        }
		//予算管理テーブル更新
        elseif($_POST['module'] == 'request_get_yk_settings'){

            $nonce = $_POST['secure'];
            if ( ! wp_verify_nonce( $nonce, 'yk_settings' ) )
            {
                // nonce 無効
                die( 'nonce 無効' );
            }

            header( 'Content-Type: application/json; charset=UTF-8' );
            $ret = array();
            $ret['code'] = 1;
            $ret['result'] = 'result!';
            echo json_encode($ret);
            return;
        }
		//店舗予算 更新
		elseif($_POST['module'] == 'request_get_yk_settings_shop_save'){

			$nonce = $_POST['secure'];
			if ( ! wp_verify_nonce( $nonce, 'yk_settings_shop' ) )
			{
			    // nonce 無効
			    die( 'nonce 無効' );
			}

			$prm = $_POST['params'];

			// 登録・更新
            $error_message = '';
            $shop_list = YK_M_SHOP::Search(array());
            for ($rowcount=0; $rowcount< count($prm['rows']); $rowcount++ )
            {
                // 1行データ取得
                $view_date = $prm['rows'][$rowcount];

                // 検索（名前だけの配列作成して、インデックス取得）
                $nameArray = array_column($shop_list, 'F_ID');
                $result = array_search(trim($view_date["F_ID"]), $nameArray);
                if($result===false)
                {
                    continue;
                }
                $shop = $shop_list[$result];

                // 更新する値をセット
                $shop->F_SHOP_BUDGET = $view_date["F_SHOP_BUDGET"];
                $shop->F_PROVISION_FUND = $view_date["F_PROVISION_FUND"]==='true' ? 1 : 0;

                // ▼入力チェック
                $is_error = false;
                if($shop->F_SHOP_BUDGET=='')
                {
                    $error_message.= ($rowcount."行目：店舗予算は必須です。\n");
			        $is_error = true;
                }
                if(!is_numeric($shop->F_SHOP_BUDGET))
                {
                    $error_message.= ($rowcount."行目：店舗予算は半角数字で入力してください。\n");
			        $is_error = true;
                }
                elseif(intval($shop->F_SHOP_BUDGET) < 0|| 1000000000 < intval($shop->F_SHOP_BUDGET)  )
                {
                    $error_message.= ($rowcount."行目：店舗予算は有効範囲を超えた入力です。\n");
			        $is_error = true;
                }


                // 更新実行
                if(!$is_error)
                {
                    $shop->Save();
                }
            }

            //$data = $prm['data'];
            //$checkdata = $prm['checkdata'];
            //foreach ($data as $key => $yk)
            //{
            //    if($yk=="")continue;

            //    $obj = null;
            //    if(empty($yk[0]) || $yk[0] == 0)
            //    {

            //    }else
            //    {
            //        //　更新
            //        $obj = YK_M_SHOP::Search(array('F_ID' =>$yk[0]));
            //        $obj[0]->F_SHOP_BUDGET = $yk[2];
            //        $obj[0]->F_PROVISION_FUND = $checkdata[$key][3] == "true" ? "1" : "0";
            //        $obj[0]->Save();
            //    }
            //}

            header( 'Content-Type: application/json; charset=UTF-8' );
            $ret = array();
            $ret['code'] =strlen($error_message)===0;
            $ret['result'] = $error_message;
            echo json_encode($ret);
            return;
        }
		//予測最低接客数 更新
		elseif($_POST['module'] == 'request_get_yk_settings_finance_save'){

			$nonce = $_POST['secure'];
			if ( ! wp_verify_nonce( $nonce, 'yk_settings_shop' ) )
			{
			    // nonce 無効
			    die( 'nonce 無効' );
			}

            $prm = $_POST['params'];

            // 更新する値をセット（空白はゼロとみなす）
			$obj = YK_T_FINANCE::Load();
			$obj->F_FURISODE= mb_strlen($prm['F_FURISODE'])==0?'0':$prm['F_FURISODE'];
			$obj->F_HAKAMA= mb_strlen($prm['F_HAKAMA'])==0?'0':$prm['F_HAKAMA'];

            // ▼入力チェック（振袖）
            $error_message = '';
            $is_error = false;
            if($obj->F_FURISODE=='')
            {
                $error_message.= ("振袖成約率は必須です。\n");
                $is_error = true;
            }
            elseif(!is_numeric($obj->F_FURISODE))
            {
                $error_message.= ("振袖成約率は半角数字で入力してください。\n");
                $is_error = true;
            }
            elseif(intval($obj->F_FURISODE) < 0|| 1000 < intval($obj->F_FURISODE)  )
            {
                $error_message.= ("振袖成約率は有効範囲を超えた入力です。\n");
                $is_error = true;
            }

            // ▼入力チェック（袴）
            if($obj->F_HAKAMA=='')
            {
                $error_message.= ("袴成約率は必須です。\n");
                $is_error = true;
            }
            elseif(!is_numeric($obj->F_HAKAMA))
            {
                $error_message.= ("袴成約率は半角数字で入力してください。\n");
                $is_error = true;
            }
            elseif(intval($obj->F_HAKAMA) < 0|| 1000 < intval($obj->F_HAKAMA)  )
            {
                $error_message.= ("袴成約率は有効範囲を超えた入力です。\n");
                $is_error = true;
            }

			//更新
            if(!$is_error)
            {
                $obj->Save();
            }

            header( 'Content-Type: application/json; charset=UTF-8' );
            $ret = array();
            $ret['is_error'] = $is_error;
            $ret['result'] = $is_error?$error_message:'';
            echo json_encode($ret);
            return;
        }
		//個人評価（設問）取得
		elseif($_POST['module'] == 'request_yk_self_evaluate_get'){

			$nonce = $_POST['secure'];
			if ( ! wp_verify_nonce( $nonce, 'yk_self_evaluate' ) )
			{
			    // nonce 無効
			    die( 'nonce 無効' );
			}

			$prm = $_POST['params'];
			$obj = YK_T_CHOICE::Search(array('F_SELF_EVALUATE' => $prm['F_ID']));
			$obj2 = YK_T_SELF_EVALUATE::Search(array('F_ID' => $prm['F_ID']));

			ob_end_clean(); //ファイル破損を防ぐ //出力バッファのゴミ捨て
            header( 'Content-Type: application/json; charset=UTF-8' );
            $ret = array();
            $ret['code'] = 1;
            $ret['result'] = $obj;
            $ret['result2'] = $obj2;
            echo json_encode($ret);
            return;
        }
		//個人評価（設問）保存
        else if($_POST['module'] == 'request_yk_self_evaluate_save'){

			$nonce = $_POST['secure'];
            if ( ! wp_verify_nonce( $nonce, 'yk_self_evaluate' ) )
            {
                // nonce 無効
                die( 'nonce 無効' );
            }

			$prm = $_POST['params'];
			$SelfEvaluate = null;
			if(empty($prm['F_ID']) || $prm['F_ID'] == 0)
			{
				// 新規追加
				$SelfEvaluate = new YK_T_SELF_EVALUATE();
				$SelfEvaluate->F_ITEM_NAME = $prm['F_ITEM_NAME'];
				$SelfEvaluate->Save();
			}
			else
			{
				// 更新
				$SelfEvaluate = YK_T_SELF_EVALUATE::Search(array('F_ID' => $prm['F_ID']));
				$SelfEvaluate[0]->F_ITEM_NAME = $prm['F_ITEM_NAME'];
				$SelfEvaluate[0]->Save();
			}

			// 登録・更新
			$data = $prm['data'];
			foreach ($data as $yk)
			{
				if($yk=="")continue;

				$Choice = null;
				if(empty($yk[0]) || $yk[0] == 0)
				{
					// 新規追加
					$F_ID = $prm['F_ID'];
					if(empty($F_ID) || $F_ID == 0)
					{
						//パラメータのF_SELF_EVALUATE_IDが空の場合は、上の新規追加で取得したF_IDをF_SELF_EVALUATEにセットする
						$F_ID = $SelfEvaluate->F_ID;
					}

					$Choice = new YK_T_CHOICE();
					$Choice->F_SELF_EVALUATE = $F_ID;
					$Choice->F_CHOICE_NAME = $yk[2];
					$Choice->F_EVALUATION_POINTS = $yk[3];
					$Choice->Save();
				}else
				{
					//　更新
					$Choice = YK_T_CHOICE::Search(array('F_ID' =>$yk[0]));
					$Choice[0]->F_SELF_EVALUATE = $yk[1];
					$Choice[0]->F_CHOICE_NAME = $yk[2];
					$Choice[0]->F_EVALUATION_POINTS = $yk[3];
					$Choice[0]->Save();
				}
			}

			//　削除
			$deldata = $prm['deldata'];
			foreach ($deldata as $yk)
			{
				if($yk=="")continue;

				$Choice = null;
				if(empty($yk[0]) || $yk[0] == 0)
				{
				}else
				{
					//　削除
					$Choice = YK_T_CHOICE::Search(array('F_ID' =>$yk[0]));
					$Choice[0]->delete();
				}
			}

            header( 'Content-Type: application/json; charset=UTF-8' );
            $ret = array();
            $ret['code'] = 1;
            $ret['result'] = '';
            echo json_encode($ret);
            return;
        }
		//個人評価（設問）全削除
        else if($_POST['module'] == 'request_yk_self_evaluate_all_delete'){
			$nonce = $_POST['secure'];
            if ( ! wp_verify_nonce( $nonce, 'yk_settings_self_evaluate' ) )
            {
                // nonce 無効
                die( 'nonce 無効' );
            }

            $prm = $_POST['params'];

			$SelfEvaluate = YK_T_SELF_EVALUATE::Search(array('F_ID' => $prm['F_ID']));
			$SelfEvaluate[0]->delete();
			$Choice = YK_T_CHOICE::Search(array('F_SELF_EVALUATE' =>$prm['F_ID']));
            $Choice[0]->delete();

			ob_end_clean(); //ファイル破損を防ぐ //出力バッファのゴミ捨て
            header( 'Content-Type: application/json; charset=UTF-8' );
            $ret = array();
            $ret['code'] = 1;
            $ret['result'] = '';
            echo json_encode($ret);
            return;
        }
		//職員マスタ 取得
        else if($_POST['module'] == 'request_yk_staff_get'){

			$nonce = $_POST['secure'];
            if ( ! wp_verify_nonce( $nonce, 'yk_staff' ) )
            {
                // nonce 無効
                die( 'nonce 無効' );
            }

			$id = $_POST['params']['id'];
            if(empty($id) || $id == 0)
            {
                header( 'Content-Type: application/json; charset=UTF-8' );
                $ret = array();
                $ret['code'] = 0;
                echo json_encode($ret);
                return;
            }
            $obj = YK_M_STAFF::Load($id);
			$Affiliation = YK_M_AFFILIATION::Search(array('F_STAFF_ID' => $obj->F_ID,'inner_join_position'=>true,'inner_join_staff'=>true,'inner_join_shop'=>true));
            $obj2 = $Affiliation[0]->PositionMasterEntity->F_POSITION_NAME;

			$shop_staff_group_array = array_reduce($Affiliation, function($result, $item) {
				$result[$item->F_STAFF_ID][] = $item;
				return $result;
			});
			$shops = '';
			foreach (array_keys($shop_staff_group_array) as $groupkey){
				$group = $shop_staff_group_array[$groupkey];
				foreach($group as $item)
				{
					$shops.= $item->ShopMasterEntity->F_SHOP_NAME.',';
				}
			}

			ob_end_clean(); //ファイル破損を防ぐ //出力バッファのゴミ捨て
            header( 'Content-Type: application/json; charset=UTF-8' );
            $ret = array();
            $ret['code'] = 1;
            $ret['result'] = $obj;
			$ret['result2'] = $obj2;
			$ret['result3'] = $shops;
            echo json_encode($ret);
            return;
        }
		//職員マスタ 保存
        else if($_POST['module'] == 'request_yk_staff_save'){

			$nonce = $_POST['secure'];
            if ( ! wp_verify_nonce( $nonce, 'yk_staff' ) )
            {
                // nonce 無効
                die( 'nonce 無効' );
            }

            $prm = $_POST['params'];

			// YK_M_STAFF（登録・更新）
			$staff = null;
            if(empty($prm['F_ID']) || $prm['F_ID'] == 0)
			{
                //新規登録
                $staff = new YK_M_STAFF();
            }
            else
			{
                //更新
                $staff = YK_M_STAFF::Load($prm['F_ID']);
            }
			$staff->F_LAST_NAME= mb_strlen($prm['F_LAST_NAME'])==0?'':$prm['F_LAST_NAME'];//  '氏'
			$staff->F_FAST_NAME= mb_strlen($prm['F_FAST_NAME'])==0?'':$prm['F_FAST_NAME'];//  '名'
			$staff->F_SHOP_ID= $prm['F_SHOP_ID'];//  '店舗マスタID'
			$staff->F_INCENTIVE= mb_strlen($prm['F_INCENTIVE'])==0?'0':$prm['F_INCENTIVE'];//  '既定のインセンティブ'
			$staff->F_SELF_EVALUATEF= mb_strlen($prm['F_SELF_EVALUATEF'])==0?'0':$prm['F_SELF_EVALUATEF'];//  '既定の個人評価'

            $is_error = false;
            $error_message='';
            // ▼入力チェック（役職'）
            if($prm['F_POSITION_ID']==''||$prm['F_POSITION_ID']==null||$prm['F_POSITION_ID']==false)
            {
                $error_message.= ("役職'は必須です。\n");
                $is_error = true;
            }
            // ▼入力チェック（店舗マスタID）
            if($staff->F_SHOP_ID=='' || count($staff->F_SHOP_ID)===0)
            {
                $error_message.= ("店舗選択は必須です。\n");
                $is_error = true;
            }
            // ▼入力チェック（既定のインセンティブ'）
            if($staff->F_INCENTIVE=='')
            {
                $error_message.= ("既定のインセンティブ'は必須です。\n");
                $is_error = true;
            }
            elseif(!is_numeric($staff->F_INCENTIVE))
            {
                $error_message.= ("既定のインセンティブ'は半角数字で入力してください。\n");
                $is_error = true;
            }
            elseif(intval($staff->F_INCENTIVE) < 0|| 99999999999 < intval($staff->F_INCENTIVE)  )
            {
                $error_message.= ("既定のインセンティブ'は有効範囲を超えた入力です。\n");
                $is_error = true;
            }
            // ▼入力チェック（既定の個人評価'）
            if($staff->F_SELF_EVALUATEF=='')
            {
                $error_message.= ("既定の個人評価'は必須です。\n");
                $is_error = true;
            }
            elseif(!is_numeric($staff->F_SELF_EVALUATEF))
            {
                $error_message.= ("既定の個人評価'は半角数字で入力してください。\n");
                $is_error = true;
            }
            elseif(intval($staff->F_SELF_EVALUATEF) < 0|| 999 < intval($staff->F_SELF_EVALUATEF)  )
            {
                $error_message.= ("既定の個人評価'は有効範囲を超えた入力です。\n");
                $is_error = true;
            }


            if(!$is_error)
            {
                $staff->Save();
            }else
            {
                header( 'Content-Type: application/json; charset=UTF-8' );
                $ret = array();
                $ret['is_error'] = $is_error;
                $ret['result'] = $is_error?$error_message:'';
                echo json_encode($ret);
                return;
            }

			// YK_M_AFFILIATION　所属店舗 checkboxが選択から外れた場合削除
			$OldShopIdList = $prm['F_SHOP_ID'];

			$Affiliation = YK_M_AFFILIATION::Search(array('F_STAFF_ID' => $staff->F_ID));
			$ShopIdList= array();
			foreach($Affiliation as $item)
			{
				// 現在のF_SHOP_IDを取得
				$ShopIdList[] = $item->F_SHOP_ID;
			}
			// 削除対象のF_SHOP_IDを取得
			$DeleteShopId = array_diff( $ShopIdList,$OldShopIdList);
			foreach($DeleteShopId as $item)
			{
				// 削除対象F_SHOP_IDで取得したデータを削除
				$Affiliation = YK_M_AFFILIATION::Search(array('F_STAFF_ID' => $staff->F_ID, 'F_SHOP_ID' => $item));
				$Affiliation[0]->delete();
			}

			// 選択したF_SHOP_IDを取得(登録・更新)
			foreach($OldShopIdList as $item)
			{
				$Affiliation = YK_M_AFFILIATION::Search(array('F_STAFF_ID' => $staff->F_ID, 'F_SHOP_ID' => $item));
				if(count($Affiliation) != 0)
				{
					if($Affiliation[0]->F_SHOP_ID == $item)
					{
						// データが存在する場合は更新
						$Affiliation[0]->F_SHOP_ID =$item;
						$Affiliation[0]->F_STAFF_ID = $staff->F_ID;
						$Affiliation[0]->F_POSITION_ID = $prm['F_POSITION_ID'];
						$Affiliation[0]->Save();
					}
				}
				else
				{
					//データが存在しない場合は新規登録
					$Affiliation = new YK_M_AFFILIATION();
					$Affiliation->F_SHOP_ID = $item;
					$Affiliation->F_STAFF_ID = $staff->F_ID;
					$Affiliation->F_POSITION_ID = $prm['F_POSITION_ID'];
					$Affiliation->Save();
				}

			}

			//if(empty($prm['F_ID']) || $prm['F_ID'] == 0)
			//{
			//    //新規登録
			//    $Affiliation = new YK_M_AFFILIATION();
			//    $Affiliation->F_SHOP_ID =$prm['F_SHOP_ID'];
			//    $Affiliation->F_STAFF_ID = $staff->F_ID;
			//    $Affiliation->F_POSITION_ID = $prm['F_POSITION_ID'];
			//    $Affiliation->Save();
			//}
			//else
			//{
			//    //更新
			//    $Affiliation = YK_M_AFFILIATION::Search(array('F_STAFF_ID' => $staff->F_ID));
			//    if(count($Affiliation) == 1)
			//    {
			//        $Affiliation[0]->F_SHOP_ID =$prm['F_SHOP_ID'];
			//        $Affiliation[0]->F_STAFF_ID = $staff->F_ID;
			//        $Affiliation[0]->F_POSITION_ID = $prm['F_POSITION_ID'];
			//        $Affiliation[0]->Save();
			//    }
			//    else
			//    {
			//        $Affiliation = YK_M_AFFILIATION::Search(array('F_STAFF_ID' => $staff->F_ID, 'F_SHOP_ID' => $staff->F_SHOP_ID));
			//        $Affiliation[0]->F_SHOP_ID =$prm['F_SHOP_ID'];
			//        $Affiliation[0]->F_STAFF_ID = $staff->F_ID;
			//        $Affiliation[0]->F_POSITION_ID = $prm['F_POSITION_ID'];
			//        $Affiliation[0]->Save();
			//    }
			//}

            header( 'Content-Type: application/json; charset=UTF-8' );
            $ret = array();
            $ret['is_error'] = $is_error;
            $ret['result'] = $is_error?$error_message:'';
            echo json_encode($ret);
            return;
        }
		//職員マスタ 削除
        else if($_POST['module'] == 'request_yk_staff_delete'){
			$nonce = $_POST['secure'];
            if ( ! wp_verify_nonce( $nonce, 'yk_staff' ) )
            {
                // nonce 無効
                die( 'nonce 無効' );
            }

            $prm = $_POST['params'];

			$Affiliation = YK_M_AFFILIATION::Search(array('F_STAFF_ID' => $prm['F_ID']));
			$staff = YK_M_STAFF::Load($prm['F_ID']);
			if(count($Affiliation) == 1)
			{
				$Affiliation[0]->delete();
				$staff->delete();
			}
			else
			{
				// 所属店舗が複数ある場合はユーザーは消さない
				$Affiliation = YK_M_AFFILIATION::Search(array('F_STAFF_ID' =>  $prm['F_ID'], 'F_SHOP_ID' => $staff->F_SHOP_ID));
				$Affiliation[0]->delete();
			}



			ob_end_clean(); //ファイル破損を防ぐ //出力バッファのゴミ捨て
            header( 'Content-Type: application/json; charset=UTF-8' );
            $ret = array();
            $ret['code'] = 1;
            $ret['result'] = '';
            echo json_encode($ret);
            return;
        }
		//職員マスタ 一括更新
        else if($_POST['module'] == 'request_get_yk_settings_staff_group_update'){

			$nonce = $_POST['secure'];
            if ( ! wp_verify_nonce( $nonce, 'yk_settings_staff' ) )
            {
                // nonce 無効
                die( 'nonce 無効' );
            }

            $prm = $_POST['params'];

            $is_error = false;
            $error_message='';
            // ▼入力チェック（役職'）
            if($prm['F_POSITION_ID']==''||$prm['F_POSITION_ID']==null||$prm['F_POSITION_ID']==false)
            {
                $error_message.= ("役職'は必須です。\n");
                $is_error = true;
            }
            // ▼入力チェック（既定のインセンティブ'）
            if($prm['F_INCENTIVE']=='')
            {
                $error_message.= ("既定のインセンティブ'は必須です。\n");
                $is_error = true;
            }
            elseif(!is_numeric($prm['F_INCENTIVE']))
            {
                $error_message.= ("既定のインセンティブ'は半角数字で入力してください。\n");
                $is_error = true;
            }
            elseif(intval($prm['F_INCENTIVE']) < 0|| 99999999999 < intval($prm['F_INCENTIVE'])  )
            {
                $error_message.= ("既定のインセンティブ'は有効範囲を超えた入力です。\n");
                $is_error = true;
            }
            // ▼入力チェック（既定の個人評価'）
            if($prm['F_SELF_EVALUATEF']=='')
            {
                $error_message.= ("既定の個人評価'は必須です。\n");
                $is_error = true;
            }
            elseif(!is_numeric($prm['F_SELF_EVALUATEF']))
            {
                $error_message.= ("既定の個人評価'は半角数字で入力してください。\n");
                $is_error = true;
            }
            elseif(intval($prm['F_SELF_EVALUATEF']) < 0|| 999 < intval($prm['F_SELF_EVALUATEF'])  )
            {
                $error_message.= ("既定の個人評価'は有効範囲を超えた入力です。\n");
                $is_error = true;
            }

            if($is_error)
            {
                header( 'Content-Type: application/json; charset=UTF-8' );
                $ret = array();
                $ret['is_error'] = $is_error;
                $ret['result'] = $is_error?$error_message:'';
                echo json_encode($ret);
                return;
            }

			$staffs = YK_M_AFFILIATION::search(array('F_POSITION_ID' => $prm['F_POSITION_ID'],'inner_join_staff'=>true));

			$obj = null;

			foreach ($staffs as  $yk){
				$obj = YK_M_STAFF::Search(array('F_ID' => $yk->StaffMasterEntity->F_ID));
				$obj[0]->F_INCENTIVE= $prm['F_INCENTIVE'];//  '既定のインセンティブ'
				$obj[0]->F_SELF_EVALUATEF = $prm['F_SELF_EVALUATEF'];//  '既定の個人評価'
				$obj[0]->Save();
			}

            header( 'Content-Type: application/json; charset=UTF-8' );
            $ret = array();
            $ret['code'] = 1;
            $ret['result'] = $obj;
            echo json_encode($ret);
            return;
        }
		else if($_GET['module'] == 'admin_request_search_shop'){
			$search_text['searchkey'] = $_GET['searchkey'];
			$shops = YK_M_SHOP::Search($search_text);
			header( 'Content-Type: application/json; charset=UTF-8' );
			echo json_encode($shops);
			return;
		}
		else if($_GET['module'] == 'admin_request_search_staff'){
			$search_text['searchkey'] = $_GET['searchkey'];
			$staffs = YK_M_STAFF::Search($search_text);
			header( 'Content-Type: application/json; charset=UTF-8' );
			echo json_encode($staffs);
			return;
		}
		else if($_GET['module'] == 'admin_request_search_position'){
			$search_text['searchkey'] = $_GET['searchkey'];
			$positions = YK_M_POSITION::Search($search_text);
			header( 'Content-Type: application/json; charset=UTF-8' );
			echo json_encode($positions);
			return;
		}
		else if($_GET['module'] == 'admin_request_search_self_evaluate'){
			$search_text['searchkey'] = $_GET['searchkey'];
			$self_evaluates = YK_T_SELF_EVALUATE::Search($search_text);
			header( 'Content-Type: application/json; charset=UTF-8' );
			echo json_encode($self_evaluates);
		}
    }
}
