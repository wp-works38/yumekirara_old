<?php
/**
 * YK_T_FINANCE short summary.
 *
 * YK_T_FINANCE description.
 *
 * @version 1.0
 * @author
 */
class YK_T_FINANCE
{
    //const TABLE_NAME = 'YK_T_FINANCE';
    public static function TABLE_NAME(){return $GLOBALS['table_prefix'].'YK_T_FINANCE';}

	public $F_ID;// 'ID'
	public $F_SALES_BUDGET;// '売り上げ予算'
	public $F_PAY_RESOURCE;// '支給原資総額算出率'
	public $F_TOTAL_PAY;// '総支給額原資'
	public $F_FURISODE;// '予測最低接客数（振袖成約率）'
	public $F_HAKAMA;// '予測最低接客数（袴成約率）'

    public static function COLUMN_NAME(){
        $t=YK_T_FINANCE::TABLE_NAME();
        return "
     ${t}.F_ID as '${t}.F_ID'
	,${t}.F_SALES_BUDGET as '${t}.F_SALES_BUDGET'
	,${t}.F_PAY_RESOURCE as '${t}.F_PAY_RESOURCE'
	,${t}.F_TOTAL_PAY as '${t}.F_TOTAL_PAY'
	,${t}.F_FURISODE as '${t}.F_FURISODE'
	,${t}.F_HAKAMA as '${t}.F_HAKAMA'
	";
	}
    /** Constructor */
    public function __construct(){}
    public function save(){

        $record = array();
		$record['F_ID'] = YK_Common::CnvStrEmpty($this->F_ID);
		$record['F_SALES_BUDGET'] = YK_Common::CnvDecimal14_1($this->F_SALES_BUDGET);
		$record['F_PAY_RESOURCE'] = YK_Common::CnvDecimal14_1($this->F_PAY_RESOURCE);
		$record['F_TOTAL_PAY'] = YK_Common::CnvDecimal14_1($this->F_TOTAL_PAY);
		$record['F_FURISODE'] = YK_Common::CnvDecimal8_4($this->F_FURISODE);
		$record['F_HAKAMA'] = YK_Common::CnvDecimal8_4($this->F_HAKAMA);

        global $wpdb;
        if ($this->F_ID == 0){
            $result = $wpdb->insert(YK_T_FINANCE::TABLE_NAME(), $record,
               array(
			     '%d' // F_ID
			   ,'%d'// F_SALES_BUDGET
			   ,'%d'// F_PAY_RESOURCE
			   ,'%d'// F_TOTAL_PAY
			   ,'%d'// F_FURISODE
			   ,'%d'// F_HAKAMA
			   ));

            if (!$result) {
                return false;
            }

            $insert_id = $wpdb->insert_id;
            $this->F_ID = $insert_id;
            return $insert_id;
        }
        else
        {
            //更新
            $where = array('F_ID' => $this->F_ID);
            $wpdb->update(YK_T_FINANCE::TABLE_NAME(),$record,$where,array(
			    '%d'// F_ID
			   ,'%d'// F_SALES_BUDGET
			   ,'%d'// F_PAY_RESOURCE
			   ,'%d'// F_TOTAL_PAY
			   ,'%d'// F_FURISODE
			   ,'%d'// F_HAKAMA
               )
			   ,array('%d'));
            if ($wpdb->last_error!='') {
                return false;
            }
            return $this->F_ID;
        }
    }
    public function delete(){
        global $wpdb;
        $where = array('F_ID' => $this->F_ID);
        $wpdb->delete( YK_T_FINANCE::TABLE_NAME(), $where, null );
        if ($wpdb->last_error!='') {
            return false;
        }
        return $this->F_ID;
    }

    public static function search($param){
        global $wpdb;
        $t = YK_T_FINANCE::TABLE_NAME();

        // ▼column
        $column = YK_T_FINANCE::COLUMN_NAME();
        //if($param['innerjoin_staff']===true)
        //{
        //    $column .= ', '.EL_Staff::COLUMN_NAME.' ';
        //}
        //elseif($param['leftjoin_staff']===true)
        //{
        //    $column .= ', '.EL_Staff::COLUMN_NAME.' ';
        //}

        // ▼table
        $table = YK_T_FINANCE::TABLE_NAME();
        //if($param['innerjoin_staff']===true)
        //{
        //    $table = $table.' INNER JOIN '.EL_Staff::TABLE_NAME.' ON ('.EL_Staff::TABLE_NAME.'.F_ID = '.YK_T_FINANCE::TABLE_NAME().'.StaffID) ';
        //}
        //elseif($param['leftjoin_staff']===true)
        //{
        //    $table = $table.' LEFT OUTER JOIN '.EL_Staff::TABLE_NAME.' ON ('.EL_Staff::TABLE_NAME.'.F_ID = '.YK_T_FINANCE::TABLE_NAME().'.StaffID) ';
        //}

        // ▼Wrhere
        $where = self::build_where($param);

        // ▼Group By
        $group = '';
        //if($param['leftjoin_menu_name']===true)
        //{
        //    $group = $group.' GROUP BY '.YK_T_FINANCE::TABLE_NAME().'.F_ID ';
        //}

        // ▼OrderBy
        $order = "${t}.F_ID";
        if(isset($param['orderkey']))
        {
            $order='';
            foreach($param['orderkey'] as $_key)
            {
                $order.= "${t}.${_key},";
            }
            $order= $order===''?"${t}.F_ID":substr($order, 0, -1);
            $order .=' '.$param['orderdirection'];
        }

        // ▼result mode(count or select)
        if($param['mode'] == 'countup'){
            $ret = $wpdb->get_row("SELECT count(*) as count FROM $table WHERE $where $group", ARRAY_A);
            return $ret['count'];

        }
        else{
            $records = $wpdb->get_results("SELECT $column FROM $table WHERE $where $group ORDER BY $order", ARRAY_A);
        }
        if (empty($records)) {
			return array();
		}

        $arr = array();
        foreach ($records as $rec){
            $result = new self();
            self::db_to_property($rec, $result);
            //if($param['innerjoin_staff']===true)
            //{
            //    $result->StaffEntity = new EL_Staff();
            //    EL_Staff::db_to_property($rec, $result->StaffEntity);
            //}
            //elseif($param['leftjoin_staff']===true)
            //{
            //    $result->StaffEntity = new EL_Staff();
            //    EL_Staff::db_to_property($rec, $result->StaffEntity);
            //}
            array_push($arr, $result);
        }
        return $arr;
    }

    private static function build_where($param){

        // ▼デフォルト条件
        //if(is_null($param['IsDelete']))
        //    $param['IsDelete']='0';

        $t = YK_T_FINANCE::TABLE_NAME();
        $where = "$t.F_ID <> 0 "; //ダミー

        //▼検索キーが指定されたらあいまい検索
        if(!empty( $param['searchkey']))
        {
            $key = esc_sql($param['searchkey']);
            $where = $where .
               " and ($t.F_SALES_BUDGET like '%" . $key . "%' or
                      $t.F_PAY_RESOURCE like '%" . $key . "%' or
                      $t.F_TOTAL_PAY like '%" . $key . "%' or
                      $t.F_FURISODE like '%" . $key . "%' or
                      $t.F_HAKAMA like '%" . $key . "%') ";
        }
        //▼日付範囲１
        //if(!empty( $param['StartDate']))
        //{
        //    $stDate = esc_sql($param['StartDate']);
        //    $where = $where .
        //       " and ($t.F_CREATE_DATETIME >= '" . $stDate . "') ";
        //}
        //if(!empty( $param['StartDate']))
        //{
        //    $edDate = esc_sql($param['EndDate']);
        //    $nextDate = date('Y-m-d', strtotime($edDate. ' + 1 days'));
        //    $where = $where .
        //       " and ($t.F_CREATE_DATETIME < '" . $nextDate . "') ";
        //}

        //▼日付範囲２
        //if(!empty( $param['Min_ModifyTime']))
        //{
        //    $modDate = esc_sql($param['Min_ModifyTime']);
        //    $where = $where .
        //       " and ($t.ModifyTime >= '" . $modDate . "') ";
        //}

        //▼各カラムの一致
        if(!is_null($param['F_ID']))$where = $where . " and $t.F_ID = " . esc_sql($param['F_ID']);
		if(!is_null($param['F_SALES_BUDGET']))$where = $where . " and $t.F_SALES_BUDGET = '" . esc_sql($param['F_SALES_BUDGET']) . "' ";
		if(!is_null($param['F_PAY_RESOURCE']))$where = $where . " and $t.F_PAY_RESOURCE = '" . esc_sql($param['F_PAY_RESOURCE']) . "' ";
		if(!is_null($param['F_TOTAL_PAY']))$where = $where . " and $t.F_TOTAL_PAY = '" . esc_sql($param['F_TOTAL_PAY']) . "' ";
		if(!is_null($param['F_FURISODE']))$where = $where . " and $t.F_FURISODE = '" . esc_sql($param['F_FURISODE']) . "' ";
		if(!is_null($param['F_HAKAMA']))$where = $where . " and $t.F_HAKAMA = '" . esc_sql($param['F_HAKAMA']) . "' ";        return $where;
    }
    public static function total_count($param){
        $param['mode'] = 'countup';
        return self::Search($param);
    }
    private static function db_to_property($owner_data, $result){
        $t = YK_T_FINANCE::TABLE_NAME();
		$result->F_ID = $owner_data["${t}.F_ID"];
		$result->F_SALES_BUDGET = $owner_data["${t}.F_SALES_BUDGET"];
		$result->F_PAY_RESOURCE = $owner_data["${t}.F_PAY_RESOURCE"];
		$result->F_TOTAL_PAY = $owner_data["${t}.F_TOTAL_PAY"];
		$result->F_FURISODE = $owner_data["${t}.F_FURISODE"];
		$result->F_HAKAMA = $owner_data["${t}.F_HAKAMA"];
    }

    public static function CreateTable()
    {
        global $wpdb;
        if(YK_Common::ExistTable(YK_T_FINANCE::TABLE_NAME()))
            return;

        $tablename = YK_T_FINANCE::TABLE_NAME();
        $q = "create table `$tablename` (
			  `F_ID` BIGINT unsigned not null AUTO_INCREMENT comment 'ID'
			  , `F_SALES_BUDGET` DECIMAL(14,1) comment '売り上げ予算'
			  , `F_PAY_RESOURCE` DECIMAL(14,1) comment '支給原資総額算出率'
			  , `F_TOTAL_PAY` DECIMAL(14,1) comment '総支給額原資'
			  , `F_FURISODE` DECIMAL(8,4) comment '予測最低接客数（振袖成約率）'
			  , `F_HAKAMA` DECIMAL(8,4) comment '予測最低接客数（袴成約率）'
			  , constraint `YK_T_FINANCE_PKC` primary key (`F_ID`)
			) comment '予算管理' ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
			";

        $wpdb->query($q);
        if(!empty($wpdb->last_error)){
            echo "更新エラーが発生しました。";
            throw new Exception($wpdb->last_error);
        }
    }

    //ユニークなユーザーＩＤを作成する
    public function make_unique_id(){
        $str = substr( '0'.date_i18n('Y'), -2);  //下二ケタ
        //ランダム5桁 先頭０埋め
        $str = $str.substr( '0000'.mt_rand(111, 99000), -5);

        //存在確認
        $userdata = WP_User::get_data_by( 'login', $str );
        if(!$userdata)
            return $str; //存在しないので採用

        //既にあったので再起処理でもう一度
        return $this->make_unique_id();
    }

    public static function Load(){
        $param['F_ID'] = 1;
        $ret = self::Search($param);
        if($ret == null || count($ret) <= 0)
        {
            return null;
        }
        return $ret[0];
    }
}
?>