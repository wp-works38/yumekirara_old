<?php
/**
 * YK_M_SHOP short summary.
 *
 * YK_M_SHOP description.
 *
 * @version 1.0
 * @author
 */
class YK_M_SHOP
{
    //const TABLE_NAME = 'YK_M_SHOP';
    public static function TABLE_NAME(){return $GLOBALS['table_prefix'].'YK_M_SHOP';}

	public $F_ID;// 'ID'
	public $F_SHOP_NAME;// '店舗名'
	public $F_SHOP_BUDGET;// '予算'
	public $F_PROVISION_FUND;// '支給原資に含む'

    public static function COLUMN_NAME(){
        $t=YK_M_SHOP::TABLE_NAME();
        return "
     ${t}.F_ID as '${t}.F_ID'
   ,${t}.F_SHOP_NAME as '${t}.F_SHOP_NAME'
   ,${t}.F_SHOP_BUDGET as '${t}.F_SHOP_BUDGET'
   ,${t}.F_PROVISION_FUND as '${t}.F_PROVISION_FUND'
	";
	}
    /** Constructor */
    public function __construct(){}
    public function save(){

        $record = array();
        $record['F_ID'] = YK_Common::CnvStrEmpty($this->F_ID);
		$record['F_SHOP_NAME'] = YK_Common::CnvStrEmpty($this->F_SHOP_NAME);
		$record['F_SHOP_BUDGET'] = YK_Common::CnvDecimal14_1($this->F_SHOP_BUDGET);
		$record['F_PROVISION_FUND'] = YK_Common::CnvStrEmpty($this->F_PROVISION_FUND);

        global $wpdb;
        if ($this->F_ID == 0){
            $result = $wpdb->insert(YK_M_SHOP::TABLE_NAME(), $record,
               array(
			     '%d' // F_ID
			   ,'%s'// F_SHOP_NAME
			   ,'%d'// F_SHOP_BUDGET
			   ,'%d'// F_PROVISION_FUND
			   ));

            if (!$result) {
                return false;
            }

            $insert_id = $wpdb->insert_id;
            $this->F_ID = $insert_id;
            return $insert_id;
        }
        else
        {
            //更新
            $where = array('F_ID' => $this->F_ID);
            $wpdb->update(YK_M_SHOP::TABLE_NAME(),$record,$where,array(
			    '%d' // F_ID
			   ,'%s'// F_SHOP_NAME
			   ,'%d'// F_SHOP_BUDGET
			   ,'%d'// F_PROVISION_FUND
               )
			   ,array('%d'));
            if ($wpdb->last_error!='') {
                return false;
            }
            return $this->F_ID;
        }
    }
    public function delete(){
        global $wpdb;
        $where = array('F_ID' => $this->F_ID);
        $wpdb->delete( YK_M_SHOP::TABLE_NAME(), $where, null );
        if ($wpdb->last_error!='') {
            return false;
        }
        return $this->F_ID;
    }

    public static function search($param){
        global $wpdb;
        $t = YK_M_SHOP::TABLE_NAME();

        // ▼column
        $column = YK_M_SHOP::COLUMN_NAME();
        //if($param['innerjoin_staff']===true)
        //{
        //    $column .= ', '.EL_Staff::COLUMN_NAME.' ';
        //}
        //elseif($param['leftjoin_staff']===true)
        //{
        //    $column .= ', '.EL_Staff::COLUMN_NAME.' ';
        //}

        // ▼table
        $table = YK_M_SHOP::TABLE_NAME();
        //if($param['innerjoin_staff']===true)
        //{
        //    $table = $table.' INNER JOIN '.EL_Staff::TABLE_NAME.' ON ('.EL_Staff::TABLE_NAME.'.F_ID = '.YK_M_SHOP::TABLE_NAME().'.StaffID) ';
        //}
        //elseif($param['leftjoin_staff']===true)
        //{
        //    $table = $table.' LEFT OUTER JOIN '.EL_Staff::TABLE_NAME.' ON ('.EL_Staff::TABLE_NAME.'.F_ID = '.YK_M_SHOP::TABLE_NAME().'.StaffID) ';
        //}

        // ▼Wrhere
        $where = self::build_where($param);

        // ▼Group By
        $group = '';
        //if($param['leftjoin_menu_name']===true)
        //{
        //    $group = $group.' GROUP BY '.YK_M_SHOP::TABLE_NAME().'.F_ID ';
        //}

        // ▼OrderBy
        $order = "${t}.F_ID";
        if(isset($param['orderkey']))
        {
            $order='';
            foreach($param['orderkey'] as $_key)
            {
                $order.= "${t}.${_key},";
            }
            $order= $order===''?"${t}.F_ID":substr($order, 0, -1);
            $order .=' '.$param['orderdirection'];
        }

        // ▼result mode(count or select)
        if($param['mode'] == 'countup'){
            $ret = $wpdb->get_row("SELECT count(*) as count FROM $table WHERE $where $group", ARRAY_A);
            return $ret['count'];

        }
        else{
            $records = $wpdb->get_results("SELECT $column FROM $table WHERE $where $group ORDER BY $order", ARRAY_A);
        }
        if (empty($records)) {
			return array();
		}

        $arr = array();
        foreach ($records as $rec){
            $result = new self();
            self::db_to_property($rec, $result);
            //if($param['innerjoin_staff']===true)
            //{
            //    $result->StaffEntity = new EL_Staff();
            //    EL_Staff::db_to_property($rec, $result->StaffEntity);
            //}
            //elseif($param['leftjoin_staff']===true)
            //{
            //    $result->StaffEntity = new EL_Staff();
            //    EL_Staff::db_to_property($rec, $result->StaffEntity);
            //}
            array_push($arr, $result);
        }
        return $arr;
    }

    private static function build_where($param){

        // ▼デフォルト条件
        //if(is_null($param['IsDelete']))
        //    $param['IsDelete']='0';

        $t = YK_M_SHOP::TABLE_NAME();
        $where = "$t.F_ID <> 0 "; //ダミー

        //▼検索キーが指定されたらあいまい検索
        if(!empty( $param['searchkey']))
        {
            $key = esc_sql($param['searchkey']);
            $where = $where .
               " and ($t.F_SELF_EVALUATE like '%" . $key . "%' or
                      $t.F_SHOP_NAME like '%" . $key . "%' or
					  $t.F_SHOP_BUDGET like '%" . $key . "%' or
                      $t.F_PROVISION_FUND like '%" . $key . "%') ";
        }
        //▼日付範囲１
        //if(!empty( $param['StartDate']))
        //{
        //    $stDate = esc_sql($param['StartDate']);
        //    $where = $where .
        //       " and ($t.F_CREATE_DATETIME >= '" . $stDate . "') ";
        //}
        //if(!empty( $param['StartDate']))
        //{
        //    $edDate = esc_sql($param['EndDate']);
        //    $nextDate = date('Y-m-d', strtotime($edDate. ' + 1 days'));
        //    $where = $where .
        //       " and ($t.F_CREATE_DATETIME < '" . $nextDate . "') ";
        //}

        //▼日付範囲２
        //if(!empty( $param['Min_ModifyTime']))
        //{
        //    $modDate = esc_sql($param['Min_ModifyTime']);
        //    $where = $where .
        //       " and ($t.ModifyTime >= '" . $modDate . "') ";
        //}

        //▼各カラムの一致
		if(!is_null($param['F_ID']))$where = $where . " and $t.F_ID = " . esc_sql($param['F_ID']);
		if(!is_null($param['F_SHOP_NAME']))$where = $where . " and $t.F_SHOP_NAME = " . esc_sql($param['F_SHOP_NAME']);
		if(!is_null($param['F_SHOP_BUDGET']))$where = $where . " and $t.F_SHOP_BUDGET = " . esc_sql($param['F_SHOP_BUDGET']);
		if(!is_null($param['F_PROVISION_FUND']))$where = $where . " and $t.F_PROVISION_FUND = " . esc_sql($param['F_PROVISION_FUND']);
        return $where;
    }
    public static function total_count($param){
        $param['mode'] = 'countup';
        return self::Search($param);
    }
    public static function db_to_property($owner_data, $result){
        $t = YK_M_SHOP::TABLE_NAME();
		$result->F_ID = $owner_data["${t}.F_ID"];
		$result->F_SHOP_NAME = $owner_data["${t}.F_SHOP_NAME"];
		$result->F_SHOP_BUDGET = $owner_data["${t}.F_SHOP_BUDGET"];
		$result->F_PROVISION_FUND = $owner_data["${t}.F_PROVISION_FUND"];
    }

    public static function CreateTable()
    {
        global $wpdb;
        if(YK_Common::ExistTable(YK_M_SHOP::TABLE_NAME()))
            return;

        $tablename = YK_M_SHOP::TABLE_NAME();
        $q = "create table `$tablename` (
			  `F_ID` BIGINT unsigned not null AUTO_INCREMENT comment 'ID'
			  , `F_SHOP_NAME` VARCHAR(255) not null comment '店舗名'
			  , `F_SHOP_BUDGET` DECIMAL(14,1) not null comment '予算'
			  , `F_PROVISION_FUND` BIGINT(1) not null comment '支給原資に含む'
			  , constraint `YK_M_SHOP_PKC` primary key (`F_ID`)
			) comment '店舗マスタ' ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
			";

        $wpdb->query($q);
        if(!empty($wpdb->last_error)){
            echo "更新エラーが発生しました。";
            throw new Exception($wpdb->last_error);
        }
    }

    //ユニークなユーザーＩＤを作成する
    public function make_unique_id(){
        $str = substr( '0'.date_i18n('Y'), -2);  //下二ケタ
        //ランダム5桁 先頭０埋め
        $str = $str.substr( '0000'.mt_rand(111, 99000), -5);

        //存在確認
        $userdata = WP_User::get_data_by( 'login', $str );
        if(!$userdata)
            return $str; //存在しないので採用

        //既にあったので再起処理でもう一度
        return $this->make_unique_id();
    }

	//新しいレコードを追加
    public static function AddNew($param){
        $yk = new YK_M_SHOP();
        $yk->F_ID = $param['F_ID'];
        $yk->F_SHOP_NAME = $param['F_SHOP_NAME'];
        $yk->F_SHOP_BUDGET = $param['F_SHOP_BUDGET'];
        $yk->F_PROVISION_FUND = $param['F_PROVISION_FUND'];
        $yk->Save();
	}

	//指定したIDの読み込み
    public static function Load($id, $param=null){
        if ($param == null)
            $param = array();
        $param['ID'] = $id;
        $ret = YK_M_SHOP::Search($param);
        if($ret == null || count($ret) <= 0)
        {
            return null;
        }
        return $ret[0];
    }
}
?>