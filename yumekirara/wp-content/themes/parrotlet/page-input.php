<?php get_header(); ?>
<?php
$settings = YK_T_FINANCE::search(array());
if(count($settings)==0)
{
    echo '予算管理テーブルにデータがありません。';
    exit;
}
$settings=$settings[0];
function get_val($v){

    return ((float)$v) * 1000;
}
function set_val($v){

    return ((float)$v) / 1000;
}
?>
<input type="hidden" id="F_SALES_BUDGET" value="<?php esc_attr_e($settings->F_SALES_BUDGET);?>" />
<input type="hidden" id="F_PAY_RESOURCE" value="<?php esc_attr_e($settings->F_PAY_RESOURCE);?>" />
<input type="hidden" id="F_TOTAL_PAY" value="<?php esc_attr_e($settings->F_TOTAL_PAY);?>" />
<input type="hidden" id="F_FURISODE" value="<?php esc_attr_e($settings->F_FURISODE);?>" />
<input type="hidden" id="F_HAKAMA" value="<?php esc_attr_e($settings->F_HAKAMA);?>" />

<a href="<?php echo esc_url( home_url() );?>">←戻る</a>
<main id="page-input">

    <div>
        <h3>店舗評価</h3>
        <table id="shoptable" class="table-incentive">
            <thead>
                <tr>
                    <th>店舗</th>
                    <th>店舗予算</th>
                    <th>各店半期売上予測<br />（千円）</th>
                    <th>各店半期売上達成率予測<br />（％）</th>
                </tr>
            </thead>
            <tbody>
                <?php
                // 店舗情報を取得してまわす
                $shop_array = YK_M_SHOP::search(array());
                foreach ($shop_array as $shop)
                {
                    echo('<tr id="shop_'.$shop->F_ID.'">');
                    {
                        //店舗
                        echo('<td>'.$shop->F_SHOP_NAME.'</td>');
						echo('<input class="shop_provision_fund_hidden" type="hidden"  value="'.$shop->F_PROVISION_FUND.'"/>');
                        //店舗予算
                        echo('<td>');
                        echo('<label class="shop_budg">'.set_val($shop->F_SHOP_BUDGET).'</label>');
                        echo('<input class="shop_budg_hidden" type="hidden" value="'.$shop->F_SHOP_BUDGET.'" />');
                        echo('</td>');

                        //各店半期売上予測（千円）
                        $sum_enable = $shop->F_PROVISION_FUND ? ' sum_enable':'';
                        echo('<td>');
                        echo('<input class="left_input" type="text" />');
                        echo('<input class="left_input_hidden'.$sum_enable.'" type="hidden" />');
                        echo('<label class="valid_shop_left" style="color:red;display:block;"></label>');
                        echo('</td>');


                        //各店半期売上達成率予測（％）
                        echo('<td>');
                        echo('<input class="right_input" type="text" />');
                        echo('<label class="valid_shop_right" style="color:red;display:block;"></label>');
                        echo('</td>');
                    }
                }
                ?>
            </tbody>
            <tfoot></tfoot>
        </table>
        

    </div>
    <div>

        <h3>従業員選択</h3>
        <label>賞与計算を行う従業員を選択してください。</label>
        <br />
        <?php

        $shop_staff = YK_M_AFFILIATION::search(array('inner_join_position'=>true,'inner_join_staff'=>true,'inner_join_shop'=>true));

        $shop_staff_group_array = array_reduce($shop_staff, function($result, $item) {
            $result[$item->F_STAFF_ID][] = $item;
            return $result;
        });

        ?>
        <select id="staffs">
            <option value="0" position="0"></option>
            <?php
                foreach (array_keys($shop_staff_group_array) as $groupkey)
                {
                    $group = $shop_staff_group_array[$groupkey];
                    $shops = '（店舗：';
                    foreach($group as $item)
                    {
                        $shops.= $item->ShopMasterEntity->F_SHOP_NAME.',';
                    }
                    $shops = rtrim($shops, ',');
                    $shops.= '）';
                    $temp_staff = $group[0]->StaffMasterEntity;
                    $temp_position = $group[0]->PositionMasterEntity;
                    // JSで店長とそれ以外の選択時に下部に表示される要素を操作する
                    echo('<option value="'.$temp_staff->F_ID.'" position="'.$temp_position->F_POSITION_RATE.'" >');
                    {
                        echo($temp_staff->F_LAST_NAME.' '.$temp_staff->F_FAST_NAME.'(職位：'.$temp_position->F_POSITION_NAME.')'.$shops);
                    }
                    echo('</option>');
                }
            ?>
        </select>
    </div>
    <div id="selfpanel">
        <h3>個人評価</h3>
        <?php
            $choices = YK_T_CHOICE::search(array('inner_join_self_evaluate'=>true));
            // 親でグループ化して回す。
            $choice_group_array = array_reduce($choices, function($result, $item) {
                $result[$item->F_SELF_EVALUATE][] = $item;
                return $result;
            });
            echo '<table class="table-incentive">';
            foreach (array_keys($choice_group_array ) as $groupkey)
            {
                $group = $choice_group_array[$groupkey];
                $select = $group[0]->SelfEvaluateEntity;
                echo '<tr>';
                echo '<th>';
                echo $select->F_ITEM_NAME;
                echo '</th>';
                echo '<td>';
                
                echo '<select class="self_evaluate" id="evaluate_reader_'.$select->F_ID.'">';
                echo '<option value="0"></option>';
                foreach ($group as $option)
                {
                    echo('<option value="'.$option->F_EVALUATION_POINTS.'" id="choice_'.$option->F_ID.'">');
                    {
                        echo($option->F_CHOICE_NAME);
                    }
                    echo('</option>');
                }
                echo '</select>';
                
                echo '</td>';
                echo '<td>';
                echo  '<label class="shop_point" ></label>';
                echo '</td>';
                echo '</tr>';
            }
            echo '</table>';

        ?>
        
    </div>
    <div>
        <h3>インセンティブ</h3>
        <h4>【振袖AD】</h4>
        <table class="table-incentive">
            <tr>
                <th>
                     振袖成約率（％）
                    <br />
                    （予測最低接客数:<span class="min-sales">
                    <?php 
                    esc_html_e($settings->F_FURISODE);
                    ?>
                            </span>
                    ）
                </th>
                <td>
                    <div>
                        <input id="furi-ritu" type="text" />
                        <label id="valid-furi-ritu" style="color:red;"></label>
                    </div>
                </td>
                <td>
                    <div>
                        <label id="calc-furi-ritu" class="calc-tmp-result" ></label>
                        <input id="calc-furi-ritu_hidden" type="hidden" class="calc-incentive" />
                    </div>
                </td>
            </tr>
            <tr>
                <th>
                     振袖成約数（件）
                </th>
                <td>
                    <div>
                        <input id="furi-count" type="text" />
                        <label id="valid-furi-count" style="color:red;"></label>
                    </div>
                </td>
                <td>
                    <div>
                        <label id="calc-furi-count" class="calc-tmp-result" ></label>
                        <input id="calc-furi-count_hidden" type="hidden" class="calc-incentive" />
                    </div>
                </td>
            </tr>
            <tr>
                <th>
                     袴成約率（％）
                    <br />
                    （予測最低接客数:<span class="min-sales">
                    <?php 
                    esc_html_e($settings->F_HAKAMA);
                    ?>
                    </span>
                    ）
                        
                </th>
                <td>
                    <div>
                        <input id="hakama-ritu" type="text" />
                        <label id="valid-hakama-ritu" style="color:red;"></label>
                    </div>
                </td>
                <td>
                    <div>
                        <label id="calc-hakama-ritu"  class="calc-tmp-result" ></label>
                        <input id="calc-hakama-ritu_hidden" type="hidden" class="calc-incentive"/>
                    </div>
                </td>
            </tr>
            <tr>
                <th>
                     袴成約数（件）
                </th>
                <td>
                    <div>
                        <input id="hakama-count" type="text" />
                        <label id="valid-hakama-count" style="color:red;"></label>
                    </div>
                </td>
                <td>
                    <div>
                        <label id="calc-hakama-count" class="calc-tmp-result" ></label>
                        <input id="calc-hakama-count_hidden" type="hidden" class="calc-incentive" />
                    </div>
                </td>
            </tr>
            <tr>
                <th>
                    フォローレター数（通）
                </th>
                <td>
                    <div>
                        <input id="letter-count" type="text" />
                        <label id="valid-letter-count" style="color:red;"></label>
                    </div>
                </td>
                <td>
                    <div>
                        <label id="calc-letter-count" class="calc-tmp-result" ></label>
                        <input id="calc-letter-count_hidden" type="hidden" class="calc-incentive" />
                    </div>
                </td>
            </tr>
            <tr>
                <th>
                      紹介客獲得数（件）
                </th>
                <td>
                    <div>
                        <input id="getcustomer-count" type="text" />
                        <label id="valid-getcustomer-count" style="color:red;"></label>
                    </div>
               </td>
                <td>
                    <div>
                        <label id="calc-getcustomer-count" class="calc-tmp-result" ></label>
                        <input id="calc-getcustomer-count_hidden" type="hidden" class="calc-incentive" />
                    </div>
                </td>
            </tr>
        </table>

        <h4>【スタジオチーム】</h4>
        <table class="table-incentive">
            <tr>
                <th>
                    振袖撮影件数（件）
                </th>
                <td>
                    <div>
                        <input id="camera-count" type="text" />
                        <label id="valid-camera-count" style="color:red;"></label>
                    </div>
                </td>
                <td>
                    <div>
                        <label id="calc-camera-count" class="calc-tmp-result" ></label>
                        <input id="calc-camera-count_hidden" type="hidden" class="calc-incentive" />
                    </div>
                </td>
            </tr>
            <tr>
                <th>
                    振袖撮影売上平均単価（円）
                </th>
                <td>
                    <div>
                        <input id="camera-average" type="text" />
                        <label id="valid-camera-average" style="color:red;"></label>
                    </div>
                </td>
                <td>
                    <div>
                        <label id="calc-camera-average" class="calc-tmp-result" ></label>
                        <input id="calc-camera-average_hidden" type="hidden" class="calc-incentive" />
                    </div>
                </td>
            </tr>

        </table>
    </div>
    <div>
        <h3>集計結果</h3>
        <div>
            <button onclick="yk_input.calc_exe();" class="" >計算実行</button>
        </div>
        <h4>全社予算</h4>
        <table class="table-incentive">
            <tr>
                <th>半期売上予算（千円）</th>
                <?php
                $all_shop_yosan = 0;
				$studio_yosan = 0;
                $all_shop_yosan_gensi = 0;
                foreach ($shop_array as $shop)
                {
                    $all_shop_yosan += $shop->F_SHOP_BUDGET;
                    if($shop->F_PROVISION_FUND==='1')
                    {
                        $all_shop_yosan_gensi+= $shop->F_SHOP_BUDGET;
                    }else
					{
						$studio_yosan+= $shop->F_SHOP_BUDGET;
					}
                }
                ?>
                <td>
                    <div class="calc-tmp-result">
                        <label id="calc_all_yosan" ><?php esc_html_e( set_val($all_shop_yosan_gensi));?></label>
                        <input id="calc_all_yosan_hidden" type="hidden" value="<?php esc_attr_e( $all_shop_yosan);?>" />
                        <input id="calc_studio_yosan_hidden" type="hidden" value="<?php esc_attr_e( $studio_yosan);?>" />
                        <label id="calc_all_yosan_gensi" style="display:none;color:red;" ><?php esc_html_e($all_shop_yosan_gensi);?></label>
                    </div>
                </td>
            </tr>
            <tr>
                <th>半期売上予測（千円）</th>
                <td>
                    <div class="calc-tmp-result">
                        <label id="calc_all_yosou"></label>
                    </div>
                </td>
            </tr>
            <tr>
                <th>半期売上達成率（％）</th>
                <td>
                    <div class="calc-tmp-result">
                        <label id="calc_all_tasei"></label>
                    </div>
                </td>
            </tr>
        </table>
	
        
        <h4>店舗予算</h4>
        <table class="table-incentive">
            <tr>
                <th>半期売上予算（千円）</th>
                <td><label id="calc_tempoyosan"></label></td>
            </tr>
            <tr>
                <th>半期売上予測（千円）</th>
                <td><label id="calc_tempoyosoku"></label></td>
            </tr>
            <tr>
                <th>半期売上達成率（％）</th>
                <td><label id="calc_tempotasei"></label></td>
            </tr>
        </table>
        <h4>支給賞与予測</h4>
        <table class="table-incentive">
            <tr>
                <th>売上評価（円）</th>
                <td><label id="calc_uriagehyouka"></label></td>
            </tr>
            <tr>
                <th>個人評価（円）</th>
                <td><label id="calc_selfhyouka"></label></td>
            </tr>
            <tr>
                <th>インセンティブ（円）</th>
                <td><label id="calc_incentive"></label></td>
            </tr>
            <tr>
                <th>総支給予測額（円）</th>
                <td><label id="calc_allresult"></label></td>
            </tr>
        </table>
    </div>
</main>


<script>
	jQuery(function () {
        yk_input.ajaxurl = '<?php echo admin_url('admin-ajax.php', __FILE__); ?>';
        yk_input.CSRF = '<?php echo wp_create_nonce('yk_input') ?>';
    
    <?php 
    echo 'yk_input.ShopArray =[';
    $yk_inputShopArray = '';
    foreach($shop_staff as $shop_staff_item)
    {
        $userid=$shop_staff_item->StaffMasterEntity->F_ID;
        $name = $shop_staff_item->StaffMasterEntity->F_LAST_NAME.' '.$shop_staff_item->StaffMasterEntity->F_FAST_NAME;
        $incentive=$shop_staff_item->StaffMasterEntity->F_INCENTIVE;
        $selfpoint=$shop_staff_item->StaffMasterEntity->F_SELF_EVALUATEF;

        $shopid=$shop_staff_item->ShopMasterEntity->F_ID;
        $shopname=$shop_staff_item->ShopMasterEntity->F_SHOP_NAME;
        $shopbudget=$shop_staff_item->ShopMasterEntity->F_SHOP_BUDGET;
        $shopfund=$shop_staff_item->ShopMasterEntity->F_PROVISION_FUND;
        
        $positionid=$shop_staff_item->PositionMasterEntity->F_ID;
        $positionname=$shop_staff_item->PositionMasterEntity->F_POSITION_NAME;
        $positionrate=$shop_staff_item->PositionMasterEntity->F_POSITION_RATE;

        $yk_inputShopArray.= '{';
        $yk_inputShopArray.="userid:\"${userid}\"";
        $yk_inputShopArray.=',';
        $yk_inputShopArray.="username:\"${name}\"";
        $yk_inputShopArray.=',';
        $yk_inputShopArray.="incentive:\"${incentive}\"";
        $yk_inputShopArray.=',';
        $yk_inputShopArray.="selfpoint:\"${selfpoint}\"";
        
        $yk_inputShopArray.=',';
        $yk_inputShopArray.="shopid:\"${shopid}\"";
        $yk_inputShopArray.=',';
        $yk_inputShopArray.="shopname:\"${shopname}\"";
        $yk_inputShopArray.=',';
        $yk_inputShopArray.="shopbudget:\"${shopbudget}\"";
        $yk_inputShopArray.=',';
        $yk_inputShopArray.="shopfund:\"${shopfund}\"";
        
        $yk_inputShopArray.=',';
        $yk_inputShopArray.="positionid:\"${positionid}\"";
        $yk_inputShopArray.=',';
        $yk_inputShopArray.="positionname:\"${positionname}\"";
        $yk_inputShopArray.=',';
        $yk_inputShopArray.="positionrate:\"${positionrate}\"";

        $yk_inputShopArray.='},';
    }
    
    $yk_inputShopArray = rtrim($yk_inputShopArray, ',');
    echo $yk_inputShopArray.'];';
    ?>


	});
</script>
<?php get_footer(); ?>