<div class="share">
<?php 
    $share_url = get_permalink();
    $share_ttl = urlencode(wp_title( '', false, 'right' ));
?>
<a href="http://www.facebook.com/sharer.php?u=<?php echo $share_url; ?>&amp;t=<?php echo $share_ttl; ?>" target="_blank" class="share_facebook"><i class="icon icon-facebook"></i> <?php esc_html_e( 'Facebook', 'liquid-light' ); ?></a>
<a href="https://twitter.com/intent/tweet?text=<?php echo $share_ttl; ?>&url=<?php echo $share_url; ?>" target="_blank" class="share_twitter"><i class="icon icon-twitter"></i> <?php esc_html_e( 'Twitter', 'liquid-light' ); ?></a>
<a href="https://plusone.google.com/_/+1/confirm?hl=ja&amp;url=<?php echo $share_url; ?>" target="_blank" class="share_google"><i class="icon icon-google-plus"></i> <?php esc_html_e( 'Google+', 'liquid-light' ); ?></a>
<a href="http://b.hatena.ne.jp/add?mode=confirm&url=<?php echo $share_url; ?>&title=<?php echo $share_ttl; ?>" target="_blank" class="share_hatena"><i class="icon icon-bookmark"></i> <?php esc_html_e( 'Hatena', 'liquid-light' ); ?></a>
</div>