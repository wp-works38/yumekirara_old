<?php
require_once(dirname(__FILE__) . '/../libs.php');

/**
 * Register Settings
 */
function YK_update_db_init() {
    //オプションに保存しないので不要

}

function YK_update_db_page() {
    Page_YK_Update_Database::get_instance()->show_page();
}

class Page_YK_Update_Database
{
	const PAGE_NAME = 'YK_update_db';
    private $action = '';		// none

    static $instance;
    private $blist;
	private $message = '';
	private $errflg = false;
    /**
	 * インスタンス化
	 *
	 */
	static function get_instance() {
		if (!isset(self::$instance)) {
			self::$instance = new Page_YK_Update_Database();
		}

		return self::$instance;
	}

	public function __construct() {

		//parent::__construct();

        // action指定
        //if (isset($_GET['action'])) {
        //    $this->action = $_GET['action'];
        //}

		// CSSロード
        //wp_enqueue_style('wp-jquery-ui-dialog');

		// Javascriptロード
        //wp_enqueue_script("mtssb_list_admin_js", $this->plugin_url . "js/mtssb-list-admin.js", array('jquery', 'jquery-ui-dialog'));
	}

    /**
	 * 一覧表示
	 */
    function show_page() {

	    $message = '';
	    $errflg = false;
        try{
		    if (isset($_POST['action']) && isset($_POST['YK_nonce'])) {
                $post_action = $_POST['action'];
                $nonce = $_POST['YK_nonce'];
			    if (!wp_verify_nonce($nonce, self::PAGE_NAME . "_{$post_action}")) {
				    die("Nonce error!");
			    }

			    if ($post_action == 'update') {
                    require_once(get_template_directory() . '/library/YK_Update_Database.php');
                    $message = YK_Update_Database::DoUpdate()?'正常に更新されました':'更新はありませんでした';
                }
			    if ($post_action == 'clear_version') {
                    //$versioninfo = get_option( 'YK_version_info' );//バージョン関係
                    //$versioninfo['version'] = YK_Update_Database::$PROGRAM_VERSION;
                    //$versioninfo['updatedate'] = date_i18n('Y/m/d');
                    update_option('YK_version_info', null);
                }
            }
        }
        catch(\Exception $e)
        {
    	    $errflg = true;
            $message = $e->getMessage;
        }
?>

<div class="wrap ct_member-page">

	<div>
		<?php
        // 保存結果の出力
        if($_POST['action']=='update')
        {
            $styleclass = $errflg?'notice notice-error is-dismissible':'update notice notice-success is-dismissible';
            echo "<div id=\"message\" class=\"$styleclass\"><p>$message </p><button type=\"button\" class=\"notice-dismiss\"><span class=\"screen-reader-text\">この通知を非表示にする</span></button></div>";
        }

        ?>
	</div>
	<h2>
		DB更新（開発用）
	</h2>
	<div>
		現在のバージョン：<?php echo get_option( 'YK_version_info' )['version'];?>
	</div>
	<form method="post">
		<input name="Submit" type="submit" style="margin-top:20px" value="<?php esc_attr_e('保存・更新'); ?>" />
		<input type="hidden" name="action" value="update" />

		<?php
        wp_nonce_field(self::PAGE_NAME.'_update','YK_nonce');
        ?>
	</form>
	<form method="post">
		<input name="Submit" type="submit" style="margin-top:20px" value="<?php esc_attr_e('バージョンクリア'); ?>" />
		<input type="hidden" name="action" value="clear_version" />

		<?php
        wp_nonce_field(self::PAGE_NAME.'_clear_version','YK_nonce');
        ?>
	</form>
</div>

<script>

	jQuery(document).ready(function ($) {


	});

</script>

<?php

    }

}

?>
