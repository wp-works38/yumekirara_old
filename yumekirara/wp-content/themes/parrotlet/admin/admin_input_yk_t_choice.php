<?php
require_once(dirname(__FILE__) . '/../libs.php');

function yk_t_choice_input_show_page() {
    YK_Admin_Input_Choice::get_instance()->show_page();
}

class YK_Admin_Input_Choice
{
	const PAGE_NAME = 'yk_t_choice_input_page';
    private $action = '';		// none

    static $instance;
	private $message = '';
	private $errflg = false;

    /**
     * インスタンス化
     *
     */
	static function get_instance() {
		if (!isset(self::$instance)) {
			self::$instance = new YK_Admin_Input_Choice();
		}

		return self::$instance;
	}

	public function __construct() {

        // action指定
        if (isset($_GET['action'])) {
            $this->action = $_GET['action'];
        }

		// CSSロード
        //wp_enqueue_style('wp-jquery-ui-dialog');

		// Javascriptロード
        //wp_enqueue_script("mtssb_list_admin_js", $this->plugin_url . "js/mtssb-list-admin.js", array('jquery', 'jquery-ui-dialog'));
	}

    /**
     *  表示
     */
    function show_page() {

		if (isset($_POST['action'])) {
            $postAction = $_POST['action'];

			if (!wp_verify_nonce($_POST['nonce'], self::PAGE_NAME . "_{$postAction}")) {
				die("Nonce error!");
			}


			if ($postAction == 'save') {

                // 読み込む
                if(!empty($_POST['F_ID']))
                {
                    $edit_items = YK_T_CHOICE::Search(array('F_ID'=>$_POST['F_ID']));

                    $this->yk = is_array($edit_items) && !empty($edit_items) ? $edit_items[0] : null;
                }
                if($this->yk == null)
                {
                    $this->yk = new YK_T_CHOICE();// 新規保存
					$this->yk->SelfEvaluateEntity = new YK_T_SELF_EVALUATE();

                }

                try {
                    //パラメータ設定し保存
                    $this->yk->F_ID = isset($_POST['F_ID'])?$_POST['F_ID']:'';
                    $this->yk->F_SELF_EVALUATE = isset($_POST['F_SELF_EVALUATE'])?$_POST['F_SELF_EVALUATE']:'';
                    $this->yk->F_CHOICE_NAME = isset($_POST['F_CHOICE_NAME'])?$_POST['F_CHOICE_NAME']:'';
                    $this->yk->F_EVALUATION_POINTS = isset($_POST['F_EVALUATION_POINTS'])?$_POST['F_EVALUATION_POINTS']:'';


                    // 入力チェック
                    $this->message = '';
                    if($this->yk->F_SELF_EVALUATE==='')
                    {
                        $this->message .= '保存エラー：個人評価（設問）IDは必須です。<br>';
                        $this->errflg = true;
                    }
                    if($this->yk->F_CHOICE_NAME==='')
                    {
                        $this->message .= '保存エラー：選択肢名は必須です。<br>';
                        $this->errflg = true;
                    }
                    if($this->yk->F_EVALUATION_POINTS==='')
                    {
                        $this->message .= '保存エラー：評価ポイントは必須です。<br>';
                        $this->errflg = true;
                    }
					if(!is_numeric( $this->yk->F_EVALUATION_POINTS))
                    {
                        $this->message .= '保存エラー：評価ポイントには数値を入力してください。<br>';
                        $this->errflg = true;
                    }

                    if(!$this->errflg)
                    {
                        if(!$this->yk->save())
                        {
                            $this->message .= '保存エラー：データ更新に失敗しました。<br>';
                            $this->errflg = true;
                        }
                    }
                }
                catch (Exception $e) {
                    $this->message = '保存エラー';
                    $this->errflg = true;
                }
            }
        }
        else{
            //GET
            if ($this->action == 'edit') {
                //編集
                // 読み込む
                $yk_array = YK_T_CHOICE::Search(array('F_ID'=>$_GET['id']));
                $this->yk = is_array($yk_array) && !empty($yk_array) ? $yk_array[0] : new YK_T_CHOICE();
				$this->yk->SelfEvaluateEntity = empty($this->yk->SelfEvaluateEntity)?new YK_T_SELF_EVALUATE():$this->yk->SelfEvaluateEntity;

            } else {
                //新規
                $this->yk = new YK_T_CHOICE();
            }
        }


?>


<div class="wrap yk-page">
    <div>
        <?php
        // 保存結果の出力
        if($_POST['action']=='save')
        {
            if($this->errflg)
            {
                $message = $this->message;
                echo "<div id=\"message\" class=\"notice notice-error is-dismissible\"><p>$message </p><button type=\"button\" class=\"notice-dismiss\"><span class=\"screen-reader-text\">この通知を非表示にする</span></button></div>";
            }else
            {
                echo "<div id=\"message\" class=\"update notice notice-success is-dismissible\"><p>保存しました。 </p><button type=\"button\" class=\"notice-dismiss\"><span class=\"screen-reader-text\">この通知を非表示にする</span></button></div>";

            }
        }

        ?>
    </div>

    <h2><?php echo ($this->yk->F_ID > 0) ? '個人評価（選択肢）編集' : '個人評価（選択肢）新規登録' ?></h2>

    <div>
        <form method="post">
            <input name="Submit" type="submit" style="margin-top:20px" value="<?php esc_attr_e('保存・更新'); ?>" />
            <br />

            <table id="resister_table">
				<tr>
					<th>ID</th>
					<td>
						<input type="text" name="F_ID" readonly value="<?php esc_attr_e($this->yk->F_ID);?>" />
					</td>
				</tr>
				<tr>
					<th>個人評価（設問）ID</th>
					<td>
						<input type="text" name="F_SELF_EVALUATE" readonly value="<?php esc_attr_e($this->yk->F_SELF_EVALUATE);?>" />
						<input type="text" name="F_ITEM_NAME" readonly value="<?php esc_attr_e($this->yk->SelfEvaluateEntity->F_ITEM_NAME);?>" />
						<!-- 個人評価（設問） 選択 -->
						<input type="hidden" id="self_evaluatelist_result" />
						<script>var pupup_self_evaluate_list_resut = jQuery('input[name = "F_SELF_EVALUATE"]');</script>

						<!-- 個人評価（設問） 選択後のアクションを定義 -->
						<script>
							pupup_self_evaluate_list_resut.change(function () {
								// ユーザーIDから検索して値をセットするAjaxを呼び出したら便利
								if (lastSelectself_evaluate != null) {
									jQuery('input[name = "F_SELF_EVALUATE"]').val(lastSelectself_evaluate['F_ID']);
									jQuery('input[name = "F_ITEM_NAME"]').val(lastSelectself_evaluate['F_ITEM_NAME']);
								}

							});
						</script><?php get_template_part( 'template-parts/yk_popup_self_evaluate_list' );  ?>
					</td>
				</tr>
				<tr>
					<th>選択肢名</th>
					<td>
						<input type="text" name="F_CHOICE_NAME" value="<?php esc_attr_e($this->yk->F_CHOICE_NAME);?>" />
					</td>
				</tr>
				<tr>
					<th>評価ポイント</th>
					<td>
						<input type="text" name="F_EVALUATION_POINTS" value="<?php esc_attr_e($this->yk->F_EVALUATION_POINTS);?>" />
					</td>
				</tr>
            </table>
            <input type="hidden" name="action" value="save" />
            <input type="hidden" name="page" value="<?php echo $_REQUEST['page'] ?>" />
            <input type="hidden" name="nonce" value="<?php echo wp_create_nonce(strtolower(YK_Admin_Input_Choice::PAGE_NAME . "_save")) ?>" />
        </form>
    </div>
</div>

<script>

        jQuery(document).ready(function($) {


        });

</script>

<?php

    }
}?>