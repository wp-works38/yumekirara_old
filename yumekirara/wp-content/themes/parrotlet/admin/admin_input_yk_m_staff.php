<?php
require_once(dirname(__FILE__) . '/../libs.php');

function yk_m_staff_input_show_page() {
    YK_Admin_Input_Staff::get_instance()->show_page();
}

class YK_Admin_Input_Staff
{
	const PAGE_NAME = 'yk_m_staff_input_page';
    private $action = '';		// none

    static $instance;
	private $message = '';
	private $errflg = false;

    /**
     * インスタンス化
     *
     */
	static function get_instance() {
		if (!isset(self::$instance)) {
			self::$instance = new YK_Admin_Input_Staff();
		}

		return self::$instance;
	}

	public function __construct() {

        // action指定
        if (isset($_GET['action'])) {
            $this->action = $_GET['action'];
        }

		// CSSロード
        //wp_enqueue_style('wp-jquery-ui-dialog');

		// Javascriptロード
        //wp_enqueue_script("mtssb_list_admin_js", $this->plugin_url . "js/mtssb-list-admin.js", array('jquery', 'jquery-ui-dialog'));
	}

    /**
     *  表示
     */
    function show_page() {

		if (isset($_POST['action'])) {
            $postAction = $_POST['action'];

			if (!wp_verify_nonce($_POST['nonce'], self::PAGE_NAME . "_{$postAction}")) {
				die("Nonce error!");
			}


			if ($postAction == 'save') {

                // 読み込む
                if(!empty($_POST['F_ID']))
                {
                    $edit_items = YK_M_STAFF::Search(array('F_ID'=>$_POST['F_ID']));

                    $this->yk = is_array($edit_items) && !empty($edit_items) ? $edit_items[0] : null;
                }
                if($this->yk == null)
                {
                    $this->yk = new YK_M_STAFF();// 新規保存
                }

                try {
                    //パラメータ設定し保存
                    $this->yk->F_ID = isset($_POST['F_ID'])?$_POST['F_ID']:'';
                    $this->yk->F_LAST_NAME = isset($_POST['F_LAST_NAME'])?$_POST['F_LAST_NAME']:'';
                    $this->yk->F_FAST_NAME = isset($_POST['F_FAST_NAME'])?$_POST['F_FAST_NAME']:'';
                    $this->yk->F_SHOP_ID = isset($_POST['F_SHOP_ID'])?$_POST['F_SHOP_ID']:'';
                    $this->yk->F_INCENTIVE = isset($_POST['F_INCENTIVE'])?$_POST['F_INCENTIVE']:'';
                    $this->yk->F_SELF_EVALUATEF = isset($_POST['F_SELF_EVALUATEF'])?$_POST['F_SELF_EVALUATEF']:'';


                    // 入力チェック
                    $this->message = '';
                    if($this->yk->F_LAST_NAME==='')
                    {
                        $this->message .= '保存エラー：姓は必須です。<br>';
                        $this->errflg = true;
                    }
                    if($this->yk->F_FAST_NAME==='')
                    {
                        $this->message .= '保存エラー：名は必須です。<br>';
                        $this->errflg = true;
                    }
                    if($this->yk->F_SHOP_ID==='')
                    {
                        $this->message .= '保存エラー：店舗マスタIDは必須です。<br>';
                        $this->errflg = true;
                    }
					if($this->yk->F_INCENTIVE==='')
                    {
                        $this->message .= '保存エラー：既定のインセンティブは必須です。<br>';
                        $this->errflg = true;
                    }
					if(!is_numeric( $this->yk->F_INCENTIVE))
                    {
                        $this->message .= '保存エラー：既定のインセンティブには数値を入力してください。<br>';
                        $this->errflg = true;
                    }
					if($this->yk->F_SELF_EVALUATEF==='')
                    {
                        $this->message .= '保存エラー：既定の個人評価は必須です。<br>';
                        $this->errflg = true;
                    }
					if(!is_numeric( $this->yk->F_SELF_EVALUATEF))
                    {
                        $this->message .= '保存エラー：既定の個人評価には数値を入力してください。<br>';
                        $this->errflg = true;
                    }
                    if(!$this->errflg)
                    {
                        if(!$this->yk->save())
                        {
                            $this->message .= '保存エラー：データ更新に失敗しました。<br>';
                            $this->errflg = true;
                        }
                    }
                }
                catch (Exception $e) {
                    $this->message = '保存エラー';
                    $this->errflg = true;
                }
            }
        }
        else{
            //GET
            if ($this->action == 'edit') {
                //編集
                // 読み込む
                $yk_array = YK_M_STAFF::Search(array('F_ID'=>$_GET['id']));
                $this->yk = is_array($yk_array) && !empty($yk_array) ? $yk_array[0] : new YK_M_STAFF();

            } else {
                //新規
                $this->yk = new YK_M_STAFF();
            }
        }


?>


<div class="wrap yk-page">
    <div>
        <?php
        // 保存結果の出力
        if($_POST['action']=='save')
        {
            if($this->errflg)
            {
                $message = $this->message;
                echo "<div id=\"message\" class=\"notice notice-error is-dismissible\"><p>$message </p><button type=\"button\" class=\"notice-dismiss\"><span class=\"screen-reader-text\">この通知を非表示にする</span></button></div>";
            }else
            {
                echo "<div id=\"message\" class=\"update notice notice-success is-dismissible\"><p>保存しました。 </p><button type=\"button\" class=\"notice-dismiss\"><span class=\"screen-reader-text\">この通知を非表示にする</span></button></div>";

            }
        }

        ?>
    </div>

    <h2><?php echo ($this->yk->F_ID > 0) ? '従業員マスタ編集' : '従業員マスタ新規登録' ?></h2>

    <div>
        <form method="post">
            <input name="Submit" type="submit" style="margin-top:20px" value="<?php esc_attr_e('保存・更新'); ?>" />
            <br />

            <table id="resister_table">
				<tr>
					<th>ID</th>
					<td>
						<input type="text" name="F_ID" readonly value="<?php esc_attr_e($this->yk->F_ID);?>" />
					</td>
				</tr>
				<tr>
					<th>姓</th>
					<td>
						<input type="text" name="F_LAST_NAME" value="<?php esc_attr_e($this->yk->F_LAST_NAME);?>" />
					</td>
				</tr>
				<tr>
					<th>名</th>
					<td>
						<input type="text" name="F_FAST_NAME" value="<?php esc_attr_e($this->yk->F_FAST_NAME);?>" />
					</td>
				</tr>
				<tr>
					<th>店舗マスタID</th>
					<td>
						<input type="text" name="F_SHOP_ID" readonly value="<?php esc_attr_e($this->yk->F_SHOP_ID);?>" />
						<input type="text" name="F_SHOP_NAME" readonly value="<?php esc_attr_e( $this->yk->ShopMasterEntity->F_SHOP_NAME);?>" />
						<!-- 店舗 選択 -->
						<input type="hidden" id="shoplist_result" />
						<script>var pupup_shop_list_resut = jQuery('input[name = "F_SHOP_ID"]');</script>

						<!-- 店舗 選択後のアクションを定義 -->
						<script>
							pupup_shop_list_resut.change(function () {
								// ユーザーIDから検索して値をセットするAjaxを呼び出したら便利
								if (lastSelectshop != null) {
									jQuery('input[name = "F_SHOP_ID"]').val(lastSelectshop['F_ID']);
									jQuery('input[name = "F_SHOP_NAME"]').val(lastSelectshop['F_SHOP_NAME']);
								}

							});
						</script><?php get_template_part( 'template-parts/yk_popup_shop_list' );  ?>
					</td>
				</tr>
				<tr>
					<th>評価既定のインセンティブ</th>
					<td>
						<input type="text" name="F_INCENTIVE" value="<?php esc_attr_e($this->yk->F_INCENTIVE);?>" />
					</td>
				</tr>
				<tr>
					<th>既定の個人評価</th>
					<td>
						<input type="text" name="F_SELF_EVALUATEF" value="<?php esc_attr_e($this->yk->F_SELF_EVALUATEF);?>" />
					</td>
				</tr>
            </table>
            <input type="hidden" name="action" value="save" />
            <input type="hidden" name="page" value="<?php echo $_REQUEST['page'] ?>" />
            <input type="hidden" name="nonce" value="<?php echo wp_create_nonce(strtolower(YK_Admin_Input_Staff::PAGE_NAME . "_save")) ?>" />
        </form>
    </div>
</div>

<script>

        jQuery(document).ready(function($) {


        });

</script>

<?php

    }
}?>