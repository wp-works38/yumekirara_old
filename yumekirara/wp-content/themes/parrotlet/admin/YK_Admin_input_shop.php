<?php
require_once(dirname(__FILE__) . '/../libs.php');

function yk_shop_input_show_page() {
    YK_Admin_input_shop::get_instance()->show_page();
}

class YK_Admin_input_shop
{
	const PAGE_NAME = 'yk_shop_input_page';
    private $action = '';		// none

    static $instance;
	private $message = '';
	private $errflg = false;
    private $ymd = '';
    private $h = '';
    private $m = '';
    /**
	 * インスタンス化
	 *
	 */
	static function get_instance() {
		if (!isset(self::$instance)) {
			self::$instance = new YK_Admin_input_shop();
		}

		return self::$instance;
	}

	public function __construct() {

        // action指定
        if (isset($_GET['action'])) {
            $this->action = $_GET['action'];
        }
	}

    /**
	 *  表示
	 */
    function show_page() {

		if (isset($_POST['action'])) {
            $postAction = $_POST['action'];

			if (!wp_verify_nonce($_POST['nonce'], self::PAGE_NAME . "_{$postAction}")) {
				die("Nonce error!");
			}


			if ($postAction == 'save') {

                // 読み込む
                if(!empty($_POST['F_ID']))
                {
                    $edit_items = YK_M_shop::Search(array('F_ID'=>$_POST['F_ID']));

                    $this->ex_link = is_array($edit_items) && !empty($edit_items) ? $edit_items[0] : null;
                }
                if($this->ex_link == null)
                {
                    $this->ex_link = new YK_M_shop();// 新規保存
                }

                try {
                    //パラメータ設定し保存
                    $this->ex_link->F_ID = isset($_POST['F_ID'])?$_POST['F_ID']:'';
                    $this->ex_link->F_SHOP_NAME = isset($_POST['F_SHOP_NAME'])?$_POST['F_SHOP_NAME']:'';

                    // 入力チェック
                    $this->message = '';
                    if($this->ex_link->F_SHOP_NAME==='')
                    {
                        $this->message .= '保存エラー：店舗名は必須です。<br>';
                        $this->errflg = true;
                    }

                    if(!$this->errflg)
                    {
                        if(!$this->ex_link->save())
                        {
                            $this->message .= '保存エラー：データ更新に失敗しました。<br>';
                            $this->errflg = true;
                        }
                    }
                }
                catch (Exception $e) {
                    $this->message = '保存エラー';
                    $this->errflg = true;
                }
            }
        }
        else{
            //GET
            if ($this->action == 'edit') {
                //編集
                // 読み込む
                $array = YK_M_shop::Search(array('F_ID'=>$_GET['id']));
                $this->ex_link = is_array($array) && !empty($array) ? $array[0] : new YK_M_shop();

            } else {
                //新規
                $this->ex_link = new YK_M_shop();
            }
        }


?>


<div class="wrap input-page">
	<div>
		<?php
        // 保存結果の出力
        if($_POST['action']=='save')
        {
            if($this->errflg)
            {
                $message = $this->message;
                echo "<div id=\"message\" class=\"notice notice-error is-dismissible\"><p>$message </p><button type=\"button\" class=\"notice-dismiss\"><span class=\"screen-reader-text\">この通知を非表示にする</span></button></div>";
            }else
            {
                echo "<div id=\"message\" class=\"update notice notice-success is-dismissible\"><p>保存しました。 </p><button type=\"button\" class=\"notice-dismiss\"><span class=\"screen-reader-text\">この通知を非表示にする</span></button></div>";

            }
        }

        ?>
	</div>

	<h2>
		<?php echo ($this->ex_link->F_ID > 0) ? '店舗編集' : '店舗新規登録' ?>
	</h2>

	<div>
		<form method="post">
			<input name="Submit" type="submit" style="margin-top:20px" value="<?php esc_attr_e('保存・更新'); ?>" />
			<br />

			<table id="resister_table">
				<tr>
					<th>ＩＤ</th>
					<td>
						<input type="text" name="F_ID" readonly value="<?php esc_attr_e($this->ex_link->F_ID);?>" />
					</td>
				</tr>
				<tr>
					<th>店舗名</th>
					<td>
						<input type="text" name="F_SHOP_NAME" value="<?php esc_attr_e($this->ex_link->F_SHOP_NAME);?>" />
					</td>
				</tr>
			</table>
			<input type="hidden" name="action" value="save" />
			<input type="hidden" name="page" value="<?php echo $_REQUEST['page'] ?>" />
			<input type="hidden" name="nonce" value="<?php echo wp_create_nonce(strtolower(YK_Admin_input_shop::PAGE_NAME . "_save")) ?>" />
		</form>
	</div>
</div>

<script>

	jQuery(document).ready(function ($) {


	});

</script>

<?php

    }
}?>