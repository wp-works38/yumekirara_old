<?php
require_once(dirname(__FILE__) . '/../libs.php');

/**
 * Register Settings
 */
function yk_t_finance_init() {
    //オプションに保存しないので不要

}

function yk_t_finance_list_show_page() {
    YK_Admin_List_Finance::get_instance()->show_page();
}

class YK_Admin_List_Finance
{
	const PAGE_NAME = 'yk_t_finance_list_page';
    private $action = '';		// none

    static $list_instance;
    private $blist;
	private $message = '';
	private $errflg = false;
    /**
	 * インスタンス化
	 *
	 */
	static function get_instance() {
		if (!isset(self::$list_instance)) {
			self::$list_instance = new YK_Admin_List_Finance();
		}

		return self::$list_instance;
	}

	public function __construct() {

		//parent::__construct();

        // action指定
        if (isset($_GET['action'])) {
            $this->action = $_GET['action'];
        }

		// CSSロード
        //wp_enqueue_style('wp-jquery-ui-dialog');

		// Javascriptロード
        //wp_enqueue_script("mtssb_list_admin_js", $this->plugin_url . "js/mtssb-list-admin.js", array('jquery', 'jquery-ui-dialog'));
	}

    /**
	 * 一覧表示
	 */
    function show_page() {
        $this->errflg = false;
        $this->message = '';

        $searchkey = $_REQUEST['searchkey'];

        switch ($this->action) {
            //case 'select' :
            //    $this->_input_check($_REQUEST);
            //    break;
            case 'delete' :
                // NONCEチェックOKなら削除する
                if (wp_verify_nonce($_GET['nonce'], self::PAGE_NAME . "_{$this->action}")) {
                    $yk_array = YK_T_FINANCE::Search(array('F_ID'=>$_GET['id']));
                    if(is_array($yk_array) && !empty($yk_array))
                    {
                        $yk_array[0]->Delete();
                    }
                } else {
                    $this->message = 'Nonce check error.';
                    $this->errflg = true;
                }
                // ページネーションのリンクにdeleteが残るのでURLをクリアする
                $_SERVER['REQUEST_URI'] = remove_query_arg(array('id', 'action', 'nonce'));
                break;
            default:
                break;
        }

?>

<div class="wrap sl_external_link-page">

	<h2>
		予算管理一覧
		<a href="?page=yk_t_finance_input_page" id="insert_yk_button" class="page-title-action">新規登録</a>
	</h2>

	<?php
        // リスト表示
        $this->blist = new YK_T_FINANCE_List($this, $searchkey);
        $this->blist->prepare_items();
    ?>
	<div id="yk-list">
		<form method="post">
			<input name="searchkey" type="text" value="<?php echo $searchkey ?>" />
			<input name="Submit" type="submit" style="margin-top:20px" value="<?php esc_attr_e('検索'); ?>" />

			<input type="hidden" name="page" value="<?php echo $_REQUEST['page'] ?>" />
			<input type="hidden" id="nonce_ajax" value="<?php echo wp_create_nonce(strtolower(YK_Admin_List_Finance::PAGE_NAME)) ?>" /><?php $this->blist->display() ?>
		</form>
	</div>
</div>

<script>

	jQuery(document).ready(function ($) {


	});

</script>

<?php

    }

    /**
	 * カラムソート指定をキー配列にする
	 *
	 */
    public function set_order_key($params)
    {
        $order = array('key' => array('F_ID'), 'direction' => 'desc');

        if (isset($params['orderby']) && $params['orderby']!='') {
            $order['key'] = array($params['orderby']);
		}

		if (isset($params['order'])) {
			$order['direction'] = $params['order'] == 'asc' ? 'asc' : 'desc';
		}

		return $order;
	}
}

/**
 * リストクラス　メンバー　
 */
class YK_T_FINANCE_List extends WP_List_Table {

	private $per_page = 2;
    private $dba;
    private $searchkey;
    private $shoparray;
    private $staffarray;
    private $categoryarray;
	/**
	 * Constructor
	 *
	 */
	public function __construct($dba, $searchkey) {
		global $status, $page;
        $this->dba = $dba;
        $this->searchkey = $searchkey;

		parent::__construct(array(
			'singular' => 'externallink',
			'plural' => 'externallinks',
			'ajax' => false
		));

	}

	/**
	 * リストカラム情報
	 *
	 */
	public function get_columns() {
		return array(
			'F_ID' => 'F_ID',
			'F_SALES_BUDGET' => '売り上げ予算',
			'F_PAY_RESOURCE' => '支給原資総額算出率',
			'F_TOTAL_PAY' => '総支給額原資',
			'F_FURISODE' => '予測最低接客数（振袖成約率）',
			'F_HAKAMA' => '予測最低接客数（袴成約率）',

		);
	}

	/**
	 * ソートカラム情報
	 *
	 */
	public function get_sortable_columns() {
		return array(
			'F_ID' => array('F_ID', false),
		);
	}

	/**
	 * カラムデータのデフォルト表示
	 *
	 */
	public function column_default($item, $column_name) {

		switch ($column_name) {
			case 'ID' :
			case 'F_SALES_BUDGET' :
			case 'F_PAY_RESOURCE' :
			case 'F_TOTAL_PAY' :
			case 'F_FURISODE' :
			case 'F_HAKAMA' :
				return $item->{$column_name};
			case 'json' :
				return json_encode($item);
			default :
				return print_r($item, true);
		}
	}
    /**
	 * カラムデータ edit_id とアクションリンク表示
	 *
	 */
	public function column_F_ID($item) {
        $p1 = YK_Admin_Input_Finance::PAGE_NAME;
		// アクション
		$actions = array(
			'edit' => sprintf("<a href=\"?page=${p1}&amp;id=%d&amp;action=edit\">%s</a>", $item->F_ID, __('Edit')),
			'delete' => sprintf('<a href="?page=yk_t_finance_list_page&amp;id=%d&amp;action=delete&amp;nonce=%s" onclick="return confirm(\'%s\')">%s</a>', $item->F_ID, wp_create_nonce(YK_Admin_List_Finance::PAGE_NAME . '_delete'), '削除してもよろしいですか？', __('Delete')),
		);

		return $item->F_ID . $this->row_actions($actions);
	}
	/**
	 * リスト表示準備
	 *
	 * @dba		parent object
	 */
	public function prepare_items() {
        $dba = $this->dba;
        $searchkey = $this->searchkey;

		// カラムヘッダープロパティの設定
		$this->_column_headers = array($this->get_columns(), array(), $this->get_sortable_columns());

		// カレントページの取得
		$current_page = $this->get_pagenum() - 1;

		// メンバーデータの取得

        //検索
        $params = array();
        //$params['innerjoin_staff'] =true; //検索キー
        //$params['innerjoin_shop'] = true; //検索キー
        $params['searchkey'] = $searchkey; //検索キー
        $params['startindex'] = $current_page * $this->per_page;
        $params['per_page'] = $this->per_page;
        $order = $dba->set_order_key($_REQUEST);
        $params['orderkey'] =  $order['key'];
        $params['orderdirection'] =  $order['direction'];
		$this->items = YK_T_FINANCE::Search($params);


		// 検索総数の取得
        $params['startindex'] = null;
        $params['per_page'] = null;
		$total_items = YK_T_FINANCE::Total_Count($params);

		// ページネーション設定
		$this->set_pagination_args(array(
			'total_items' => $total_items,
			'per_page' => $this->per_page,
			'total_pages' => ceil($total_items / $this->per_page),
		));

	}
}
?>
