
<style type="text/css">
    .popup_self_evaluate_list_button {
        background-color: #fcfcfc #f8f8f8 #f3f3f3 #f8f8f8;
        background-image: -webkit-linear-gradient(top,#f2f2f2,#ebebeb 50%,#ddd 50%,#cfcfcf);
        padding: 2px 15px;
        border: 1px solid;
        -webkit-border-radius: 2px;
        color:black;
        font-size:14px;
    }
</style>

<a class="popup_self_evaluate_list_button" onclick="lastSelectself_evaluate = null; jQuery('#popup_self_evaluate').show();">個人評価（設問）選択</a>
<div id="popup_self_evaluate">
    <div id="popupsub_self_evaluate">
        <div id="popup_search_panel_self_evaluate">
            <input type="text" id="popup_search_self_evaluate" placeholder="検索条件を入力してください" />
            <a onclick="search_self_evaluate_class.getSelf_Evaluate(jQuery('#popup_search_self_evaluate').val());">検索</a>
            <img id="popup_self_evaluate_loading_icon" src="<?php echo get_template_directory_uri() . "/images/icon_loader_a_ww_02_s1.gif"?>" />
            <span id="popup_search_erorr_message_self_evaluate">検索エラー</span>
            <div id="search_result">
                <table id="popup_self_evaluate_table"></table>
            </div>
            <a onclick="jQuery('#popup_self_evaluate').hide();">閉じる</a>
        </div><?php ?>
    </div>
</div>

<script>
    if (typeof pupup_self_evaluate_list_resut === 'undefined')
        alert('テンプレートの外で[pupup_self_evaluate_list_resut]を定義してください');

    // ユーザー選択
    var search_self_evaluate_class;
    var lastSelectself_evaluate = null;
    jQuery(document).ready(function ($) {
        jQuery('#popup_self_evaluate').hide();
        var SearchUserClass = function (jQuery) {
            this.setSelf_EvaluateId = function (elem, id) {
                if (typeof pupup_self_evaluate_list_resut === 'undefined')
                    alert('テンプレートの外で[pupup_self_evaluate_list_resut]を定義してください');
                var text = jQuery(elem).parent().parent().find('.self_evaluate_json').text();
                lastSelectself_evaluate = JSON.parse(text);
                pupup_self_evaluate_list_resut.val(id);
                pupup_self_evaluate_list_resut.change();
                jQuery('#popup_self_evaluate').hide();
            }
            this.getSelf_Evaluate = function (search_text) {
                jQuery('#popup_self_evaluate_loading_icon').show();
                jQuery('#popup_search_erorr_message_self_evaluate').hide();
                $.ajax({
                    url: admin_ajax_url,
                    type: 'GET',
                    data: {
                        // 呼び出すアクション名
                        action: 'my_ajax_get_posts',
                        // アクションに対応するnonce
                        secure: CSRF,
                        // 検索条件
                        searchkey: search_text,
                        module: 'admin_request_search_self_evaluate',
                    }
                })
                    .done(function (res) {
                        console.log(res);
                        jQuery('#popup_self_evaluate_loading_icon').hide();
                        jQuery("#popup_self_evaluate_table tr").remove();

                        var ul = jQuery("#popup_self_evaluate_table");
                        ul.append('<tr><th>選択</th><th>ID</th><th>名前</th></tr>');
                        jQuery(res).each(function (index, element) {
                            var self_evaluateid = element["F_ID"];
                            var self_evaluatename = element["F_ITEM_NAME"];

                            var td_select = '<td><a onclick="search_self_evaluate_class.setSelf_EvaluateId(this, ' + self_evaluateid + ')">選択</a></td>';
                            var td_self_evaluateid = '<td><span>' + self_evaluateid + '</span></td>';
                            var td_self_evaluatename = '<td><span>' + self_evaluatename + '</span></td>';
                            var td_json = '<td class="self_evaluate_json" style="display:none">' + JSON.stringify(element) + '</td>';

                            ul.append('<tr class="remove_key" >' + td_select + td_self_evaluateid + td_self_evaluatename + td_json + '</tr>');
                        });

                    })
                    .fail(function (jqXHR, textStatus, errorThrown) {
                        jQuery('#popup_self_evaluate_loading_icon').hide();
                        jQuery('#search_erorr_message').show();
                        console.log(jqXHR, textStatus, errorThrown, arguments);
                    });
            }
        }
        search_self_evaluate_class = new SearchUserClass(jQuery);
        jQuery('#popup_self_evaluate_loading_icon').hide();
        search_self_evaluate_class.getSelf_Evaluate();
    });

    function popup_search_self_evaluate() {

    }

</script>

<script>
    var admin_ajax_url  = '<?php echo admin_url('admin-ajax.php', __FILE__); ?>';
    var CSRF = '<?php echo wp_create_nonce('text_test_ajax') ?>';
</script>