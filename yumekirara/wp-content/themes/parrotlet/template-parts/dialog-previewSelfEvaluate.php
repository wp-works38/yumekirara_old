<?php
/**
プレビュー
 */
$pagejs = get_template_directory_uri() . '/js/dialog-previewSelfEvaluate.js';
$update_datetime_js = filemtime($jsPath);
wp_enqueue_script( 'my_dialog-previewSelfEvaluate' , $pagejs, array(),$update_datetime_js);

?>

<div class="modal fade" id="modal-previewSelfEvaluate" tabindex="-1" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog modal-lg">
		<!-- 3.モーダルのコンテンツ -->
		<div class="modal-content">
			<button type="button" class="close dlg-custom-close" data-dismiss="modal">
				<span aria-hidden="true">&times;</span>
			</button>

			<!-- 5.モーダルのボディ -->
			<div class="modal-body">
				<label id="ErrorMessageSelfEvaluate" style="color:red;"></label>
					<div id="choice">
						<div id="choice-wrap">
							<div id="choice-contents">
								<div>項目名</div>
								<div>
									<input name="F_SELF_EVALUATE_ID" type="hidden"/>
									<input name="F_ITEM_NAME" type="text"/>
								</div>
								<div class="add_button_wrapper">
									<button onclick="dialog_previewSelfEvaluate.Add();">項目を追加</button>
								</div>
								<table id="choice-table">
									<tbody>
									</tbody>
								</table>
								<div id="choice-table">

								</div>
							</div>
						</div>
					</div>
			</div>
			<!-- 6.モーダルのフッタ -->
			<div class="modal-footer">
				<button class="btn-default" data-dismiss="modal">閉じる</button>
				<button class="btn-primary" onclick="dialog_previewSelfEvaluate.Save(); return false;">保存</button>
			</div>
		</div>
	</div>
</div>
<input type="hidden" id="self_evaluate_id" />
<input type="hidden" id="self_evaluate_price" />
<script>
    const path = '<?php echo admin_url('admin-ajax.php', __FILE__); ?>';
</script>
<script>
	jQuery(function () {
		dialog_previewSelfEvaluate.ajaxurl = '<?php echo admin_url('admin-ajax.php', __FILE__); ?>';
        dialog_previewSelfEvaluate.CSRF = '<?php echo wp_create_nonce('yk_self_evaluate') ?>';	
	});
</script>
