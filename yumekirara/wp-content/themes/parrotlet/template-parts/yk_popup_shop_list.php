
<style type="text/css">
    .popup_shop_list_button {
        background-color: #fcfcfc #f8f8f8 #f3f3f3 #f8f8f8;
        background-image: -webkit-linear-gradient(top,#f2f2f2,#ebebeb 50%,#ddd 50%,#cfcfcf);
        padding: 2px 15px;
        border: 1px solid;
        -webkit-border-radius: 2px;
        color:black;
        font-size:14px;
    }
</style>

<a class="popup_shop_list_button" onclick="lastSelectshop = null; jQuery('#popup_shop').show();">店舗選択</a>
<div id="popup_shop">
    <div id="popupsub_shop">
        <div id="popup_search_panel_shop">
            <input type="text" id="popup_search_shop" placeholder="検索条件を入力してください" />
            <a onclick="search_shop_class.getShop(jQuery('#popup_search_shop').val());">検索</a>
            <img id="popup_shop_loading_icon" src="<?php echo get_template_directory_uri() . "/images/icon_loader_a_ww_02_s1.gif"?>" />
            <span id="popup_search_erorr_message_shop">検索エラー</span>
            <div id="search_result">
                <table id="popup_shop_table"></table>
            </div>
            <a onclick="jQuery('#popup_shop').hide();">閉じる</a>
        </div><?php ?>
    </div>
</div>

<script>
    if (typeof pupup_shop_list_resut === 'undefined')
        alert('テンプレートの外で[pupup_shop_list_resut]を定義してください');

    // ユーザー選択
    var search_shop_class;
    var lastSelectshop = null;
    jQuery(document).ready(function ($) {
        jQuery('#popup_shop').hide();
        var SearchUserClass = function (jQuery) {
            this.setShopId = function (elem, id) {
                if (typeof pupup_shop_list_resut === 'undefined')
                    alert('テンプレートの外で[pupup_shop_list_resut]を定義してください');
                var text = jQuery(elem).parent().parent().find('.shop_json').text();
                lastSelectshop = JSON.parse(text);
                pupup_shop_list_resut.val(id);
                pupup_shop_list_resut.change();
                jQuery('#popup_shop').hide();
            }
            this.getShop = function (search_text) {
                jQuery('#popup_shop_loading_icon').show();
                jQuery('#popup_search_erorr_message_shop').hide();
                $.ajax({
                    url: admin_ajax_url,
                    type: 'GET',
                    data: {
                        // 呼び出すアクション名
                        action: 'my_ajax_get_posts',
                        // アクションに対応するnonce
                        secure: CSRF,
                        // 検索条件
                        searchkey: search_text,
                        module: 'admin_request_search_shop',
                    }
                })
                    .done(function (res) {
                        console.log(res);
                        jQuery('#popup_shop_loading_icon').hide();
                        jQuery("#popup_shop_table tr").remove();

                        var ul = jQuery("#popup_shop_table");
                        ul.append('<tr><th>選択</th><th>ID</th><th>名前</th></tr>');
                        jQuery(res).each(function (index, element) {
                            var shopid = element["F_ID"];
                            var shopname = element["F_SHOP_NAME"];

                            var td_select = '<td><a onclick="search_shop_class.setShopId(this, ' + shopid + ')">選択</a></td>';
                            var td_shopid = '<td><span>' + shopid + '</span></td>';
                            var td_shopname = '<td><span>' + shopname + '</span></td>';
                            var td_json = '<td class="shop_json" style="display:none">' + JSON.stringify(element) + '</td>';

                            ul.append('<tr class="remove_key" >' + td_select + td_shopid + td_shopname + td_json + '</tr>');
                        });

                    })
                    .fail(function (jqXHR, textStatus, errorThrown) {
                        jQuery('#popup_shop_loading_icon').hide();
                        jQuery('#search_erorr_message').show();
                        console.log(jqXHR, textStatus, errorThrown, arguments);
                    });
            }
        }
        search_shop_class = new SearchUserClass(jQuery);
        jQuery('#popup_shop_loading_icon').hide();
        search_shop_class.getShop();
    });

    function popup_search_shop() {

    }

</script>

<script>
    var admin_ajax_url  = '<?php echo admin_url('admin-ajax.php', __FILE__); ?>';
    var CSRF = '<?php echo wp_create_nonce('text_test_ajax') ?>';
</script>