<?php
/**
プレビュー
 */
$pagejs = get_template_directory_uri() . '/js/dialog-previewStaff.js';
$update_datetime_js = filemtime($jsPath);
wp_enqueue_script( 'my_dialog-previewStaff' , $pagejs, array(),$update_datetime_js);
?>

<div class="modal fade" id="modal-previewStaff" tabindex="-1" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog modal-lg">
		<!-- 3.モーダルのコンテンツ -->
		<div class="modal-content">
			<button type="button" class="close dlg-custom-close" data-dismiss="modal">
				<span aria-hidden="true">&times;</span>
			</button>

			<!-- 5.モーダルのボディ -->
			  <div class="modal-body">
                <div style="text-align:left">
					<label id="ErrorMessageStaff" style="color:red;"></label>
                   	<div id="staff-wrap">
						<div id="staff-contents">
							<table>
								<tbody>
									<tr>
										
										<th>氏名</th>
										<td>
											<input name="F_ID" type="hidden" />
											<input name="F_LAST_NAME" type="text" />
											<input name="F_FAST_NAME" type="text" />
										</td>
									</tr>
									<tr>
										<th>役職</th>
										<td>
											<label>
												<input type="radio" id="F_POSITION_MANAGER" name="F_POSITION_ID" value="1" />店長
											</label>
											<label>
												<input type="radio" id="F_POSITION_CHIEF" name="F_POSITION_ID" value="2" />チーフ
											</label>
											<label>
												<input type="radio" id="F_POSITION_MEMBER" name="F_POSITION_ID" value="3" />スタッフ
											</label>
										</td>
									</tr>
									<tr>
										<th>所属店舗</th>
										<td>
											<label>
												<input type="checkbox" id="F_KAWAGOE" class="F_SHOP_ID" value="1" />川越
											</label>
											<label>
												<input type="checkbox" id="F_TOKOROZAWA" class="F_SHOP_ID" value="2" />所沢
											</label>
											<label>
												<input type="checkbox" id="F_TATIKAWA" class="F_SHOP_ID" value="3" />立川
											</label>
											<label>
												<input type="checkbox" id="F_KITIJOUJI" class="F_SHOP_ID" value="4" />吉祥寺
											</label>
											<label>
												<input type="checkbox" id="F_STUDIO" class="F_SHOP_ID" value="5" />スタジオ
											</label>
										</td>
									</tr>
									<tr>
										<th>規定のインセンティブ</th>
										<td>
                                            <input id="F_INCENTIVE_show" type="text" />円
                                            <input id="F_INCENTIVE" name="F_INCENTIVE" type="hidden" />
                                            <div>
                                                <label id="F_INCENTIVE_error" style="color:red"></label>
                                            </div>
										</td>
									</tr>
									<tr>
										<th>規定の個人評価</th>
										<td>
                                            <input id="F_SELF_EVALUATEF" name="F_SELF_EVALUATEF" type="text" />
                                            <div>
                                                <label id="F_SELF_EVALUATEF_error" style="color:red"></label>
                                            </div>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
                </div>
            </div>

			<!-- 6.モーダルのフッタ -->
			<div class="modal-footer">
				<button type="button" class="btn-default" data-dismiss="modal">閉じる</button>
				<button id="delete" type="button" class="btn-danger" onclick="dialog_previewStaff.Delete(); return false;">削除</button>
				<button id="save" type="button" class="btn-primary" onclick="dialog_previewStaff.Save(); return false;">保存</button>
			</div>
		</div>
	</div>
</div>
<script>
</script>
<script>
	jQuery(function () {
		dialog_previewStaff.ajaxurl = '<?php echo admin_url('admin-ajax.php', __FILE__); ?>';
        dialog_previewStaff.CSRF = '<?php echo wp_create_nonce('yk_staff') ?>';
	});
			
</script>
