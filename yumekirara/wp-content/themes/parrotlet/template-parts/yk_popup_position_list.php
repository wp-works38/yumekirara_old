
<style type="text/css">
    .popup_position_list_button {
        background-color: #fcfcfc #f8f8f8 #f3f3f3 #f8f8f8;
        background-image: -webkit-linear-gradient(top,#f2f2f2,#ebebeb 50%,#ddd 50%,#cfcfcf);
        padding: 2px 15px;
        border: 1px solid;
        -webkit-border-radius: 2px;
        color:black;
        font-size:14px;
    }
</style>

<a class="popup_position_list_button" onclick="lastSelectposition = null; jQuery('#popup_position').show();">役職選択</a>
<div id="popup_position">
    <div id="popupsub_position">
        <div id="popup_search_panel_position">
            <input type="text" id="popup_search_position" placeholder="検索条件を入力してください" />
            <a onclick="search_position_class.getPosition(jQuery('#popup_search_position').val());">検索</a>
            <img id="popup_position_loading_icon" src="<?php echo get_template_directory_uri() . "/images/icon_loader_a_ww_02_s1.gif"?>" />
            <span id="popup_search_erorr_message_position">検索エラー</span>
            <div id="search_result">
                <table id="popup_position_table"></table>
            </div>
            <a onclick="jQuery('#popup_position').hide();">閉じる</a>
        </div><?php ?>
    </div>
</div>

<script>
    if (typeof pupup_position_list_resut === 'undefined')
        alert('テンプレートの外で[pupup_position_list_resut]を定義してください');

    // ユーザー選択
    var search_position_class;
    var lastSelectposition = null;
    jQuery(document).ready(function ($) {
        jQuery('#popup_position').hide();
        var SearchUserClass = function (jQuery) {
            this.setPositionId = function (elem, id) {
                if (typeof pupup_position_list_resut === 'undefined')
                    alert('テンプレートの外で[pupup_position_list_resut]を定義してください');
                var text = jQuery(elem).parent().parent().find('.position_json').text();
                lastSelectposition = JSON.parse(text);
                pupup_position_list_resut.val(id);
                pupup_position_list_resut.change();
                jQuery('#popup_position').hide();
            }
            this.getPosition = function (search_text) {
                jQuery('#popup_position_loading_icon').show();
                jQuery('#popup_search_erorr_message_position').hide();
                $.ajax({
                    url: admin_ajax_url,
                    type: 'GET',
                    data: {
                        // 呼び出すアクション名
                        action: 'my_ajax_get_posts',
                        // アクションに対応するnonce
                        secure: CSRF,
                        // 検索条件
                        searchkey: search_text,
                        module: 'admin_request_search_position',
                    }
                })
                    .done(function (res) {
                        console.log(res);
                        jQuery('#popup_position_loading_icon').hide();
                        jQuery("#popup_position_table tr").remove();

                        var ul = jQuery("#popup_position_table");
                        ul.append('<tr><th>選択</th><th>ID</th><th>名前</th></tr>');
                        jQuery(res).each(function (index, element) {
                            var positionid = element["F_ID"];
                            var positionname = element["F_POSITION_NAME"];

                            var td_select = '<td><a onclick="search_position_class.setPositionId(this, ' + positionid + ')">選択</a></td>';
                            var td_positionid = '<td><span>' + positionid + '</span></td>';
                            var td_positionname = '<td><span>' + positionname + '</span></td>';
                            var td_json = '<td class="position_json" style="display:none">' + JSON.stringify(element) + '</td>';

                            ul.append('<tr class="remove_key" >' + td_select + td_positionid + td_positionname + td_json + '</tr>');
                        });

                    })
                    .fail(function (jqXHR, textStatus, errorThrown) {
                        jQuery('#popup_position_loading_icon').hide();
                        jQuery('#search_erorr_message').show();
                        console.log(jqXHR, textStatus, errorThrown, arguments);
                    });
            }
        }
        search_position_class = new SearchUserClass(jQuery);
        jQuery('#popup_position_loading_icon').hide();
        search_position_class.getPosition();
    });

    function popup_search_position() {

    }

</script>

<script>
    var admin_ajax_url  = '<?php echo admin_url('admin-ajax.php', __FILE__); ?>';
    var CSRF = '<?php echo wp_create_nonce('text_test_ajax') ?>';
</script>