
<style type="text/css">
    .popup_staff_list_button {
        background-color: #fcfcfc #f8f8f8 #f3f3f3 #f8f8f8;
        background-image: -webkit-linear-gradient(top,#f2f2f2,#ebebeb 50%,#ddd 50%,#cfcfcf);
        padding: 2px 15px;
        border: 1px solid;
        -webkit-border-radius: 2px;
        color:black;
        font-size:14px;
    }
</style>

<a class="popup_staff_list_button" onclick="lastSelectstaff = null; jQuery('#popup_staff').show();">従業員選択</a>
<div id="popup_staff">
    <div id="popupsub_staff">
        <div id="popup_search_panel_staff">
            <input type="text" id="popup_search_staff" placeholder="検索条件を入力してください" />
            <a onclick="search_staff_class.getStaff(jQuery('#popup_search_staff').val());">検索</a>
            <img id="popup_staff_loading_icon" src="<?php echo get_template_directory_uri() . "/images/icon_loader_a_ww_02_s1.gif"?>" />
            <span id="popup_search_erorr_message_staff">検索エラー</span>
            <div id="search_result">
                <table id="popup_staff_table"></table>
            </div>
            <a onclick="jQuery('#popup_staff').hide();">閉じる</a>
        </div><?php ?>
    </div>
</div>

<script>
    if (typeof pupup_staff_list_resut === 'undefined')
        alert('テンプレートの外で[pupup_staff_list_resut]を定義してください');

    // ユーザー選択
    var search_staff_class;
    var lastSelectstaff = null;
    jQuery(document).ready(function ($) {
        jQuery('#popup_staff').hide();
        var SearchUserClass = function (jQuery) {
            this.setStaffId = function (elem, id) {
                if (typeof pupup_staff_list_resut === 'undefined')
                    alert('テンプレートの外で[pupup_staff_list_resut]を定義してください');
                var text = jQuery(elem).parent().parent().find('.staff_json').text();
                lastSelectstaff = JSON.parse(text);
                pupup_staff_list_resut.val(id);
                pupup_staff_list_resut.change();
                jQuery('#popup_staff').hide();
            }
            this.getStaff = function (search_text) {
                jQuery('#popup_staff_loading_icon').show();
                jQuery('#popup_search_erorr_message_staff').hide();
                $.ajax({
                    url: admin_ajax_url,
                    type: 'GET',
                    data: {
                        // 呼び出すアクション名
                        action: 'my_ajax_get_posts',
                        // アクションに対応するnonce
                        secure: CSRF,
                        // 検索条件
                        searchkey: search_text,
                        module: 'admin_request_search_staff',
                    }
                })
                    .done(function (res) {
                        console.log(res);
                        jQuery('#popup_staff_loading_icon').hide();
                        jQuery("#popup_staff_table tr").remove();

                        var ul = jQuery("#popup_staff_table");
                        ul.append('<tr><th>選択</th><th>ID</th><th>名前</th></tr>');
                        jQuery(res).each(function (index, element) {
                            var staffid = element["F_ID"];
                            var staffname = element["F_LAST_NAME"]+" "+element["F_FAST_NAME"];

                            var td_select = '<td><a onclick="search_staff_class.setStaffId(this, ' + staffid + ')">選択</a></td>';
                            var td_staffid = '<td><span>' + staffid + '</span></td>';
                            var td_staffname = '<td><span>' + staffname + '</span></td>';
                            var td_json = '<td class="staff_json" style="display:none">' + JSON.stringify(element) + '</td>';

                            ul.append('<tr class="remove_key" >' + td_select + td_staffid + td_staffname + td_json + '</tr>');
                        });

                    })
                    .fail(function (jqXHR, textStatus, errorThrown) {
                        jQuery('#popup_staff_loading_icon').hide();
                        jQuery('#search_erorr_message').show();
                        console.log(jqXHR, textStatus, errorThrown, arguments);
                    });
            }
        }
        search_staff_class = new SearchUserClass(jQuery);
        jQuery('#popup_staff_loading_icon').hide();
        search_staff_class.getStaff();
    });

    function popup_search_staff() {

    }

</script>

<script>
    var admin_ajax_url  = '<?php echo admin_url('admin-ajax.php', __FILE__); ?>';
    var CSRF = '<?php echo wp_create_nonce('text_test_ajax') ?>';
</script>