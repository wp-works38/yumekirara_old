<?php get_header(); ?>

<a href="<?php echo esc_url( home_url() );?>">←戻る</a>
<div id="page-settings">
	<div>
    <h1 id="page-title">システム設定</h1>
    </div>
	<hr />
    <div>
        <h3>総支給原資算出</h3>
        <?php
        // 店舗予算の合計値
        $sum = 0;
        $shops = YK_M_SHOP::search(array());
        foreach($shops as $shop)
        {
            if($shop->F_PROVISION_FUND)
            {
                $sum+=$shop->F_SHOP_BUDGET;
            }
        }
        ?>
        <div>
            <h4>【仮半期売上予算（全店舗合計）】</h4>
            店舗予算の合計：<label id="all_shop_sum"><?php esc_html_e($sum); ?></label>千円
        </div>
        <div>
            <table>
                <tr>
                    <th>全社予算達成率</th>
                    <th>支給原資総額算出率</th>
                    <th>支給原資総額</th>
                </tr>
                <tr>
                    <td>90%未満</td>
                    <td></td>
                    <td>個人取得評価分のみ支給</td>
                </tr>
                <tr>
                    <td>90%以上</td>
                    <td>0.00750</td>
                    <td class="sikyugennsisougaku"><?php esc_html_e($sum*0.00750); ?></td>
                </tr>
                <tr>
                    <td>91%以上</td>
                    <td>0.00825</td>
                    <td class="sikyugennsisougaku">
                        <?php esc_html_e($sum*0.00825); ?>
                    </td>
                </tr>
                <tr>
                    <td>92%以上</td>
                    <td>0.00900</td>
                    <td class="sikyugennsisougaku">
                        <?php esc_html_e($sum*0.00900); ?>
                    </td>
                </tr>
                <tr>
                    <td>93%以上</td>
                    <td>0.00975</td>
                    <td class="sikyugennsisougaku">
                        <?php esc_html_e($sum*0.00975); ?>
                    </td>
                </tr>
                <tr>
                    <td>94%以上</td>
                    <td>0.01050</td>
                    <td class="sikyugennsisougaku">
                        <?php esc_html_e($sum*0.01050); ?>
                    </td>
                </tr>
                <tr>
                    <td>95%以上</td>
                    <td>0.01125</td>
                    <td class="sikyugennsisougaku">
                        <?php esc_html_e($sum*0.01125); ?>
                    </td>
                </tr>
                <tr>
                    <td>96%以上</td>
                    <td>0.01200</td>
                    <td class="sikyugennsisougaku">
                        <?php esc_html_e($sum*0.01200); ?>
                    </td>
                </tr>
                <tr>
                    <td>97%以上</td>
                    <td>0.01275</td>
                    <td class="sikyugennsisougaku">
                        <?php esc_html_e($sum*0.01275); ?>
                    </td>
                </tr>
                <tr>
                    <td>98%以上</td>
                    <td>0.01350</td>
                    <td class="sikyugennsisougaku">
                        <?php esc_html_e($sum*0.01350); ?>
                    </td>
                </tr>
                <tr>
                    <td>99%以上</td>
                    <td>0.01425</td>
                    <td class="sikyugennsisougaku">
                        <?php esc_html_e($sum*0.01425); ?>
                    </td>
                </tr>
                <tr>
                    <td>100%以上</td>
                    <td>0.01500</td>
                    <td class="sikyugennsisougaku">
                        <?php esc_html_e($sum*0.01500); ?>
                    </td>
                </tr>
                <tr>
                    <td>101%以上</td>
                    <td>0.01575</td>
                    <td class="sikyugennsisougaku">
                        <?php esc_html_e($sum*0.01575); ?>
                    </td>
                </tr>
                <tr>
                    <td>102%以上</td>
                    <td>0.01650</td>
                    <td class="sikyugennsisougaku">
                        <?php esc_html_e($sum*0.01650); ?>
                    </td>
                </tr>
                <tr>
                    <td>103%以上</td>
                    <td>0.01725</td>
                    <td class="sikyugennsisougaku">
                        <?php esc_html_e($sum*0.01725); ?>
                    </td>
                </tr>
                <tr>
                    <td>104%以上</td>
                    <td>0.01800</td>
                    <td class="sikyugennsisougaku">
                        <?php esc_html_e($sum*0.01800); ?>
                    </td>
                </tr>
                <tr>
                    <td>105%以上</td>
                    <td>0.01875</td>
                    <td class="sikyugennsisougaku">
                        <?php esc_html_e($sum*0.01875); ?>
                    </td>
                </tr>
                <tr>
                    <td>106%以上</td>
                    <td>0.01950</td>
                    <td class="sikyugennsisougaku">
                        <?php esc_html_e($sum*0.01950); ?>
                    </td>
                </tr>
                <tr>
                    <td>107%以上</td>
                    <td>0.02025</td>
                    <td class="sikyugennsisougaku">
                        <?php esc_html_e($sum*0.02025); ?>
                    </td>
                </tr>
                <tr>
                    <td>108%以上</td>
                    <td>0.02100</td>
                    <td class="sikyugennsisougaku">
                        <?php esc_html_e($sum*0.02100); ?>
                    </td>
                </tr>
                <tr>
                    <td>109%以上</td>
                    <td>0.02175</td>
                    <td class="sikyugennsisougaku">
                        <?php esc_html_e($sum*0.02175); ?>
                    </td>
                </tr>
                <tr>
                    <td>110%以上</td>
                    <td>0.02250</td>
                    <td class="sikyugennsisougaku">
                        <?php esc_html_e($sum*0.02250); ?>
                    </td>
                </tr>
            </table>
		
        </div>
    </div>
    <hr />
	<div>
		<h3>店舗予算</h3>
		<div id="shop-wrap">
					<?php
                    $shops = YK_M_SHOP::Search(array());
                    ?>
					<div id="shop-contents">
						<table id="shop-table">
							<tbody>
								<tr>
									<th>店舗名</th>
									<th>予算</th>
									<th>評価支給原資に含む</th>
								</tr>
								<?php foreach ($shops as $yk):
                                          $F_SHOP_NAME = $yk->F_SHOP_NAME;
                                          $F_SHOP_BUDGET = $yk->F_SHOP_BUDGET;
                                          $F_PROVISION_FUND = $yk->F_PROVISION_FUND;
                                          $F_ID = $yk->F_ID;
                                ?>
								<tr>
									<td id="shop<?php echo $F_ID;?>">
										<input type="hidden" name="F_ID" value=" <?php esc_attr_e($F_ID);?> " />
										<input type="hidden" name="F_SHOP" value=" <?php esc_attr_e($F_SHOP_NAME);?> " />
										<?php echo $F_SHOP_NAME;?>
									</td>
									<td>
                                        <input class="left_input_show" type="text" value="<?php esc_attr_e($F_SHOP_BUDGET);?>" />千円
										<input class="left_input" type="hidden" name="F_SHOP_BUDGET" value="<?php esc_attr_e($F_SHOP_BUDGET);?>" />
										<label class="valid_shop_left" style="color:red;display:block"></label>
									</td>
									<td>
										<input type="checkbox" name="F_PROVISION_FUND" class="checkboxProvisionFund" value="<?php esc_attr_e($F_PROVISION_FUND);?>" <?php echo $F_PROVISION_FUND=='1'? 'checked':'';?> />
									</td>
								</tr>
								<?php
                                      endforeach;
                                ?>
							</tbody>
						</table>
						<div class="save_button_wrapper">
							<button class="btn-primary" onclick="yk_settings_shop.SaveShop();">保存</button>
						</div>
					</div>
			</div>
    </div>
	<hr />

	<div id="finance-contents">
			<h3>予測最低接客数</h3>
			<!--<label id="MessageFinance" style="color:red;"></label>-->
			<?php
			$finance = YK_T_FINANCE::Search(array('F_ID' => 1));
			$F_FURISODE = $finance[0]->F_FURISODE;
			$F_HAKAMA = $finance[0]->F_HAKAMA;
            ?>
			<table>
				<tbody>
					<tr>
						<th>振袖成約率</th>
						<td>
							<input id="furi-ritu" type="text" name="F_FURISODE" value="<?php esc_attr_e($F_FURISODE);?>" />
							<label id="valid-furi-ritu" style="color:red;;display:block"></label>
						</td>
					</tr>
					<tr>
						<th>袴成約率</th>
						<td>
							<input id="hakama-ritu" type="text" name="F_HAKAMA" value="<?php esc_attr_e($F_HAKAMA);?>" />
							<label id="valid-hakama-ritu" style="color:red;;display:block"></label>
						</td>
					</tr>
				</tbody>
			</table>
		<div class="save_button_wrapper">
				<button class="btn-primary" onclick="yk_settings_shop.SaveFinance();">保存</button>
		</div>
	</div>
	<hr />
	<div>
		<h3>入力項目設定</h3>
		<div id="self_evaluate">
			<h4>【個人評価】</h4>
			<div class="add_button_wrapper">
				<button class="btn-info" onclick="yk_settings_self_evaluate.newSelfEvaluate();">項目を追加</button>
			</div>
			<div id="self_evaluate-wrap">
				<?php
				$self_evaluates = YK_T_SELF_EVALUATE::Search(array());
                ?>
				<div id="self_evaluate-contents">
					<table>
						<tbody>
							<tr>
								<th>項目名</th>
								<th>選択肢数</th>
								<th>編集</th>
								<th>削除</th>
							</tr>
							<?php foreach ($self_evaluates as $yk):
									  $F_ITEM_NAME = $yk->F_ITEM_NAME;
									  $F_ITEM_COUNT = "";
									  $F_ID = $yk->F_ID;

									  $choice = YK_T_CHOICE::Search(array('F_SELF_EVALUATE' => $yk->F_ID));
									  $F_ITEM_COUNT=count($choice);
                            ?>
							<tr>
								<td id="self_evaluate<?php echo $F_ID;?>">
									<input type="hidden" id="F_ID" value="<?php esc_attr_e($F_ID);?>" />
									<?php echo $F_ITEM_NAME;?>
								</td>
								<td>
									<?php echo $F_ITEM_COUNT;?>
								</td>
								<td>
									<button class="btn-success" onclick="yk_settings_self_evaluate.editSelfEvaluate(<?php echo $yk->F_ID; ?>);">編集</button>
								</td>
								<td>
									<button class="btn-danger" id="self_evaluate-delete" onclick="yk_settings_self_evaluate.deleteSelfEvaluate(<?php echo $yk->F_ID; ?>);">削除</button>
								</td>
							</tr>
							<?php
								  endforeach;
                            ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	<hr />
	<div>
		<h3>職員マスタ</h3>
		<div class="add_button_wrapper">
			<button class="btn-info" onclick="yk_settings_staff.newStaff();">項目を追加</button>
		</div>
		<div id="staff-wrap">
			<?php
			//$shop_staff = YK_M_AFFILIATION::search(array('inner_join_position'=>true,'inner_join_staff'=>true,'inner_join_shop'=>true));
			$shop_staff = YK_M_AFFILIATION::search(array('inner_join_position'=>true,'inner_join_staff'=>true,'inner_join_shop'=>true));

			$shop_staff_group_array = array_reduce($shop_staff, function($result, $item) {
				$result[$item->F_STAFF_ID][] = $item;
				return $result;
			});
            ?>
			<div id="staff-contents">
				<table>
					<tbody>
						<tr>
							<th>氏名</th>
							<th>役職</th>
							<th>所属店舗</th>
							<th>規定のインセンティブ</th>
							<th>規定の個人評価</th>
							<th>編集</th>
						</tr>
						<?php 
						//foreach ($shop_staff as $key => $yk):
						foreach (array_keys($shop_staff_group_array) as $groupkey):
							$group = $shop_staff_group_array[$groupkey];
							$shops = '';
							foreach($group as $item)
							{
								$shops.= $item->ShopMasterEntity->F_SHOP_NAME.'<br />';
								$F_LAST_NAME = $item->StaffMasterEntity->F_LAST_NAME;
								$F_FAST_NAME = $item->StaffMasterEntity->F_FAST_NAME;
								$F_POSITION_NAME= $item->PositionMasterEntity->F_POSITION_NAME;
								$F_INCENTIVE = $item->StaffMasterEntity->F_INCENTIVE;
								$F_SELF_EVALUATEF = $item->StaffMasterEntity->F_SELF_EVALUATEF;
								$F_ID =  $item->StaffMasterEntity->F_ID;
							}
							$F_SHOP_NAME = $shops;
                        ?>
						<tr>
							<td id="staff<?php echo $F_ID;?>">
								<?php echo $F_LAST_NAME ?><?php echo $F_FAST_NAME;?>
							</td>
							<td>
								<?php echo $F_POSITION_NAME;?>
							</td>
							<td>
								<?php echo $F_SHOP_NAME;?>
							</td>
							<td class="F_INCENTIVE">
								<?php echo $F_INCENTIVE;?>
							</td>
							<td>
								<?php echo $F_SELF_EVALUATEF;?>
							</td>
							<td>
								<button class="btn-success" onclick="yk_settings_staff.editStaff(<?php echo $F_ID; ?>);">編集</button>
							</td>
						</tr>
						<?php
						endforeach;
                        ?>
					</tbody>
				</table>
			</div>
			<br />
			<div id="staff-groupUpdte">
				<h4>【職員マスタ 一括更新】</h4>
				<!--<label id="MessageGroupUpdte" style="color:red;"></label>-->
				<table>
					<tbody>
						<tr>
							<th>役職</th>
							<th>規定のインセンティブ</th>
							<th>規定の個人評価</th>
						</tr>
						<tr>
							<td>
								<?php
								$position = YK_M_POSITION::search(array());
                                ?>
								<select id="staffs">
									<option value="0" position="0"></option>
									<?php
									foreach ($position as $key => $yk)
									{

										$temp_position =$yk->F_POSITION_NAME;
										
										echo('<option value="'.$yk->F_ID.'" position="'.$yk->F_ID.'" >');
										{
											echo($temp_position);
										}
										echo('</option>');
									}
                                    ?>
								</select>
							</td>
							<td>
								<input id="incentiveGroupUpdte_show" type="text" name="F_INCENTIVE_GROUP_UPDTE_show"/>
								<input id="incentiveGroupUpdte" value="0" type="hidden" name="F_INCENTIVE_GROUP_UPDTE"/>
								<label id="valid-incentiveGroupUpdte" style="color:red;"></label>
							</td>
							<td>
								<input id="selfEvaluatefGroupUpdte" type="text" name="F_SELF_EVALUATEF_GROUP_UPDTE"/>
								<label id="valid-selfEvaluatefGroupUpdte" style="color:red;"></label>
							</td>
						</tr>
					</tbody>
				</table>
				<div class="save_button_wrapper">
					<button class="btn-primary" onclick="yk_settings_staff.groupUpdate();">保存</button>
				</div>
			</div>
		</div>

	</div>
</div>
<?php
//プレビューダイアログ
get_template_part( 'template-parts/dialog', 'previewSelfEvaluate' );
get_template_part( 'template-parts/dialog', 'previewStaff' );
?>
<script>
	jQuery(function () {
        yk_settings.ajaxurl = '<?php echo admin_url('admin-ajax.php', __FILE__); ?>';
        yk_settings.CSRF = '<?php echo wp_create_nonce('yk_settings'); ?>';

		yk_settings_shop.ajaxurl = '<?php echo admin_url('admin-ajax.php', __FILE__); ?>';
		yk_settings_shop.CSRF = '<?php echo wp_create_nonce('yk_settings_shop'); ?>';

		yk_settings_self_evaluate.ajaxurl = '<?php echo admin_url('admin-ajax.php', __FILE__); ?>';
		yk_settings_self_evaluate.CSRF = '<?php echo wp_create_nonce('yk_settings_self_evaluate'); ?>';

		yk_settings_staff.ajaxurl = '<?php echo admin_url('admin-ajax.php', __FILE__); ?>';
		yk_settings_staff.CSRF = '<?php echo wp_create_nonce('yk_settings_staff'); ?>';
	});
</script>

<?php get_footer(); ?>